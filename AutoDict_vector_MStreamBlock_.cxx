#include "vector"
#include "/mnt/hgfs/Root/Projects/ADC64/./adctr.C"
#ifdef __CINT__ 
#pragma link C++ nestedclasses;
#pragma link C++ nestedtypedefs;
#pragma link C++ class vector<MStreamBlock>+;
#pragma link C++ class vector<MStreamBlock>::*;
#ifdef G__VECTOR_HAS_CLASS_ITERATOR
#pragma link C++ operators vector<MStreamBlock>::iterator;
#pragma link C++ operators vector<MStreamBlock>::const_iterator;
#pragma link C++ operators vector<MStreamBlock>::reverse_iterator;
#endif
#endif
