/*
 * Open result file from cosmic_analysis macro
 * draw amplitude histogramms, fit them and
 * correct to calibration draw results trens
 *
 * !!! Run macro in bash mode:
 * root -b -q -l cosmic_analysis_results.c+
 *
 * Dmitry Finogeev
 * dmitry-finogeev@yandex.ru
 */

#define isLOCAL 0 // local - at my laptop

#include <iostream>
#include <fstream>
//#include <conio.h>
#include <ctime>

#include <TFile.h>
#include <TGraph.h>
#include <TF1.h>
#include <TTree.h>
#include <TObjArray.h>
#include <TString.h>
#include <TDirectory.h>
#include <TCanvas.h>
#include <TH1F.h>
#include <TSystemFile.h>
#include <TSystemDirectory.h>
#include <TBrowser.h>
#include <TAxis.h>
#include "langaus.hh"
#include <TLegend.h>
#include <TKey.h>
#include <TPaveStats.h>
#include <TPad.h>
#include <TObject.h>
#include <TAxis.h>
#include <TMarker.h>
#include <TLatex.h>
#include <TStyle.h>
#include <TText.h>
#include <TPaveLabel.h>

#include "Readme_reader.h"
#include "data_calculating.h"

#include "../libraries/utilites/utilites.h"

TF1*
FitLangau(TH1 *hsum, Double_t x_min = 4000., Double_t x_max = 10000.)
{
    
    TF1 *fit = new
            TF1 ("langau", langaufun, x_min, x_max, 4);
    fit->SetParNames ("Width", "MP", "Area", "GSigma");
    
    /* Double_t best_chi = 10000000., best_p1, best_p2, best_p3, best_p4;
      
	 for(Double_t ip1 = 0.; ip1 <= 5000; ip1 += 1000.)
	 for(Double_t ip2 = 0.; ip2 <= 5000; ip2 += 1000.)
	 for(Double_t ip3 = 0.; ip3 <= 10000000; ip3 += 1000000.)
	 for(Double_t ip4 = 0.; ip4 <= 5000; ip4 += 1000.)
	 {
	 fit->SetParameters(ip1,ip2,ip3,ip4);
	 hsum->Fit(fit,"RQN","",x_min,x_max);
	 
	 Double_t ichi = fit->GetChisquare();
	 if(ichi < best_chi)
	 {
	 best_chi = ichi;
	 best_p1 = ip1;
	 best_p2 = ip2;
	 best_p3 = ip3;
	 best_p4 = ip4;
	 }
	 }
	 fit->SetParameters(best_p1,best_p2,best_p3,best_p4);
	 */
    fit->SetParameters (500, 3000, 7000000, 500);
    hsum->Fit (fit, "RQN", "", x_min, x_max);
    
    return fit;
    
}

void
cosmic_analysis_results()
{
    
    // Input data #############################################################################
    //TString source_path = "/mnt/disk_data_8TB/cosmic_data_2017_quarter4/RESULTS/";
    //TString source_path = "/mnt/disk_data_8TB/cosmic_data_2018_quater1/RESULTS/";
    //TString source_path = "/mnt/hgfs/DETECTORS_DATA/RESULTS/CALO/root_results/cosmic_18_winter/";
    //TString source_path = "/mnt/disk_data/cosmic_data_2018_quater1/RESULTS/";
    //TString source_path = "/home/runna61/adc64_data_2018_quater1/cosmic_results/";
    //TString source_path = "/mnt/disk_data/cosmic_data_2018_quater2/Optical_contact_tests/commroots/";
    //TString source_path = "/mnt/disk_data/cosmic_data_2018_quater2/Optical_contact_tests/setting1/";
    //TString source_path = "/mnt/disk_data/cosmic_data_2018_quater2/RESULT/";
    //TString source_path = "/mnt/disk_data/cosmic_data_2018_quater3/RESULT/";
    //TString source_path = "/mnt/disk_data_8TB/cosmic_data_2018_quater3/RESULTS/";
    TString source_path = "/mnt/disk_data_8TB/cosmic_data_2018_quater2/RESULTS/";
    
    //    TString source_file_name = "qqq";
    //    TString source_file_name = "qqq";
    //    TString source_file_name = "qqq";
    //    TString source_file_name = "qqq";
    //    TString source_file_name = "qqq";
    //    TString source_file_name = "qqq";
    //    TString source_file_name = "qqq";
    //TString source_file_name = "cosmic_analysis_super_module_comm_trsh_test";
    //TString source_file_name = "cosmic_analysis_NICA_sel";
    //TString source_file_name = "beam_analysis_super_module_test";
    //TString source_file_name = "CBM_6modules_TH_200_ZS100_TC_3_2M__test_1";
    //TString source_file_name = "CBM_6modules_TH_200_ZS100_TC_3_2M__timensig_10";
    //TString source_file_name = "CMB_INR_cosmic_compare01__timensig_2";
    //TString source_file_name = "CMB_INR_cosmic_compare3__timensig_2_sa2000";
    
    //    TString source_file_name = "CMB_T10_supermodule_01__timensig_2_sa2000";
    //TString source_file_name = "CMB_T10_supermodule_04__timensig_2_sa2000";
    //TString source_file_name = "CMB_T10_supermodule_commtrsh__timensig_2_sa2000";
    //TString source_file_name = "CMB_T10_supermodule_chmod_04__timensig_2_sa2000";
    
    //TString source_file_name = "CMB_T10_supermodule_carousel_plus_01__timensig_2_sa2000";
    //TString source_file_name = "CMB_T10_supermodule_carousel_plus_02__timensig_2_sa2000";
    //TString source_file_name = "CMB_T10_supermodule_carousel_plus_03__timensig_2_sa2000";
    
    //TString source_file_name = "NICA_Optical_contact_all__timensig_2_sa2000";
    //TString source_file_name = "NICA_modules_calibration_1__timensig_2_sa2000";
    //TString source_file_name = "CBM_modules_calibration_2__timensig_2_sa2000";
    //TString source_file_name = "CBM_modules_calibration_gate_40_100__timensig_2_sa2000";
    //TString source_file_name = "CBM_modules_calibration_07__timensig_2_sa2000";
    //TString source_file_name = "CBM_modules_calibration_09__timensig_2_sa2000";
    
    //TString source_file_name = "NICA_results_sep_01__timensig_2_sa2000";
    //TString source_file_name = "NICA_results_sep_allruns__timensig_2_sa2000";
    //TString source_file_name = "CBM_selection_tests__timensig_2_sa1000";
    //TString source_file_name = "NICA_results_sep_all_sel_fitsignals__timensig_2_sa1000";
    //TString source_file_name = "NICA_results_sep_all_sel_fitsignals_bsight__timensig_2_sa1000";
    //TString source_file_name = "NICA_results_test_bsight__bsb2_bma4000_bsa2000";
    //TString source_file_name = "NICA_results_test_bsight__bsb3_bma8000_bsa5000";
    TString source_file_name = "CBM_results_test_bsight__bsb2_bma9000_bsa5000";
    
    TString result_file_postfix = "_fix80_test2";
    //TString result_file_postfix = "_calib3_feeex";
    //TString result_file_postfix = "_calib_T10mip";
    //TString result_file_postfix = "_calib10hz";
    
    //TString result_file_postfix = "_custom";
    //TString result_file_postfix = "_calib_custom";
    
    // ########################################################################################
    
    // Histogramms parameterd #################################################################
    const Int_t nSpectra = 9;
    TString hist_postfix[nSpectra] = {"_pedestal", "_near_right", "_near_left",  "_ample_timesel", "_near_two", "_ampl_hor", "_right_left", "_near_eqamp", "_bsight"};
    TString hist_title[nSpectra] = {"Pedestal", "Right cell", "Left cell", "Time selection", "Two sides cells", "Horizontal", "Right or left cells", "Right or left same ampl", "Back sihgt"};
    const Int_t Graph_colors[nSpectra] = {kBlack, kBlack, kViolet, kRed, kBlue, kOrange, kGreen, kMagenta, kBlack};
    
    const Int_t refer_selection = 4; //selection to draw diff
    const Int_t nGraphs = 5; //hists with no graphs goes first
    
    // ########################################################################################
    
    // Calculation constants ##################################################################
    const Double_t spectra_edge = 15000.;
    //const Double_t spectra_edge = 8000.;
    const Double_t pedescut_nsigma = 2.;
#define draw_orig_gr 0
#define draw_gr_onspcanv 0
    
    Double_t fix_calib_nPe_65[100];
    for (int i = 0; i < 100; i++)
	fix_calib_nPe_65[i] = 1. / 65.;
    //Double_t calib_nPe[50] = {0.010066, 0.010997, 0.011308, 0.011206, 0.013336, 0.011757, 0.011477, 0.010321, 0.010909, 0.014363, 0.009043, 0.009498, 0.010011, 0.011088, 0.011000, 0.009311, 0.008377, 0.008992, 0.009945, 0.011108, 0.008625, 0.008211, 0.008546, 0.009587, 0.010399, 0.008708, 0.008807, 0.008904, 0.009885, 0.010760, 0.008128, 0.008801, 0.008422, 0.008296, 0.009081, 0.008047, 0.007792, 0.007823, 0.007819, 0.009405, 0.006859, 0.006206, 0.006002, 0.008204, 0.006684, 0.006835, 0.007673, 0.006187, 0.007643, 0.006398};
    
    //feeex calib3
    //Double_t calib_nPe[50] = {0.011569, 0.011912, 0.012667, 0.011025, 0.014068, 0.012077, 0.010871, 0.009482, 0.011196, 0.016389, 0.009647, 0.009604, 0.009816, 0.011675, 0.011478, 0.009215, 0.007846, 0.009724, 0.009828, 0.011159, 0.008278, 0.008204, 0.008404, 0.011087, 0.011187, 0.008878, 0.009113, 0.008649, 0.010239, 0.012172, 0.007797, 0.008379, 0.008407, 0.008501, 0.009256, 0.007727, 0.007745, 0.008059, 0.007718, 0.009208, 0.006701, 0.005284, 0.006363, 0.006383, 0.007532, 0.008033, 0.006870, 0.006804, 0.006433, 0.007553};
    
    ////T9 calibration
    //    Float_t calib_coef_T9_array[90] = {
    ///*1*/       4078.862549, 4405.585938, 4575.482910, 3832.785400, 4247.193848, 4670.833496, 4753.293945, 5667.749512, 5020.571777, 5307.924805,
    ///*2*/        5036.793945, 5405.000000, 5403.117188, 4580.207520, 5445.171387, 6949.497559, 5405.000000, 5540.647949, 7059.627930, 7006.018066,
    ///*3*/        4168.392090, 4558.722656, 3635.833252, 3760.658447, 3708.611328, 5046.182617, 4659.461426, 4804.741211, 3206.583008, 5276.968750,
    ///*4*/        4228.919434, 3636.270020, 4416.788086, 5094.445801, 4311.268555, 4712.837402, 4415.657227, 5658.632324, 5148.392578, 5284.519531,
    ///*5*/        4137.052246, 3012.095703, 3099.355713, 3710.074219, 3745.490723, 4519.034668, 3931.458008, 5358.987793, 5395.296387, 5241.137207,
    ///*6*/        4414.908691, 3573.950928, 2970.518799, 3686.361816, 3534.961914, 4013.786621, 3587.492920, 4617.906738, 4029.712402, 3609.557129,
    ///*7*/        4420.347656, 4891.221191, 4824.579590, 4835.147949, 5162.984375, 5358.456055, 5590.303711, 5571.962402, 4795.303711, 5977.821289,
    ///*8*/        3716.259277, 4370.007324, 4497.434570, 3946.989502, 4189.361816, 5091.888184, 5819.684570, 5473.451172, 4791.077148, 6481.239746,
    ///*9*/        3623.433105, 3845.909912, 4247.372070, 4464.138184, 4445.795898, 5883.323242, 6660.334473, 6504.312012, 7108.781738, 6617.249512};
    
    //    Double_t calib_mip[50] = {
    //        /*1*/       4078.862549, 4405.585938, 4575.482910, 3832.785400, 4247.193848, 4670.833496, 4753.293945, 5667.749512, 5020.571777, 5307.924805,
    //        /*8*/        3716.259277, 4370.007324, 4497.434570, 3946.989502, 4189.361816, 5091.888184, 5819.684570, 5473.451172, 4791.077148, 6481.239746,
    //        /*4*/        4228.919434, 3636.270020, 4416.788086, 5094.445801, 4311.268555, 4712.837402, 4415.657227, 5658.632324, 5148.392578, 5284.519531,
    //        /*9*/        3623.433105, 3845.909912, 4247.372070, 4464.138184, 4445.795898, 5883.323242, 6660.334473, 6504.312012, 7108.781738, 6617.249512,
    //        /*7*/        4420.347656, 4891.221191, 4824.579590, 4835.147949, 5162.984375, 5358.456055, 5590.303711, 5571.962402, 4795.303711, 5977.821289
    //    };
    
    //T10 18 calibration
    Float_t calib_coef_T10_array[90] = {3958.815674, 4277.951660, 4123.958008, 3857.548828, 3974.188965, 3918.371826, 3811.691650, 4980.092285, 4549.606934, 5026.444824, 4319.376953,
                                        4247.344238, 4258.086914, 3744.627197, 4496.120117, 5600.403809, 4260.308594, 4111.816406, 5049.223633, 5437.295410, 2579.473877, 2603.652344, 2287.946289, 2136.926514, 2269.806396,
                                        3146.388916, 2717.178955, 3109.925537, 2979.493408, 2896.758789, 4102.378906, 3359.866699, 3581.947754, 4172.458496, 3829.394531, 3811.293701, 3653.186035, 4216.835449, 4111.963379,
                                        4267.990723, 3139.956299, 2097.376465, 2033.845825, 2765.148193, 2851.850342, 3412.271729, 2849.544434, 4152.022461, 3764.705811, 4221.978516, 4949.441895, 3726.755371, 3476.849609,
                                        4166.181641, 4027.062256, 4319.995117, 4224.491211, 5129.055176, 4226.379883, 3936.925293, 3307.514404, 4020.762695, 3618.496826, 4048.077637, 4310.086914, 3810.220459, 3833.856201,
                                        3724.991211, 3480.839355, 5156.125000, 3048.399170, 3675.618652, 2892.769043, 2992.311035, 2710.385010, 3062.729004, 3216.180176, 2695.501709, 2611.051758, 3090.907471, 3481.270752,
                                        3379.054932, 3834.524414, 3379.232178, 3538.951904, 4262.039551, 5037.883789, 4883.824707, 5328.943848, 5052.604492};
    
    //shutter Sergey
    calib_coef_T10_array[40] = 3460.283936 / 1.2;
    calib_coef_T10_array[41] = 2096.850342;
    calib_coef_T10_array[42] = 1950.428223;
    calib_coef_T10_array[43] = 2528.640137;
    calib_coef_T10_array[44] = 2685.658691;
    calib_coef_T10_array[45] = 3210.465088;
    calib_coef_T10_array[46] = 2669.266357;
    calib_coef_T10_array[47] = 4205.971191;
    calib_coef_T10_array[48] = 3653.506348;
    calib_coef_T10_array[49] = 3833.762695;
    
    Double_t LED_calib_10hz[90] = {0.009258, 0.009223, 0.009096, 0.009491, 0.011785, 0.009128, 0.008197, 0.008399, 0.007983, 0.010846, 0.009258, 0.007963, 0.008070, 0.009295, 0.009790,
                                   0.007700, 0.008753, 0.008149, 0.009331, 0.009668, 0.009567, 0.011392, 0.012038, 0.006483, 0.012059, 0.008902, 0.005489, 0.019567, 0.008676, 0.008371, 0.007876, 0.008777, 0.006406,
                                   0.008252, 0.010179, 0.008717, 0.008729, 0.008663, 0.009113, 0.009732, 0.006893, 0.008487, 0.007642, 0.007793, 0.009890, 0.007861, 0.006778, 0.006340, 0.009046, 0.009335, 0.008753,
                                   0.009042, 0.008421, 0.009154, 0.011277, 0.007035, 0.008859, 0.008556, 0.001572, 0.010678, 0.005219, 0.008856, 0.007678, 0.007699, 0.009223, 0.001388, 0.003849, 0.007978, 0.008114,
                                   0.010275, 0.002462, 0.010128, 0.008112, 0.008221, 0.008104, 0.007936, 0.006942, 0.007863, 0.007555, 0.008336, 0.009207, 0.008200, 0.008535, 0.008457, 0.010181, 0.009068, 0.007785,
                                   0.008505, 0.009425, 0.010287};
    
    Double_t calib_nPe_20_04[90] = {0.008382, 0.008715, 0.009320, 0.009278, 0.011870, 0.008563, 0.008318, 0.007707, 0.007950, 0.010377, 0.009011, 0.007973, 0.007330, 0.009032, 0.008750,
                                    0.007016, 0.008941, 0.007936, 0.008546, 0.008890, 0.005373, 0.005391, 0.007411, 0.005294, 0.007273, 0.006020, 0.005906, 0.007634, 0.006230, 0.006408, 0.007942, 0.008672, 0.006445,
                                    0.008208, 0.010589, 0.008771, 0.008593, 0.008632, 0.009292, 0.010120, 0.007725, 0.007920, 0.007514, 0.007570, 0.009247, 0.006725, 0.006772, 0.006362, 0.009357, 0.008756, 0.008356,
                                    0.008685, 0.008398, 0.008024, 0.010637, 0.007561, 0.008741, 0.008324, 0.000744, 0.010359, 0.005872, 0.008234, 0.007390, 0.007474, 0.009333, 0.005533, 0.005973, 0.007360, 0.008279,
                                    0.010568, 0.002733, 0.008931, 0.008063, 0.008603, 0.008024, 0.007751, 0.006901, 0.007534, 0.008245, 0.007914, 0.007908, 0.007863, 0.024039, 0.007486, 0.008808, 0.006504, 0.006714,
                                    0.007768, 0.008779, 0.009150};
    Double_t calib_nPe_20_04_chmod_5_9[90] = {0.008718, 0.009034, 0.009393, 0.009532, 0.011313, 0.008535, 0.008812, 0.007938, 0.008078, 0.010365, 0.008723, 0.007841, 0.007479, 0.008745,
                                              0.009162, 0.006992, 0.008938, 0.008151, 0.008505, 0.008786, 0.005373, 0.005700, 0.007542, 0.005373, 0.007421, 0.006134, 0.005528, 0.007463, 0.006172, 0.006303, 0.008143, 0.008644,
                                              0.006526, 0.008276, 0.010450, 0.008982, 0.008828, 0.008719, 0.009067, 0.009848, 0.007953, 0.007578, 0.007934, 0.008684, 0.009304, 0.007173, 0.006874, 0.006730, 0.008531, 0.008094,
                                              0.008529, 0.008831, 0.008464, 0.007938, 0.011109, 0.007826, 0.008654, 0.008468, 0.000640, 0.010215, 0.004238, 0.008113, 0.007274, 0.007127, 0.009074, 0.001758, 0.004281, 0.007597,
                                              0.008601, 0.010412, 0.001912, 0.008889, 0.007987, 0.008299, 0.008006, 0.006992, 0.007192, 0.007517, 0.008188, 0.008201, 0.007586, 0.008145, 0.007608, 0.006636, 0.009219, 0.008112,
                                              0.006924, 0.007532, 0.008714, 0.008415};
    
    Double_t calib_nPe_20_04_chmod_5_9_20hz[90] = {0.009455, 0.010279, 0.009748, 0.009890, 0.012467, 0.009828, 0.009316, 0.008036, 0.008502, 0.010959, 0.010009, 0.008341, 0.008059, 0.009095,
                                                   0.010916, 0.007922, 0.009102, 0.008668, 0.009630, 0.009921, 0.010807, 0.013071, 0.012634, 0.006510, 0.012132, 0.009198, 0.005782, 0.015874, 0.008449, 0.008684, 0.008503, 0.009556,
                                                   0.006559, 0.008482, 0.010929, 0.008824, 0.009161, 0.008992, 0.010176, 0.010705, 0.008787, 0.008634, 0.008672, 0.008793, 0.009719, 0.008127, 0.007603, 0.007523, 0.009214, 0.008909,
                                                   0.008802, 0.009137, 0.008502, 0.008738, 0.012624, 0.008016, 0.009147, 0.009221, 0.000629, 0.011059, 0.007519, 0.008610, 0.007828, 0.007581, 0.009498, 0.002029, 0.005353, 0.008440,
                                                   0.009317, 0.011374, 0.002250, 0.009760, 0.008222, 0.008779, 0.009024, 0.008434, 0.007480, 0.008167, 0.008020, 0.008693, 0.008288, 0.008734, 0.008242, 0.007090, 0.010245, 0.008640,
                                                   0.007726, 0.008531, 0.009298, 0.008667};
    calib_nPe_20_04_chmod_5_9_20hz[20] = 1. / 195.163165;
    calib_nPe_20_04_chmod_5_9_20hz[21] = 1. / 177.346423;
    calib_nPe_20_04_chmod_5_9_20hz[22] = 1. / 137.410285;
    calib_nPe_20_04_chmod_5_9_20hz[23] = 1. / 194.771768;
    calib_nPe_20_04_chmod_5_9_20hz[24] = 1. / 132.122010;
    calib_nPe_20_04_chmod_5_9_20hz[25] = 1. / 182.981390;
    calib_nPe_20_04_chmod_5_9_20hz[26] = 1. / 180.698935;
    
    calib_nPe_20_04_chmod_5_9_20hz[23] = 1. / 194.771768;
    calib_nPe_20_04_chmod_5_9_20hz[24] = 1. / 132.122010;
    calib_nPe_20_04_chmod_5_9_20hz[25] = 1. / 182.981390;
    calib_nPe_20_04_chmod_5_9_20hz[26] = 1. / 180.698935;
    
    Double_t calib_nPe_car1[90] = {0.008157, 0.008568, 0.008202, 0.009318, 0.010219, 0.008302, 0.007235, 0.008927, 0.008291, 0.009367, 0.009054, 0.009301, 0.009335, 0.009297, 0.011407,
                                   0.009045, 0.008268, 0.008696, 0.008600, 0.008090, 0.009389, 0.008467, 0.009580, 0.009507, 0.009368, 0.006280, 0.006730, 0.008319, 0.009037, 0.010021, 0.005184, 0.005855, 0.005656,
                                   0.005672, 0.006954, 0.006091, 0.005164, 0.005761, 0.006525, 0.006149, 0.009512, 0.009059, 0.008612, 0.008674, 0.010920, 0.009747, 0.009231, 0.008821, 0.010261, 0.010402, 0.007255,
                                   0.007739, 0.008225, 0.008570, 0.009797, 0.006311, 0.008036, 0.006699, 0.003372, 0.008785, 0.012016, 0.010592, 0.011792, 0.014463, 0.016803, 0.011871, 0.010412, 0.010004, 0.010277,
                                   0.013472, -0.001276, 0.008659, 0.007617, 0.007918, 0.008031, 0.005952, 0.005586, 0.006606, 0.006770, 0.007951, 0.008121, 0.009136, 0.008452, 0.010442, 0.009986, 0.007979, 0.007324,
                                   0.008113, 0.008675, 0.009624};
    Double_t calib_nPe_car2[90] = {0.007487, 0.009262, 0.009171, 0.009767, 0.010761, 0.007995, 0.007690, 0.008947, 0.008634, 0.009604, 0.008200, 0.007689, 0.007933, 0.008142, 0.009103,
                                   0.007754, 0.007150, 0.008058, 0.008934, 0.008168, 0.008429, 0.009348, 0.009775, 0.009521, 0.011539, 0.009732, 0.008831, 0.008370, 0.009083, 0.011208, 0.008616, 0.008807, 0.007452,
                                   0.010365, 0.009515, 0.007288, 0.007746, 0.008681, 0.008627, 0.009528, 0.005052, 0.006167, 0.007135, 0.005177, 0.007690, 0.006229, 0.005517, 0.004749, 0.006912, 0.005531, 0.008603,
                                   0.009182, 0.008821, 0.009846, 0.010638, 0.009564, 0.008684, 0.009171, 0.003643, 0.010634, 0.004669, 0.006820, 0.007108, 0.008119, 0.009237, 0.007264, 0.006749, 0.006844, 0.007854,
                                   0.009153, 0., 0.013183, 0.008916, 0.009212, 0.009937, 0.007866, 0.007637, 0.007201, 0.008770, 0.008035, 0.010615, 0.008681, 0.009360, 0.009729, 0.010868, 0.009677, 0.007762, 0.009124,
                                   0.007704, 0.009449};
    Double_t calib_nPe_car3[90] = {0.007542, 0.006082, 0.007090, 0.009035, 0.007617, 0.006667, 0.006449, 0.007807, 0.007027, 0.009263, 0.008984, 0.007439, 0.008206, 0.008916, 0.010151,
                                   0.008409, 0.007887, 0.008594, 0.008134, 0.009443, 0.007861, 0.007778, 0.007726, 0.008734, 0.009668, 0.007549, 0.006449, 0.008195, 0.008486, 0.008960, 0.008709, 0.008117, 0.009078,
                                   0.008456, 0.012078, 0.009048, 0.008941, 0.008411, 0.008514, 0.010687, 0.007785, 0.008444, 0.009364, 0.009504, 0.010718, 0.007813, 0.007828, 0.007728, 0.009480, 0.009756, 0.004767,
                                   0.005237, 0.007100, 0.005022, 0.007247, 0.005524, 0.005622, 0.005108, 0.003043, 0.005990, 0.014800, 0.011181, 0.010578, 0.014704, 0.013607, 0.011690, 0.009867, 0.010412, 0.010073,
                                   0.014087, 0.006768, 0.008322, 0.008493, 0.008059, 0.008070, 0.006411, 0.006541, 0.006747, 0.007561, 0.007863, 0.010345, 0.008702, 0.008413, 0.009385, 0.010489, 0.009136, 0.008300,
                                   0.008509, 0.009477, 0.010773};
    
    Double_t calib_nPe_set1[21] = {0.013563, 0.013750, 0.015296, 0.014208, 0.016605, 0.013393, 0.014419, 0.011192, 0.013723, 0.014622, 0.014378, 0.017912, 0.013725, 0.014870, 0.014255,
                                   0.014063, 0.015385, 0.016815, 0.017503, 0.013154, 0.013919};
    Double_t calib_nPe_set2[21] = {0.014740, 0.013454, 0.015058, 0.014861, 0.016161, 0.013896, 0.014610, 0.014921, 0.014004, 0.014562, 0.015585, 0.018044, 0.013167, 0.014308, 0.016022,
                                   0.014461, 0.016278, 0.017027, 0.018259, 0.013376, 0.013578};
    Double_t calib_nPe_set3[21] = {0.015487, 0.013815, 0.014933, 0.014282, 0.016361, 0.013714, 0.015073, 0.018152, 0.013669, 0.014387, 0.015267, 0.017255, 0.013242, 0.014417, 0.016345,
                                   0.013537, 0.015079, 0.016462, 0.017383, 0.012876, 0.013624};
    Double_t calib_nPe_set4[21] = {0.014348, 0.013215, 0.015071, 0.013964, 0.016318, 0.013280, 0.014610, 0.016477, 0.014039, 0.014993, 0.015217, 0.018118, 0.013945, 0.014159, 0.015868,
                                   0.013932, 0.014745, 0.014734, 0.017022, 0.012865, 0.013388};
    
    /***
	 source_file_name = "CMB_T10_supermodule_carousel_plus_09__timensig_2_sa2000";
	 
	 
	 Double_t calib_nPe_carousel[90];
	 int i = 0;
	 //10_mc1-2
	 calib_nPe_carousel[i++] = 1./110.453621;
	 calib_nPe_carousel[i++] = 1./107.511439;
	 calib_nPe_carousel[i++] = 1./107.120586;
	 calib_nPe_carousel[i++] = 1./107.555823;
	 calib_nPe_carousel[i++] = 1./87.666454;
	 calib_nPe_carousel[i++] = 1./110.557278;
	 calib_nPe_carousel[i++] = 1./120.954739;
	 calib_nPe_carousel[i++] = 1./115.001467;
	 calib_nPe_carousel[i++] = 1./116.278724;
	 calib_nPe_carousel[i++] = 1./123.604989;
	 
	 //08_mc3-4
	 calib_nPe_carousel[i++] = 1./106.505984;
	 calib_nPe_carousel[i++] = 1./118.106138;
	 calib_nPe_carousel[i++] = 1./104.383202;
	 calib_nPe_carousel[i++] = 1./105.183750;
	 calib_nPe_carousel[i++] = 1./106.749479;
	 calib_nPe_carousel[i++] = 1./159.225199;
	 calib_nPe_carousel[i++] = 1./148.581219;
	 calib_nPe_carousel[i++] = 1./120.199724;
	 calib_nPe_carousel[i++] = 1./110.652377;
	 calib_nPe_carousel[i++] = 1./99.791011;
	 
	 //07_mc5-6
	 calib_nPe_carousel[i++] = 1./192.889257;
	 calib_nPe_carousel[i++] = 1./170.781796;
	 calib_nPe_carousel[i++] = 1./176.800001;
	 calib_nPe_carousel[i++] = 1./176.315827;
	 calib_nPe_carousel[i++] = 1./143.804820;
	 calib_nPe_carousel[i++] = 1./164.172881;
	 calib_nPe_carousel[i++] = 1./193.641212;
	 calib_nPe_carousel[i++] = 1./173.576289;
	 calib_nPe_carousel[i++] = 1./153.258181;
	 calib_nPe_carousel[i++] = 1./162.621062;
	 
	 //02_mc7-8
	 calib_nPe_carousel[i++] = 1./105.125755;
	 calib_nPe_carousel[i++] = 1./110.382250;
	 calib_nPe_carousel[i++] = 1./116.119885;
	 calib_nPe_carousel[i++] = 1./115.285179;
	 calib_nPe_carousel[i++] = 1./91.573018;
	 calib_nPe_carousel[i++] = 1./102.597547;
	 calib_nPe_carousel[i++] = 1./108.329731;
	 calib_nPe_carousel[i++] = 1./113.370420;
	 calib_nPe_carousel[i++] = 1./97.456239;
	 calib_nPe_carousel[i++] = 1./96.135593;
	 
	 //05_mc9-10
	 calib_nPe_carousel[i++] = 1./137.831959;
	 calib_nPe_carousel[i++] = 1./129.213055;
	 calib_nPe_carousel[i++] = 1./121.578993;
	 calib_nPe_carousel[i++] = 1./116.685958;
	 calib_nPe_carousel[i++] = 1./102.072972;
	 calib_nPe_carousel[i++] = 1./158.460881;
	 calib_nPe_carousel[i++] = 1./124.432765;
	 calib_nPe_carousel[i++] = 1./149.274033;
	 calib_nPe_carousel[i++] = 1./296.565761;
	 calib_nPe_carousel[i++] = 1./113.836296;
	 
	 //06_mc11-12
	 calib_nPe_carousel[i++] = 1./83.219899;
	 calib_nPe_carousel[i++] = 1./94.411624;
	 calib_nPe_carousel[i++] = 1./84.801532;
	 calib_nPe_carousel[i++] = 1./69.143185;
	 calib_nPe_carousel[i++] = 1./59.514498;
	 calib_nPe_carousel[i++] = 1./84.236494;
	 calib_nPe_carousel[i++] = 1./96.045742;
	 calib_nPe_carousel[i++] = 1./99.956972;
	 calib_nPe_carousel[i++] = 1./97.302252;
	 calib_nPe_carousel[i++] = 1./74.230766;
	 
	 //09_mc19-20
	 calib_nPe_carousel[i++] = 1./-783.806903;
	 calib_nPe_carousel[i++] = 1./115.485955;
	 calib_nPe_carousel[i++] = 1./131.289910;
	 calib_nPe_carousel[i++] = 1./126.298726;
	 calib_nPe_carousel[i++] = 1./124.520779;
	 calib_nPe_carousel[i++] = 1./168.000876;
	 calib_nPe_carousel[i++] = 1./179.022955;
	 calib_nPe_carousel[i++] = 1./151.376791;
	 calib_nPe_carousel[i++] = 1./147.716913;
	 calib_nPe_carousel[i++] = 1./125.768415;
	 
	 //01_mc13-14
	 calib_nPe_carousel[i++] = 1./123.138007;
	 calib_nPe_carousel[i++] = 1./109.462222;
	 calib_nPe_carousel[i++] = 1./118.312335;
	 calib_nPe_carousel[i++] = 1./95.764844;
	 calib_nPe_carousel[i++] = 1./100.143447;
	 calib_nPe_carousel[i++] = 1./125.327856;
	 calib_nPe_carousel[i++] = 1./136.537515;
	 calib_nPe_carousel[i++] = 1./123.256910;
	 calib_nPe_carousel[i++] = 1./115.271800;
	 calib_nPe_carousel[i++] = 1./103.908858;
	 
	 //04_mc17-18
	 calib_nPe_carousel[i++] = 1./122.594515;
	 calib_nPe_carousel[i++] = 1./116.707338;
	 calib_nPe_carousel[i++] = 1./121.914271;
	 calib_nPe_carousel[i++] = 1./107.319545;
	 calib_nPe_carousel[i++] = 1./97.854657;
	 calib_nPe_carousel[i++] = 1./120.447134;
	 calib_nPe_carousel[i++] = 1./138.224930;
	 calib_nPe_carousel[i++] = 1./112.022073;
	 calib_nPe_carousel[i++] = 1./120.615377;
	 calib_nPe_carousel[i++] = 1./106.760706;
	 /****/
    
    // ########################################################################################
    printf ("\n\n\n########################################################################################\n");
    printf ("############################   COSMIC ANALYSIS RESULTS   ###############################\n");
    printf ("########################################################################################\n\\n");
    
    // Open result file #######################################################################
    TString result_file_name = source_path + source_file_name + result_file_postfix;
    std
            ::ofstream log_out ((source_path + source_file_name + result_file_postfix + "_log.txt").Data());
    
    TFile *result_file = new
            TFile ((result_file_name + ".root").Data (), "RECREATE");
    //result_file->cd();
    // ########################################################################################
    
    // Open and scan source file ##############################################################
    TFile *source_file = new
            TFile (source_path + source_file_name + ".root", "READONLY");
    if (!source_file->IsOpen ()) {
	printf ("File \"%s\" not found\n", (source_path + source_file_name + ".root").Data ());
	return;
    } else {
	printf ("File \"%s\" was opened\n", (source_path + source_file_name + ".root").Data ());
    }
    
    Int_t Total_runs = 0, modules_in_run = 0;
    Bool_t Is_file_str_uniform = true;
    
    TObjArray Module_name_arr, run_name_arr, hist_name_prefix_arr;
    Int_t Cell_Number = 999;
    
    // Main folder ------------------------------------
    TString main_folder_name = "analysis_results";
    gDirectory->cd(main_folder_name.Data());
    
    // List of files --------------------------------
    printf ("Inspecting the file:\n");
    TIter
            keyListFiles(gDirectory->GetListOfKeys());
    TKey * keyFile;
    while (keyFile = (TKey*) keyListFiles ()) {
	printf ("file [%s] : %s \n", keyFile->GetClassName (), keyFile->GetName ());
	
	if (((TString) (keyFile->GetClassName ())).Contains ("TDirectoryFile")) {
	    gDirectory->cd( keyFile->GetName() );
	    TIter
	            keyListModule(gDirectory->GetListOfKeys());
	    TKey * keyModule;
	    Int_t tot_mod_local = 0;
	    
	    // List of modules --------------------------------
	    while (keyModule = (TKey*) keyListModule ()) {
		printf ("   module [%s] : %s \n", keyModule->GetClassName (), keyModule->GetName ());
		
		if (((TString) (keyModule->GetClassName ())).Contains ("TDirectoryFile")) {
		    //skeeping runs -------------------------
		    if (
		            //		      (strcmp(keyFile->GetName(), "18_09_10_15_50_INR_12mod_NICA_TH250_ZS150_DSPoff") != 0) &&
		            //		      (strcmp(keyFile->GetName(), "18_09_11_16_20_INR_12mod_NICA_TH250_ZS150_DSPoff") != 0) &&
		            //		      (strcmp(keyFile->GetName(), "18_09_13_15_25_INR_12mod_NICA_TH200_ZS100_DSPoff") != 0) &&
		            //		      (strcmp(keyFile->GetName(), "18_09_14_00_42_INR_12mod_NICA_TH200_ZS100_DSPoff") != 0) &&
		            //		      (strcmp(keyFile->GetName(), "18_09_14_14_03_INR_12mod_NICA_TH200_ZS100_DSPoff") != 0) &&
		            //		      (strcmp(keyFile->GetName(), "18_09_17_16_13_INR_12mod_NICA_TH200_ZS100_DSPoff") != 0) &&
		            (1)) {
			tot_mod_local++;
			Module_name_arr.Add(new TObjString(keyModule->GetName()));
			run_name_arr.Add(new TObjString( Form("%s", keyFile->GetName()) ));
			hist_name_prefix_arr.Add(new TObjString( Form("%s_%s", keyFile->GetName(), keyModule->GetName()) ));
		    }
		    
		    gDirectory->cd( keyModule->GetName() );
		    TIter
		            keyListCell(gDirectory->GetListOfKeys());
		    TKey * keyCell;
		    
		    Int_t n_cell = 0;
		    while (keyCell = (TKey*) keyListCell ()) {
			printf ("      cell [%s] : %s \n", keyCell->GetClassName (), keyCell->GetName ());
			if (((TString) (keyCell->GetClassName ())).Contains ("TDirectoryFile")) {
			    n_cell++;
			}
		    }
		    if (Cell_Number != 999) if (Cell_Number != n_cell) Is_file_str_uniform = false;
		    
		    if (n_cell < Cell_Number) Cell_Number = n_cell;
		    
		    gDirectory->cd("..");
		}
	    }
	    
	    if (tot_mod_local > 0) {
		Total_runs++;
		if (modules_in_run == 0) modules_in_run = tot_mod_local;
		else if (tot_mod_local != modules_in_run) Is_file_str_uniform = false;
	    }
	    
	    gDirectory->cd("..");
	}
	
    }
    
    Int_t modules_total = Module_name_arr.GetLast ();
    
    if (Total_runs * modules_in_run != modules_total + 1) Is_file_str_uniform = false;
    //modules_total = 5;
    
    printf ("\n\nModule will be calculated:\n");
    for (Int_t module = 0; module <= modules_total; module++) {
	printf ("%s - hist name prefix \"%s\"\n", ((TObjString*) Module_name_arr.At (module))->GetString ().Data (), ((TObjString*) run_name_arr.At (module))->GetString ().Data ());
    }
    
    printf ("\n");
    printf ("Total runs found: %i\n", Total_runs);
    printf ("Total modules in run: %i\n", modules_in_run);
    printf ("Total modules: %i\n", modules_total);
    printf ("Total cells for all modules: %i\n", Cell_Number);
    printf ("File structure is uniform: %i\n\n\n", Is_file_str_uniform);
    // ########################################################################################
    
    source_file->cd ();
    
    // Initialisation ########################################################################
    
    TObjArray *result_array = new
            TObjArray;
    result_array->SetName ("Results");
    result_array->SetOwner ();
    
    TObjArray *results_hists = new
            TObjArray;
    results_hists->SetName ("Hists");
    result_array->Add (results_hists);
    
    TObjArray *canvas_array = new
            TObjArray;
    canvas_array->SetName ("Canvas");
    
    Double_t mean, sigma;
    TH1 *th1_hist_ptr = NULL;
    TH2 *th2_hist_ptr = NULL;
    TF1 *tfunc_ptr = NULL;
    TCanvas *tcanv_ptr = NULL;
    
    TLatex *lable;
    
    TCanvas *temp_canv = new
            TCanvas ("temp", "temp");
    
    gStyle->SetErrorX (0);
    
    // Results hists creation ---------------------------
    Int_t x_min = 0, x_max = 100, x_bin = 50;
    for (Int_t igraph = nSpectra - nGraphs; igraph < nSpectra; igraph++) {
	TObjArray *graph_arr = new
	        TObjArray;
	graph_arr->SetName (("results_" + hist_postfix[igraph]).Data ());
	graph_arr->SetOwner ();
	result_array->Add (graph_arr);
	
	th1_hist_ptr = new TH1F (Form ("res_hist_ampl_all_%s", hist_postfix[igraph].Data ()), Form ("Amplitudes all ch [%s]", hist_title[igraph].Data ()), x_bin, x_min, x_max);
	graph_arr->Add (th1_hist_ptr);
	
	th1_hist_ptr = new TH1F (Form ("res_hist_refdif_%s_all", hist_postfix[igraph].Data ()), Form ("Difference all ch [%s] - [%s]", hist_title[igraph].Data (), hist_title[refer_selection].Data ()),
	                         200, -100, 100);
	graph_arr->Add (th1_hist_ptr);
	
	th1_hist_ptr = new TH1F (Form ("res_hist_graph_%s", hist_postfix[igraph].Data ()), Form ("Result graph [%s]", hist_title[igraph].Data ()), Cell_Number, 0, Cell_Number);
	//th1_hist_ptr->GetYaxis()->SetRangeUser(20., 120.);
	//th1_hist_ptr->GetYaxis()->SetRangeUser(0., 60.);
	//th1_hist_ptr->GetYaxis()->SetRangeUser(0., 10.);
	th1_hist_ptr->SetMarkerStyle (22);
	th1_hist_ptr->SetMarkerColor (Graph_colors[igraph]);
	th1_hist_ptr->SetLineColor (Graph_colors[igraph]);
	th1_hist_ptr->SetStats (kFALSE);
	graph_arr->Add (th1_hist_ptr);
	
	for (Int_t icell = 0; icell < Cell_Number; icell++) {
	    th1_hist_ptr = new TH1F (Form ("res_hist_ampl_cell%i_%s", icell, hist_postfix[igraph].Data ()), Form ("Amplitudes ch %i [%s]", icell, hist_title[igraph].Data ()), x_bin, x_min, x_max);
	    graph_arr->Add (th1_hist_ptr);
	    
	    th1_hist_ptr = new TH1F (Form ("res_hist_refdif_cell%i_%s", icell, hist_postfix[igraph].Data ()), Form ("Difference ch %i [%s] - [%s]",
	                                                                                                            icell, hist_title[igraph].Data (), hist_title[refer_selection].Data ()),
	                             200, -100, 100);
	    graph_arr->Add (th1_hist_ptr);
	}
	
    }
    //---------------------------------------------------
    
    // ########################################################################################
    
    // Head canvas -------------------------------------
    tcanv_ptr = new
            TCanvas (Form ("headContent"));
    //module_data_array->Add(tcanv_ptr);
    canvas_array->Add (tcanv_ptr);
    
    TText *text = new
            TText (0.1, 0.95, "Cosmic calorimeter analysis");
    text->Draw ();
    
    //    text->DrawText(0.1,0.85, " * Horisontal selection: signal time in gate at all cells");
    //    text->DrawText(0.1,0.80, " * Right (Left): signal time in gate at rignt (left) cell;");
    //    text->DrawText(0.1,0.75, "    amplitude at rignt (left) less than treshold (now=10000)");
    //    text->DrawText(0.1,0.70, " * Two near cells: signal in gate and amplitude above");
    //    text->DrawText(0.1,0.65, "    pedestal trsh at two near cells");
    //    text->DrawText(0.1,0.60, " * Right or Left cells: Right or Left selection composition");
    //    text->DrawText(0.1,0.55, " * Right or left same ampl: signal in gate");
    //    text->DrawText(0.1,0.50, "    and same amplitude as selected cell at Right or Left cell");
    
    text->DrawText (0.1, 0.85, "Selection:");
    text->DrawText (0.1, 0.80, "1) Right or left cell has signal in time gate");
    text->DrawText (0.1, 0.75, "2) Right or left cell amplitude above threshold");
    text->DrawText (0.1, 0.70, "3) Amplitude difference between current and");
    text->DrawText (0.1, 0.65, "right or left less than 30 Ph.e.");
    
    // ------------------------------------------------
    
    // Content canvas ----------------------------------
    tcanv_ptr = new
            TCanvas (Form ("canvContent"));
    //module_data_array->Add(tcanv_ptr);
    canvas_array->Add (tcanv_ptr);
    
    lable = new
            TLatex;
    lable->SetTextAlign (12);
    lable->SetTextSize (0.03);
    lable->DrawLatex (0.1, 0.95, "Content");
    
    for (Int_t module = 0; module <= modules_total; module++) {
	lable->SetTextSize ((modules_total < 40) ? 0.02 : 0.01);
	lable->DrawLatex (
	            0.1,
	            0.92 - module * ((modules_total < 40) ? 0.02 : 0.01),
	            Form ("%s - run \"%s\"   ..... p%i", ((TObjString*) Module_name_arr.At (module))->GetString ().Data (), ((TObjString*) hist_name_prefix_arr.At (module))->GetString ().Data (),
	                  3 + module * (nGraphs + 2)));
    }
    lable->SetTextSize (0.02);
    lable->DrawLatex (0.1, 0.05, Form ("Total cells for all modules: %i", Cell_Number));
    // ------------------------------------------------
    
    // #######################################################################################
    // ######################   MAIN LOOP   ##################################################
    // #######################################################################################
    for (Int_t module_iter = 0; module_iter <= modules_total; module_iter++) {
	result_file->cd ();
	
	// Initialisation ---------------------------------
	TString module_name = ((TObjString*) Module_name_arr.At (module_iter))->GetString ();
	TString module_hist_pr_name = ((TObjString*) hist_name_prefix_arr.At (module_iter))->GetString ();
	
	TObjArray *module_data_array = new
	        TObjArray ();
	module_data_array->SetName (Form ("%s", module_name.Data ()));
	module_data_array->SetOwner ();
	result_array->Add (module_data_array);
	
	TObjArray graph_array;
	TObjArray graph_array_db; //second graph array for other coefficients
	
	Double_t *cell_pedestals = new Double_t[Cell_Number];
	Double_t *mean_val_ref = new Double_t[Cell_Number];
	memset(mean_val_ref,0,sizeof (Double_t)*Cell_Number);
	
	Int_t current_run = 0;
	Int_t current_mod_inrun = 0;
	if (Is_file_str_uniform) {
	    current_run = ceil ((module_iter + 1.) / (float) modules_in_run) - 1;
	    current_mod_inrun = module_iter - current_run * modules_in_run;
	}
	
	
	// ------------------------------------------------
	
	// ------------------------------------------------
	printf ("\n--------------------------\n");
	printf ("module name: %s\n", module_name.Data ());
	printf ("Hist name: %s\n", module_hist_pr_name.Data ());
	printf ("Run iter %i; Module iter %i\n", current_run, current_mod_inrun);
	printf ("--------------------------\n\n");
	// ------------------------------------------------
	
	// Title canvas -----------------------------------
	tcanv_ptr = new TCanvas (Form ("%s_canvTitle", module_hist_pr_name.Data ()));
	//module_data_array->Add(tcanv_ptr);
	canvas_array->Add (tcanv_ptr);
	
	lable = new
	        TLatex;
	lable->SetTextAlign (12);
	
	lable->SetTextSize (0.03);
	lable->DrawLatex (0.1, 0.7, module_hist_pr_name.Data ());
	lable->SetTextSize (0.1);
	lable->DrawLatex (0.1, 0.5, module_name.Data ());
	temp_canv->cd ();
	// ------------------------------------------------
	
	
	// Hist loop ******************************************
	for (Int_t hist_iter = 0; hist_iter < nSpectra; hist_iter++) {
	    // Initialisation ---------------------------------
	    Double_t *mean_val = new Double_t[Cell_Number];
	    Double_t *mean_err = new Double_t[Cell_Number];
	    Double_t *sigma_val = new Double_t[Cell_Number];
	    
	    if (hist_iter >= (nSpectra - nGraphs)) {
		result_file->cd ();
		tcanv_ptr = new
		        TCanvas (Form ("%s_canv%s", module_hist_pr_name.Data (), hist_postfix[hist_iter].Data ()));
		tcanv_ptr->DivideSquare (Cell_Number + 2 + (draw_gr_onspcanv ? 1 : 0), 0.01, 0.01);
		//tcanv_ptr->Divide(4,3,0.01,0.01);
		module_data_array->Add (tcanv_ptr);
		canvas_array->Add (tcanv_ptr);
		
		lable = new
		        TLatex;
		lable->SetTextAlign (12);
		
		tcanv_ptr->cd (1);
		lable->SetTextSize (0.02);
		lable->DrawLatex (0.1, 0.8, module_hist_pr_name.Data ());
		lable->SetTextSize (0.1);
		lable->DrawLatex (0.1, 0.7, module_name.Data ());
		lable->SetTextSize (0.08);
		lable->DrawLatex (0.1, 0.6, hist_title[hist_iter].Data ());
	    }
	    // ------------------------------------------------
	    
	    // Cell loop ******************************************
	    for (Int_t cell_iter = 0; cell_iter < Cell_Number; cell_iter++) {
		// Initialisation ---------------------------------
		TString hist_name_find = module_hist_pr_name + Form ("_cell%i", cell_iter) + hist_postfix[hist_iter];
		// ------------------------------------------------
		
		th1_hist_ptr = ((TH1*) source_file->FindObjectAny (hist_name_find.Data ()));
		
		// Canvas draw ------------------------------------
		source_file->cd ();
		
		if (th1_hist_ptr) {
		    result_file->cd ();
		    th1_hist_ptr = (TH1*) (th1_hist_ptr->Clone ((hist_name_find + "_").Data ()));
		    
		    tcanv_ptr->cd (cell_iter + 2);
		    
		    tfunc_ptr = NULL;
		    mean_val[cell_iter] = 0.;
		    mean_err[cell_iter] = 0.;
		    sigma_val[cell_iter] = 0.;
		    
		    if (hist_iter == 0) //pedestal hist
		    {
			tfunc_ptr = th1_hist_ptr->GetFunction (fit_funk_name);
			if (tfunc_ptr) {
			    mean = tfunc_ptr->GetParameter (1);
			    sigma = tfunc_ptr->GetParameter (2);
			    cell_pedestals[cell_iter] = mean + pedescut_nsigma * sigma;
			    cell_pedestals[cell_iter] = 2000.;
			}
			
		    } else if (hist_postfix[hist_iter].Contains ("ampl_hor")) {
			
			tfunc_ptr = th1_hist_ptr->GetFunction (fit_funk_name);
			if (tfunc_ptr) delete tfunc_ptr;
			
			tfunc_ptr = th1_hist_ptr->GetFunction ("langau");
			if (tfunc_ptr) delete tfunc_ptr;
			
			//th1_hist_ptr->Rebin((hist_iter == 2)?5:5);
			th1_hist_ptr->Rebin (5);
			//th1_hist_ptr->SetTitle(Form("Cell %i", cell_iter));
			
			/*******/
			//FitHistogrammInRMSRange(th1_hist_ptr, mean, sigma, 2.0, 2000., 15000.);
			FitHistogrammSmartInMaxPeak (th1_hist_ptr, mean, sigma, 3.5, 2000., 10000.);
			tfunc_ptr = th1_hist_ptr->GetFunction (fit_funk_name);
			/*******
			 th1_hist_ptr->GetXaxis()->SetRangeUser(cell_pedestals[cell_iter], spectra_edge);
			 mean = th1_hist_ptr->GetMean();
			 sigma = th1_hist_ptr->GetRMS();
			 Int_t nEntr = th1_hist_ptr->GetEntries();
			 th1_hist_ptr->GetXaxis()->SetRangeUser(0, spectra_edge);
			 
			 if(nEntr > 50)
			 {
			 Double_t n_sigma_fit = 1.5;
			 Double_t hist_maximum= th1_hist_ptr->GetBinCenter( th1_hist_ptr->GetMaximumBin() );
			 Double_t fit_min = hist_maximum-n_sigma_fit*sigma;
			 Double_t fit_max = hist_maximum+n_sigma_fit*sigma;
			 Double_t left_edge = (cell_pedestals[cell_iter] < fit_min)?fit_min:cell_pedestals[cell_iter];
			 //tfunc_ptr = new TF1(fit_funk_name, "gaus", left_edge, mean+n_sigma_fit*sigma);
			 tfunc_ptr = new TF1(fit_funk_name, "gaus", 0, 7000);
			 th1_hist_ptr->Fit(tfunc_ptr, "QRNLM", "");
			 }
			 /*******/
			//tfunc_ptr = th1_hist_ptr->GetFunction("langau");
			
			th1_hist_ptr->GetXaxis ()->SetRangeUser (2000, 9000);
			mean_val[cell_iter] = th1_hist_ptr->GetMean ();
			mean_err[cell_iter] = th1_hist_ptr->GetMeanError();
			sigma_val[cell_iter] = th1_hist_ptr->GetRMS();
			
			if (0) {
			    mean_val[cell_iter] = tfunc_ptr->GetParameter (1);
			    mean_err[cell_iter] = tfunc_ptr->GetParError (1);
			    sigma_val[cell_iter] = tfunc_ptr->GetParameter (2);
			}
			
		    } else if (hist_postfix[hist_iter].Contains ("near_two")) {
			
			tfunc_ptr = th1_hist_ptr->GetFunction (fit_funk_name);
			if (tfunc_ptr) delete tfunc_ptr;
			
			tfunc_ptr = th1_hist_ptr->GetFunction ("langau");
			if (tfunc_ptr) delete tfunc_ptr;
			
			//th1_hist_ptr->Rebin((hist_iter == 2)?5:5);
			th1_hist_ptr->Rebin (2);
			//th1_hist_ptr->SetTitle(hist_name_find.Data());
			
			/*******/
			//FitHistogrammInRMSRange(th1_hist_ptr, mean, sigma, 1.0, 0., 5000.);
			//printf("-----------------------------\n");
			FitHistogrammSmartInMaxPeak (th1_hist_ptr, mean, sigma, 1.0, 1500., 15000.);
			tfunc_ptr = th1_hist_ptr->GetFunction (fit_funk_name);
			/*******
			 th1_hist_ptr->GetXaxis()->SetRangeUser(cell_pedestals[cell_iter], spectra_edge);
			 mean = th1_hist_ptr->GetMean();
			 sigma = th1_hist_ptr->GetRMS();
			 Int_t nEntr = th1_hist_ptr->GetEntries();
			 th1_hist_ptr->GetXaxis()->SetRangeUser(0, spectra_edge);
			 
			 if(nEntr > 50)
			 {
			 Double_t n_sigma_fit = 1.5;
			 Double_t hist_maximum= th1_hist_ptr->GetBinCenter( th1_hist_ptr->GetMaximumBin() );
			 Double_t fit_min = hist_maximum-n_sigma_fit*sigma;
			 Double_t fit_max = hist_maximum+n_sigma_fit*sigma;
			 Double_t left_edge = (cell_pedestals[cell_iter] < fit_min)?fit_min:cell_pedestals[cell_iter];
			 //tfunc_ptr = new TF1(fit_funk_name, "gaus", left_edge, mean+n_sigma_fit*sigma);
			 tfunc_ptr = new TF1(fit_funk_name, "gaus", 0, 7000);
			 th1_hist_ptr->Fit(tfunc_ptr, "QRNLM", "");
			 }
			 /*******/
			//tfunc_ptr = th1_hist_ptr->GetFunction("langau");
			if (tfunc_ptr) {
			    mean_val[cell_iter] = tfunc_ptr->GetParameter (1);
			    mean_err[cell_iter] = tfunc_ptr->GetParError (1);
			    sigma_val[cell_iter] = tfunc_ptr->GetParameter (2);
			}
			
		    } else if (hist_postfix[hist_iter].Contains ("near_eqamp")) {
			
			tfunc_ptr = th1_hist_ptr->GetFunction (fit_funk_name);
			if (tfunc_ptr) delete tfunc_ptr;
			
			tfunc_ptr = th1_hist_ptr->GetFunction ("langau");
			if (tfunc_ptr) delete tfunc_ptr;
			
			//th1_hist_ptr->Rebin((hist_iter == 2)?5:5);
			//th1_hist_ptr->Rebin(2);
			//th1_hist_ptr->SetTitle(Form("Cell %i", cell_iter));
			
			/*******/
			//FitHistogrammInRMSRange(th1_hist_ptr, mean, sigma, 1.0, cell_pedestals[cell_iter], 5000.);
			//printf("-----------------------------\n");
			//FitHistogrammSmartInMaxPeak(th1_hist_ptr, mean, sigma, 0.5, cell_pedestals[cell_iter], 15000.);
			FitHistogrammSmartInMaxPeak (th1_hist_ptr, mean, sigma, 1.0, 0., 15000.);
			tfunc_ptr = th1_hist_ptr->GetFunction (fit_funk_name);
			/*******
			 th1_hist_ptr->GetXaxis()->SetRangeUser(cell_pedestals[cell_iter], spectra_edge);
			 mean = th1_hist_ptr->GetMean();
			 sigma = th1_hist_ptr->GetRMS();
			 Int_t nEntr = th1_hist_ptr->GetEntries();
			 th1_hist_ptr->GetXaxis()->SetRangeUser(0, spectra_edge);
			 
			 if(nEntr > 50)
			 {
			 Double_t n_sigma_fit = 1.5;
			 Double_t hist_maximum= th1_hist_ptr->GetBinCenter( th1_hist_ptr->GetMaximumBin() );
			 Double_t fit_min = hist_maximum-n_sigma_fit*sigma;
			 Double_t fit_max = hist_maximum+n_sigma_fit*sigma;
			 Double_t left_edge = (cell_pedestals[cell_iter] < fit_min)?fit_min:cell_pedestals[cell_iter];
			 //tfunc_ptr = new TF1(fit_funk_name, "gaus", left_edge, mean+n_sigma_fit*sigma);
			 tfunc_ptr = new TF1(fit_funk_name, "gaus", 0, 7000);
			 th1_hist_ptr->Fit(tfunc_ptr, "QRNLM", "");
			 }
			 /*******/
			//tfunc_ptr = th1_hist_ptr->GetFunction("langau");
			if (tfunc_ptr) {
			    mean_val[cell_iter] = tfunc_ptr->GetParameter (1);
			    mean_err[cell_iter] = tfunc_ptr->GetParError (1);
			    sigma_val[cell_iter] = tfunc_ptr->GetParameter (2);
			}
		    } else if (hist_postfix[hist_iter].Contains ("bsight")) {
			
			tfunc_ptr = th1_hist_ptr->GetFunction (fit_funk_name);
			if (tfunc_ptr) delete tfunc_ptr;
			
			tfunc_ptr = th1_hist_ptr->GetFunction ("langau");
			if (tfunc_ptr) delete tfunc_ptr;
			
			//th1_hist_ptr->Rebin((hist_iter == 2)?5:5);
			th1_hist_ptr->Rebin(2);
			//th1_hist_ptr->SetTitle(Form("Cell %i", cell_iter));
			
			/*******/
			//FitHistogrammInRMSRange(th1_hist_ptr, mean, sigma, 1.0, cell_pedestals[cell_iter], 5000.);
			//printf("-----------------------------\n");
			//FitHistogrammSmartInMaxPeak(th1_hist_ptr, mean, sigma, 0.5, cell_pedestals[cell_iter], 15000.);
			FitHistogrammSmartInMaxPeak (th1_hist_ptr, mean, sigma, 1.0, 0., 15000.);
			tfunc_ptr = th1_hist_ptr->GetFunction (fit_funk_name);
			/*******
			 th1_hist_ptr->GetXaxis()->SetRangeUser(cell_pedestals[cell_iter], spectra_edge);
			 mean = th1_hist_ptr->GetMean();
			 sigma = th1_hist_ptr->GetRMS();
			 Int_t nEntr = th1_hist_ptr->GetEntries();
			 th1_hist_ptr->GetXaxis()->SetRangeUser(0, spectra_edge);
			 
			 if(nEntr > 50)
			 {
			 Double_t n_sigma_fit = 1.5;
			 Double_t hist_maximum= th1_hist_ptr->GetBinCenter( th1_hist_ptr->GetMaximumBin() );
			 Double_t fit_min = hist_maximum-n_sigma_fit*sigma;
			 Double_t fit_max = hist_maximum+n_sigma_fit*sigma;
			 Double_t left_edge = (cell_pedestals[cell_iter] < fit_min)?fit_min:cell_pedestals[cell_iter];
			 //tfunc_ptr = new TF1(fit_funk_name, "gaus", left_edge, mean+n_sigma_fit*sigma);
			 tfunc_ptr = new TF1(fit_funk_name, "gaus", 0, 7000);
			 th1_hist_ptr->Fit(tfunc_ptr, "QRNLM", "");
			 }
			 /*******/
			//tfunc_ptr = th1_hist_ptr->GetFunction("langau");
			
			th1_hist_ptr->GetXaxis ()->SetRangeUser (2000, 9000);
			mean_val[cell_iter] = th1_hist_ptr->GetMean ();
			mean_err[cell_iter] = th1_hist_ptr->GetMeanError();
			sigma_val[cell_iter] = th1_hist_ptr->GetRMS();
			
			if (0) {
			    mean_val[cell_iter] = tfunc_ptr->GetParameter (1);
			    mean_err[cell_iter] = tfunc_ptr->GetParError (1);
			    sigma_val[cell_iter] = tfunc_ptr->GetParameter (2);
			}
		    } else {
			
			tfunc_ptr = th1_hist_ptr->GetFunction (fit_funk_name);
			if (tfunc_ptr) delete tfunc_ptr;
			
			tfunc_ptr = th1_hist_ptr->GetFunction ("langau");
			if (tfunc_ptr) delete tfunc_ptr;
			
			//th1_hist_ptr->Rebin((hist_iter == 2)?5:5);
			//th1_hist_ptr->Rebin(2);
			//th1_hist_ptr->SetTitle(Form("Cell %i", cell_iter));
			
			/*******/
			//FitHistogrammInRMSRange(th1_hist_ptr, mean, sigma, 1.0, cell_pedestals[cell_iter], 5000.);
			//printf("-----------------------------\n");
			//FitHistogrammSmartInMaxPeak(th1_hist_ptr, mean, sigma, 0.5, cell_pedestals[cell_iter], 15000.);
			FitHistogrammSmartInMaxPeak (th1_hist_ptr, mean, sigma, 1.0, 0., 15000.);
			tfunc_ptr = th1_hist_ptr->GetFunction (fit_funk_name);
			/*******
			 th1_hist_ptr->GetXaxis()->SetRangeUser(cell_pedestals[cell_iter], spectra_edge);
			 mean = th1_hist_ptr->GetMean();
			 sigma = th1_hist_ptr->GetRMS();
			 Int_t nEntr = th1_hist_ptr->GetEntries();
			 th1_hist_ptr->GetXaxis()->SetRangeUser(0, spectra_edge);
			 
			 if(nEntr > 50)
			 {
			 Double_t n_sigma_fit = 1.5;
			 Double_t hist_maximum= th1_hist_ptr->GetBinCenter( th1_hist_ptr->GetMaximumBin() );
			 Double_t fit_min = hist_maximum-n_sigma_fit*sigma;
			 Double_t fit_max = hist_maximum+n_sigma_fit*sigma;
			 Double_t left_edge = (cell_pedestals[cell_iter] < fit_min)?fit_min:cell_pedestals[cell_iter];
			 //tfunc_ptr = new TF1(fit_funk_name, "gaus", left_edge, mean+n_sigma_fit*sigma);
			 tfunc_ptr = new TF1(fit_funk_name, "gaus", 0, 7000);
			 th1_hist_ptr->Fit(tfunc_ptr, "QRNLM", "");
			 }
			 /*******/
			//tfunc_ptr = th1_hist_ptr->GetFunction("langau");
			if (tfunc_ptr) {
			    mean_val[cell_iter] = tfunc_ptr->GetParameter (1);
			    mean_err[cell_iter] = tfunc_ptr->GetParError (1);
			    sigma_val[cell_iter] = tfunc_ptr->GetParameter (2);
			}
			
		    }
		    
		    if(hist_iter == refer_selection)
		    {
			mean_val_ref[cell_iter] = mean_val[cell_iter];
		    }
		    
		    gStyle->SetTitleSize (0.1, "t");
		    TVirtualPad *pad = tcanv_ptr->cd (cell_iter + 2);
		    pad->SetLeftMargin (0.1);
		    pad->SetRightMargin (0.01);
		    pad->SetTopMargin (0.01);
		    pad->SetBottomMargin (0.1);
		    
		    th1_hist_ptr->GetXaxis ()->SetRangeUser (0, 20000);
		    th1_hist_ptr->SetTitle (Form ("Cell %i [%.1f (+/-) %.1f]", cell_iter, mean_val[cell_iter], mean_err[cell_iter]));
		    
		    th1_hist_ptr->Draw ();
		    //if(tfunc_ptr)tfunc_ptr->Draw("same");
		    
		} else {
		    printf ("histogramm for %s not found\n", hist_name_find.Data ());
		}
		// ------------------------------------------------
		
	    }
	    // Cell loop ******************************************
	    
	    // Printing fit results ---------------------------
	    if (hist_iter >= (nSpectra - nGraphs)) {
		printf ("\n%s\n", hist_title[hist_iter].Data ());
		printf ("info   ch    mean  mean_err  sigma\n");
		for (Int_t icell = 0; icell < Cell_Number; icell++) {
		    printf ("%s %i %f %f %f\n", module_hist_pr_name.Data (), module_iter * Cell_Number + icell, mean_val[icell], mean_err[icell], sigma_val[icell]);
		    log_out << Form ("%s %i %f %f %f\n", module_hist_pr_name.Data (), module_iter * Cell_Number + icell, mean_val[icell], mean_err[icell], sigma_val[icell]);
		}
	    }
	    // ------------------------------------------------
	    
	    // !!!!!!!!!!!!!!!!!!!!!!
	    
	    // Drawing graphs ---------------------------------
	    if (hist_iter >= (nSpectra - nGraphs)) {
		th1_hist_ptr = new
		        TH1F (Form ("%s_graph%s", module_hist_pr_name.Data (), hist_postfix[hist_iter].Data ()), hist_title[hist_iter].Data (), Cell_Number, 0, Cell_Number);
		th1_hist_ptr->SetTitle (th1_hist_ptr->GetName ()); /* !!! */
		
		//th1_hist_ptr->GetYaxis()->SetRangeUser(20., 120.);
		//th1_hist_ptr->GetYaxis()->SetRangeUser(0., 60.);
		//th1_hist_ptr->GetYaxis()->SetRangeUser(0., 10.);
		th1_hist_ptr->SetMarkerStyle (22);
		th1_hist_ptr->SetMarkerColor (Graph_colors[hist_iter]);
		th1_hist_ptr->SetLineColor (Graph_colors[hist_iter]);
		th1_hist_ptr->SetStats (kFALSE);
		
		TH1* th1_histres_all = ((TH1*) source_file->FindObjectAny (Form ("res_hist_ampl_all_%s", hist_postfix[hist_iter].Data ())));
		TH1* th1_refdif_all = ((TH1*) source_file->FindObjectAny (   Form ("res_hist_refdif_%s_all", hist_postfix[hist_iter].Data ())   ));
		
		Double_t cell_calib;
		
		for (Int_t icell = 0; icell < Cell_Number; icell++) {
		    Int_t cont_cell = current_mod_inrun * Cell_Number + icell;
		    
		    // cell_calib = 1./65.;
		    
		    //		    if(current_run == 0) cell_calib = calib_nPe_set1[cont_cell];
		    //		    if(current_run == 1) cell_calib = calib_nPe_set2[cont_cell];
		    //		    if(current_run == 2) cell_calib = calib_nPe_set3[cont_cell];
		    //		    if(current_run == 3) cell_calib = calib_nPe_set4[cont_cell];
		    //printf("Debug: run%i contcell%i\n", current_run, cont_cell);
		    
		    //	  cell_calib = 1. / 113.;
		    //	  if (module_name == "module_3") cell_calib = 1. / 178. / 23. * 45.;
		    //	  if (module_name == "module_9") cell_calib = 1. / 178. / 23. * 45.;
		    //	  if (module_name == "module_15") cell_calib = 1. / 178. / 23. * 45.;
		    
		    cell_calib = 1. / 80.;
		    
		    printf ("custom coeff for mod \"%s\": %f\n", module_name.Data (), cell_calib);
		    
		    th1_hist_ptr->SetBinContent (icell + 1, mean_val[icell] * cell_calib);
		    th1_hist_ptr->SetBinError (icell + 1, mean_err[icell] * cell_calib);
		    
		    if (th1_histres_all) th1_histres_all->Fill (mean_val[icell] * cell_calib);
		    TH1* th1_histres_cell = ((TH1*) source_file->FindObjectAny (Form ("res_hist_ampl_cell%i_%s", icell, hist_postfix[hist_iter].Data ())));
		    if (th1_histres_cell) th1_histres_cell->Fill (mean_val[icell] * cell_calib);
		    
		    if (th1_refdif_all) th1_refdif_all->Fill ((mean_val[icell] - mean_val_ref[icell]) * cell_calib);
		    TH1* th1_refdif_cell = ((TH1*) source_file->FindObjectAny (   Form ("res_hist_refdif_cell%i_%s", icell, hist_postfix[hist_iter].Data ())   ));
		    if (th1_refdif_cell) th1_refdif_cell->Fill ((mean_val[icell] - mean_val_ref[icell]) * cell_calib);
		    
		}
		
		Float_t max_val = th1_hist_ptr->GetBinContent (th1_hist_ptr->GetMaximumBin ());
		Float_t min_val = th1_hist_ptr->GetBinContent (th1_hist_ptr->GetMinimumBin ());
		//th1_hist_ptr->GetYaxis ()->SetRangeUser (0, max_val + 400 * cell_calib);
		th1_hist_ptr->GetYaxis ()->SetRangeUser (0, 100);
		
		graph_array.Add (th1_hist_ptr);
		results_hists->Add (th1_hist_ptr);
		tcanv_ptr->cd (Cell_Number + 2);
		if (draw_gr_onspcanv) th1_hist_ptr->Draw ();
	    }
	    // ------------------------------------------------
	    
	    // Drawing graphs with other coeffs ---------------
	    if (hist_iter >= (nSpectra - nGraphs)) {
		th1_hist_ptr = new
		        TH1F (Form ("%s_graph%s_db", module_hist_pr_name.Data (), hist_postfix[hist_iter].Data ()), hist_title[hist_iter].Data (), Cell_Number, 0, Cell_Number);
		th1_hist_ptr->SetTitle (th1_hist_ptr->GetName ()); /* !!! */
		
		th1_hist_ptr->SetMarkerStyle (22);
		th1_hist_ptr->SetMarkerColor (Graph_colors[hist_iter]);
		th1_hist_ptr->SetLineColor (Graph_colors[hist_iter]);
		th1_hist_ptr->SetStats (kFALSE);
		
		Double_t cell_calib;
		for (Int_t icell = 0; icell < Cell_Number; icell++) {
		    cell_calib = 1.;
		    
		    th1_hist_ptr->SetBinContent (icell + 1, mean_val[icell] * cell_calib);
		    th1_hist_ptr->SetBinError (icell + 1, mean_err[icell] * cell_calib);
		}
		
		Float_t max_val = th1_hist_ptr->GetBinContent (th1_hist_ptr->GetMaximumBin ());
		Float_t min_val = th1_hist_ptr->GetBinContent (th1_hist_ptr->GetMinimumBin ());
		//th1_hist_ptr->GetYaxis()->SetRangeUser(min_val-10., max_val+10);
		th1_hist_ptr->GetYaxis ()->SetRangeUser (0, max_val + 400 * cell_calib);
		//th1_hist_ptr->GetYaxis ()->SetRangeUser (0, 100);
		
		graph_array_db.Add (th1_hist_ptr);
		results_hists->Add (th1_hist_ptr);
		
	    }
	    // ------------------------------------------------
	    
	    delete[] mean_val;
	    delete[] mean_err;
	    delete[] sigma_val;
	}
	// Hist loop ******************************************
	
	TCanvas *tcanv_ptr = NULL;
	TPaveText *title = NULL;
	
	/****************************************************************************
	// Drawing hist on same canvas --------------------------
	tcanv_ptr = new TCanvas(Form("%s_samecanvhists", module_hist_pr_name.Data()));
	module_data_array->Add(tcanv_ptr);
	canvas_array->Add(tcanv_ptr);
	
	
	//	lable->SetTextAlign(12);
	//	tcanv_ptr->cd();
	
	//	lable->SetTextSize(0.02);
	//	lable->DrawLatex( 0.01, 0.99, Form("%s - run \"%s\"", ((TObjString*)Module_name_arr.At(module_iter))->GetString().Data(),
	//					 ((TObjString*)hist_name_prefix_arr.At(module_iter))->GetString().Data()) );
	
	
	
	gStyle->SetOptTitle(0);
	
	for(Int_t ihist = 0; ihist <= graph_array.GetLast(); ihist++)
	{
	    th1_hist_ptr = (TH1*)graph_array.At(ihist);
	    tcanv_ptr->cd()->SetGridy();
	    tcanv_ptr->cd()->SetTitle("Results graphs");
	    
	    th1_hist_ptr->Draw("same e1");
	    
	    tcanv_ptr->cd()->BuildLegend(0.7,0.83,1.,1.);
	}
	
	
	//TPaveLabel *title = new TPaveLabel(.11,.95,.35,.99,"brNDC");
	title = new TPaveText(.01,.95,.65,.99,"brNDC");
	title->AddText(Form("%s - run \"%s\"", ((TObjString*)Module_name_arr.At(module_iter))->GetString().Data(),
	                    ((TObjString*)hist_name_prefix_arr.At(module_iter))->GetString().Data()));
	tcanv_ptr->cd();
	title->Draw();
	tcanv_ptr->Modified(1);
	tcanv_ptr->Update();
	
	
	//	TPaveText *pt = (TPaveText*)(tcanv_ptr->cd()->GetPrimitive("title"));
	//	pt->SetTextSize(0.1);
	//	pt->SetLabel("dd");
	//	pt->Draw();
	//	tcanv_ptr->Modified();
	
	//	TPaveText *tx = new TPaveText();
	//	tx->SetLabel(Form("%s - run \"%s\"", ((TObjString*)Module_name_arr.At(module_iter))->GetString().Data(),
	//			  ((TObjString*)hist_name_prefix_arr.At(module_iter))->GetString().Data()) );
	//	tx->DrawPave(0.01,0.99,0.5,0.8, 1);
	//	tcanv_ptr->Modified(1);
	
	// ------------------------------------------------
	/****************************************************************************/
	
	
	
	/****************************************************************************
	// Drawing on add canvas --------------------------
	tcanv_ptr = new TCanvas(Form("%s_canvGraph", module_hist_pr_name.Data()));
	module_data_array->Add(tcanv_ptr);
	canvas_array->Add(tcanv_ptr);
	
	
	//	lable->SetTextAlign(12);
	//	tcanv_ptr->cd();
	
	//	lable->SetTextSize(0.02);
	//	lable->DrawLatex( 0.01, 0.99, Form("%s - run \"%s\"", ((TObjString*)Module_name_arr.At(module_iter))->GetString().Data(),
	//					 ((TObjString*)hist_name_prefix_arr.At(module_iter))->GetString().Data()) );
	
	
	
	gStyle->SetOptTitle(0);
	
	for(Int_t ihist = 0; ihist <= graph_array.GetLast(); ihist++)
	{
	    th1_hist_ptr = (TH1*)graph_array.At(ihist);
	    tcanv_ptr->cd()->SetGridy();
	    tcanv_ptr->cd()->SetTitle("Results graphs");
	    
	    th1_hist_ptr->Draw("same e1");
	    
	    tcanv_ptr->cd()->BuildLegend(0.7,0.83,1.,1.);
	}
	
	
	//TPaveLabel *title = new TPaveLabel(.11,.95,.35,.99,"brNDC");
	title = new TPaveText(.01,.95,.65,.99,"brNDC");
	title->AddText(Form("%s - run \"%s\"", ((TObjString*)Module_name_arr.At(module_iter))->GetString().Data(),
	                    ((TObjString*)hist_name_prefix_arr.At(module_iter))->GetString().Data()));
	tcanv_ptr->cd();
	title->Draw();
	tcanv_ptr->Modified(1);
	tcanv_ptr->Update();
	
	
	//	TPaveText *pt = (TPaveText*)(tcanv_ptr->cd()->GetPrimitive("title"));
	//	pt->SetTextSize(0.1);
	//	pt->SetLabel("dd");
	//	pt->Draw();
	//	tcanv_ptr->Modified();
	
	//	TPaveText *tx = new TPaveText();
	//	tx->SetLabel(Form("%s - run \"%s\"", ((TObjString*)Module_name_arr.At(module_iter))->GetString().Data(),
	//			  ((TObjString*)hist_name_prefix_arr.At(module_iter))->GetString().Data()) );
	//	tx->DrawPave(0.01,0.99,0.5,0.8, 1);
	//	tcanv_ptr->Modified(1);
	
	// ------------------------------------------------
	/****************************************************************************/
	
	//drawing graphs with two coeffd
	/****************************************************************************/
	tcanv_ptr = new
	        TCanvas (Form ("%s_canvGraph_db", module_hist_pr_name.Data ()));
	
	tcanv_ptr->Divide (draw_orig_gr ? 2 : 1, 1);
	
	module_data_array->Add (tcanv_ptr);
	canvas_array->Add (tcanv_ptr);
	
	gStyle->SetOptTitle (1);
	
	for (Int_t ihist = 0; ihist <= graph_array.GetLast (); ihist++) {
	    th1_hist_ptr = (TH1*) graph_array.At (ihist);
	    tcanv_ptr->cd (1)->SetGridy ();
	    //tcanv_ptr->cd(1)->SetTitle("Amplitude;cell;Np.e.");
	    th1_hist_ptr->SetTitle (";cell;Np.e.");
	    
	    th1_hist_ptr->Draw ("same e1");
	    
	    th1_hist_ptr = (TH1*) graph_array_db.At (ihist);
	    tcanv_ptr->cd (2)->SetGridy ();
	    //th1_hist_ptr->SetTitle("Amplitude, ch");
	    
	    if (draw_orig_gr) th1_hist_ptr->Draw ("same e1");
	}
	
	//TPaveLabel *title = new TPaveLabel(.11,.95,.35,.99,"brNDC");
	title = new
	        TPaveText (.01, .95, .65, .99, "brNDC");
	title->AddText (
	            Form ("%s - run \"%s\"", ((TObjString*) Module_name_arr.At (module_iter))->GetString ().Data (), ((TObjString*) hist_name_prefix_arr.At (module_iter))->GetString ().Data ()));
	tcanv_ptr->cd ();
	title->Draw ();
	tcanv_ptr->Modified (1);
	tcanv_ptr->Update ();
	
	//	TPaveText *pt = (TPaveText*)(tcanv_ptr->cd()->GetPrimitive("title"));
	//	pt->SetTextSize(0.1);
	//	pt->SetLabel("dd");
	//	pt->Draw();
	//	tcanv_ptr->Modified();
	
	//	TPaveText *tx = new TPaveText();
	//	tx->SetLabel(Form("%s - run \"%s\"", ((TObjString*)Module_name_arr.At(module_iter))->GetString().Data(),
	//			  ((TObjString*)hist_name_prefix_arr.At(module_iter))->GetString().Data()) );
	//	tx->DrawPave(0.01,0.99,0.5,0.8, 1);
	//	tcanv_ptr->Modified(1);
	
	// ------------------------------------------------
	/****************************************************************************/
	
	delete[] cell_pedestals;
	delete[] mean_val_ref;
    }
    // #######################################################################################
    // #######################################################################################
    // #######################################################################################
    
    // Calculating results hists #############################################################
    for (Int_t igraph = nSpectra - nGraphs; igraph < nSpectra; igraph++) {
	
	TH1* th1_graphres = ((TH1*) source_file->FindObjectAny (Form ("res_hist_graph_%s", hist_postfix[igraph].Data ())));
	if (th1_graphres) {
	    for (Int_t icell = 0; icell < Cell_Number; icell++) {
		th1_hist_ptr = ((TH1*) source_file->FindObjectAny (Form ("res_hist_ampl_cell%i_%s", icell, hist_postfix[igraph].Data ())));
		if (th1_hist_ptr) {
		    Float_t cell_value = th1_hist_ptr->GetMean ();
		    Float_t cell_rms = th1_hist_ptr->GetMeanError ();
		    th1_graphres->SetBinContent (icell + 1, cell_value);
		    th1_graphres->SetBinError (icell + 1, cell_rms);
		}
	    }
	}
    }
    
    // #######################################################################################
    
    // Drawing results hists #################################################################
    
    for (Int_t igraph = nSpectra - nGraphs; igraph < nSpectra; igraph++) {
	
	tcanv_ptr = new
	        TCanvas (Form ("canvresults_%s", hist_title[igraph].Data ()));
	tcanv_ptr->DivideSquare (Cell_Number + 2);
	canvas_array->Add (tcanv_ptr);
	
	TH1* th1_graphres = ((TH1*) source_file->FindObjectAny (Form ("res_hist_graph_%s", hist_postfix[igraph].Data ())));
	if (th1_graphres) {
	    tcanv_ptr->cd (1);
	    th1_graphres->Draw ();
	}
	TH1* th1_histres = ((TH1*) source_file->FindObjectAny (Form ("res_hist_ampl_all_%s", hist_postfix[igraph].Data ())));
	if (th1_histres) {
	    tcanv_ptr->cd (2);
	    th1_histres->Draw ();
	}
	
	for (Int_t icell = 0; icell < Cell_Number; icell++) {
	    th1_hist_ptr = ((TH1*) source_file->FindObjectAny (Form ("res_hist_ampl_cell%i_%s", icell, hist_postfix[igraph].Data ())));
	    if (th1_hist_ptr) {
		tcanv_ptr->cd (3 + icell);
		th1_hist_ptr->Draw ();
	    }
	}
    }
    
    // #######################################################################################
    
    
    // Drawing refdif hists ##################################################################
    for (Int_t igraph = nSpectra - nGraphs; igraph < nSpectra; igraph++) {
	
	tcanv_ptr = new TCanvas (Form ("canvrefdif_%s", hist_title[igraph].Data ()));
	tcanv_ptr->DivideSquare (Cell_Number + 1);
	canvas_array->Add (tcanv_ptr);
	
	TH1* th1_histres = ((TH1*) source_file->FindObjectAny (Form ("res_hist_refdif_%s_all", hist_postfix[igraph].Data ())));
	if (th1_histres) {
	    tcanv_ptr->cd (1);
	    th1_histres->Draw ();
	}
	
	for (Int_t icell = 0; icell < Cell_Number; icell++) {
	    th1_hist_ptr = ((TH1*) source_file->FindObjectAny (Form ("res_hist_refdif_cell%i_%s", icell, hist_postfix[igraph].Data ())));
	    if (th1_hist_ptr) {
		tcanv_ptr->cd (2 + icell);
		th1_hist_ptr->Draw ();
	    }
	}
    }
    // #######################################################################################
    
    
    // Drawing pdf ###########################################################################
    if (1) {
	
	for (Int_t i = 0; i < canvas_array->GetLast (); i++) {
	    ((TCanvas*) canvas_array->At (i))->SaveAs ((result_file_name + ".pdf(").Data ());
	}
	((TCanvas*) canvas_array->At (canvas_array->GetLast ()))->SaveAs ((result_file_name + ".pdf)").Data ());
    }
    // #######################################################################################
    
    // Saving results ########################################################################
    SaveArrayStructInFile (result_array, result_file);
    result_file->Close ();
    printf ("Result file: %s was writed\n", (result_file_name + ".root").Data ());
    // #######################################################################################
    
    // Terminating ###########################################################################
    delete result_file;
    delete temp_canv;
    
    source_file->Close ();
    delete source_file;
    
    delete result_array;
    // #######################################################################################
}
