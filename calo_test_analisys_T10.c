

#include <fstream>
#include <iostream>
#include <TNtuple.h>
#include <TFile.h>
#include <TChain.h>
#include <Rtypes.h>
#include <vector>
#include <TBrowser.h>
#include <TH1.h>
#include <TMath.h>

#include "Readme_reader.h"
#include "event_data_struct.h"
#include "bindataformat.h"
#include "../libraries/utilites/utilites.h"
#include "../digdataprocessing/DigVariables.cpp"

void calo_test_analisys()
{
    ////////////////////////////////////////////////////////////////////////////////////////

    TString source_path = "/mnt/hgfs/DETECTORS_DATA/RESULTS/CALO/root_source/T10_test_sep_17/";
    TString result_path = "/mnt/hgfs/DETECTORS_DATA/RESULTS/CALO/root_results/T10_test_sep_17/";

    //    TString run_file_name = "adc64_17_09_08_20_50_protonScan_mod5_4GeV";
    //TString run_file_name = "adc64_17_09_11_15_18_muonScan_mod3_6GeV";
    //TString run_file_name = "adc64_17_09_11_12_10_muonScan_mod5_6GeV_test";


	Float_t part_time_sel_val[2] = {-200, 300};
	TString part_sel_comment = "pions";


//    TString run_file_name = "adc64_17_09_08_13_45_protonScan_mod5_6GeV";
//    Float_t part_time_sel_val[2] = {300, 700}; //6Gev
//    TString part_sel_comment = "protons";

//    TString run_file_name = "adc64_17_09_08_16_22_protonScan_mod5_5.5GeV";
//	Float_t part_time_sel_val[2] = {400, 800}; //5.5gev
//	TString part_sel_comment = "protons";

//    TString run_file_name = "adc64_17_09_08_18_11_protonScan_mod5_5GeV";
//	Float_t part_time_sel_val[2] = {500, 900}; //5gev
//	TString part_sel_comment = "protons";

//    TString run_file_name = "adc64_17_09_08_19_30_protonScan_mod5_4.5GeV";
//	Float_t part_time_sel_val[2] = {600, 1000}; //4.5gev
//	TString part_sel_comment = "protons";

//    TString run_file_name = "adc64_17_09_08_20_50_protonScan_mod5_4GeV";
//	Float_t part_time_sel_val[2] = {800, 1300}; //4gev
//	TString part_sel_comment = "protons";

//    TString run_file_name = "adc64_17_09_08_22_55_protonScan_mod5_3.5GeV";
//	Float_t part_time_sel_val[2] = {1100, 1600}; //3.5gev
//	TString part_sel_comment = "protons";

//    TString run_file_name = "adc64_17_09_09_02_20_protonScan_mod5_3.0GeV";
//	Float_t part_time_sel_val[2] = {1200, 2200}; //3.gev
//	TString part_sel_comment = "protons";

//    TString run_file_name = "adc64_17_09_09_17_00_protonScan_mod5_2.5GeV";
//	Float_t part_time_sel_val[2] = {2300, 2900}; //2.5gev
//	TString part_sel_comment = "protons";

//    TString run_file_name = "adc64_17_09_09_11_15_protonScan_mod5_2.0GeV";
//	Float_t part_time_sel_val[2] = {3500, 4500}; //2gev
//	TString part_sel_comment = "protons";

//    TString run_file_name = "adc64_17_09_12_03_54_protonScan_mod5_1.5GeV";
//	Float_t part_time_sel_val[2] = {6500, 8000}; //1.5gev
//	TString part_sel_comment = "protons";


    //TString run_file_name = "adc64_17_09_07_14_22_protonScan_mod5_6GeV";
    
    // TString run_file_name = "adc64_17_09_11_11_12_protonScan_mod5_6GeV_test";
	
	
TString run_file_name = "adc64_17_09_08_13_45_protonScan_mod5_6GeV";
TString result_file_name = run_file_name+"_coeffcalib_protons_1";

    //    TString run_file_name = "adc64_17_09_10_09_00_positionScan_mod54_0cm_6GeV";
    //    TString run_file_name = "adc64_17_09_10_09_00_positionScan_mod54_3cm_6GeV";
    //    TString run_file_name = "adc64_17_09_10_11_21_positionScan_mod54_6cm_6GeV";
    //	TString run_file_name = "adc64_17_09_10_12_29_positionScan_mod54_9cm_6GeV";
    //TString run_file_name = "adc64_17_09_10_13_47_positionScan_mod54_12cm_6GeV";
    //	TString run_file_name = "adc64_17_09_10_15_15_positionScan_mod54_14cm_6GeV";

    //	TString run_file_name = "adc64_17_09_11_17_45_protonScan_mod4_6GeV";

    //    TString result_file_name = run_file_name+"_shutcalib_3";
    // TString result_file_name = run_file_name+"_protoncalib_8_tof_timegate_cellped";
    //	TString result_file_name = run_file_name+"_notimesel_3";
	

    ////////////////////////////////////////////////////////////////////////////////////////


    /************************************************/
    const Int_t min_charge = -1000;
    const Int_t max_charge = 10000;
    const Int_t max_charge_bin = 50;
    const Int_t min_energy = -100;
    const Int_t max_energy = 500;
    const Int_t max_energy_bin = 1200;
    const Int_t pedestal_charge_cut_trsh = 1500;

    const Float_t mev_in_mip = 5.;

    UChar_t signal_time_gate[2] = {70, 90};




    //Float_t part_time_sel_val[2] = {-10000, 10000};
    //TString part_sel_comment = "notimesel_3";

    //Float_t muons_calib_coef[90] = {1.000000, 1.000000, 1.000000, 1.000000, 1.000000, 1.000000, 1.000000, 1.000000, 1.000000, 1.000000, 1.000000, 1.000000, 1.000000, 1.000000, 1.000000, 1.000000, 1.000000, 1.000000, 1.000000, 1.000000, 1.000000, 1.000000, 1.000000, 1.000000, 1.000000, 1.000000, 1.000000, 1.000000, 1.000000, 1.000000, 1.000000, 1.000000, 1.000000, 1.000000, 1.000000, 1.000000, 1.000000, 1.000000, 1.000000, 1.000000, 3209.284912, 2690.020508, 2436.337158, 3097.197021, 2913.517822, 3308.166992, 3072.371582, 3356.224121, 3709.115723, 3287.973877, 1.000000, 1.000000, 1.000000, 1.000000, 1.000000, 1.000000, 1.000000, 1.000000, 1.000000, 1.000000, 1.000000, 1.000000, 1.000000, 1.000000, 1.000000, 1.000000, 1.000000, 1.000000, 1.000000, 1.000000, 1.000000, 1.000000, 1.000000, 1.000000, 1.000000, 1.000000, 1.000000, 1.000000, 1.000000, 1.000000, 1.000000, 1.000000, 1.000000, 1.000000, 1.000000, 1.000000, 1.000000, 1.000000, 1.000000, 1.000000};

    //muons from protons beam
    Float_t muons_calib_coef[90] = {3602.727783, 4007.175781, 4062.768066, 3225.132080, 3581.253906, 3335.232910, 3795.120605, 4002.737305, 4053.151367, 4061.735840, 3835.261719, 3985.709961, 3869.251709, 3539.825439, 3962.516113, 4638.244141, 3935.250244, 3459.670410, 4460.634766, 5011.980957, 3729.739990, 3711.763428, 3376.184082, 2981.195312, 2995.848145, 3954.683594, 3728.812256, 3774.869873, 3894.516846, 3939.953613, 3297.368652, 2475.572021, 3237.176270, 3731.845459, 3257.445068, 2921.943604, 2932.668945, 3975.673828, 3411.766113, 3848.850098, 3577.103027, 2663.717041, 2658.060059, 3032.076904, 2955.073975, 3372.012207, 2911.863037, 3339.845459, 3367.746094, 3204.975830, 3705.559326, 2829.702637, 2340.188965, 2846.352539, 2678.420410, 2999.971924, 2613.679199, 3276.391846, 2797.269287, 2444.619873, 3556.812988, 3866.193848, 3616.567627, 3929.845947, 3775.548340, 3856.564697, 3774.126709, 3949.892578, 3425.570557, 4266.839844, 3260.697266, 3371.194580, 3517.864014, 3119.370117, 3037.343506, 3737.174561, 3882.688232, 3533.553223, 2927.256592, 3734.514404, 3139.055664, 3060.497314, 3377.706543, 2976.231201, 2990.233154, 3599.487061, 4448.974609, 3878.017578, 4123.644043, 4146.499512};


    //muons with shutter
    //Float_t muons_calib_coef[90] = {3598.150146, 3654.299072, 4099.895508, 3172.405518, 3813.168701, 3306.803467, 3904.533691, 4303.776367, 4410.951660, 4192.466797, 3609.238770, 3744.007324, 3918.235596, 3578.269287, 3889.962646, 5164.469727, 4580.703613, 4085.293945, 5146.818359, 5651.831543, 3357.832275, 3613.627197, 3206.332031, 2950.551270, 2995.891113, 3926.096680, 3892.563721, 3979.494385, 4059.367676, 4139.528809, 2950.075684, 2379.156982, 3066.632080, 3518.554932, 3290.194824, 2900.177490, 2875.914551, 3897.685791, 3394.433350, 3766.085693, 3264.187256, 2420.644531, 2519.123779, 3189.929688, 2809.710938, 3308.396240, 2874.966309, 3542.995361, 3425.986572, 3228.842285, 3097.137207, 2613.957031, 2230.320801, 2599.505615, 2506.487305, 2904.752686, 2548.666016, 3077.928955, 2818.947998, 2413.526367, 3337.867188, 3792.040283, 3598.449707, 3703.684570, 3769.366211, 3980.087402, 3982.571777, 4181.272949, 3568.820557, 4561.024902, 3099.008301, 3106.931396, 3094.910156, 2835.708984, 2795.911133, 3798.635986, 3807.587402, 3852.854248, 3034.866455, 3639.432373, 2635.123779, 2902.787354, 2983.515381, 2906.557129, 3217.940674, 3799.431152, 4627.056152, 4296.592773, 4670.774414, 4337.978027};
    /************************************************/
    //Readme_reader *readme_reader_ptr = new Readme_reader("/mnt/hgfs/DETECTORS_DATA/SOURCE/CALO/T10_RUNS/");
    //readme_reader_ptr->Set_runfile_name("adc64_17_09_07_14_22_protonScan_mod5_6GeV");
    Readme_reader *readme_reader_ptr = new Readme_reader("");
    readme_reader_ptr->SetInfoFile(source_path + run_file_name +".txt");
    readme_reader_ptr->Print();

    Int_t total_modules = readme_reader_ptr->Get_module_num();
    Int_t total_channels = readme_reader_ptr->Get_total_ch();


    result_file_name += "_" + part_sel_comment;
    TString hist_comment = readme_reader_ptr->Get_run_comment() + " {" + part_sel_comment + "}";









    TObjArray *result_array = new TObjArray;
    result_array->SetName(readme_reader_ptr->Get_run_name().Data());
    result_array->SetOwner();

    TObjArray *commhist_array = new TObjArray;
    commhist_array->SetName("calo_results");
    commhist_array->SetOwner();
    result_array->Add(commhist_array);



    // Histogramm creation ==========================================

    TH1 *th1_hist_ptr = NULL;
    TH2 *th2_hist_ptr = NULL;

    th1_hist_ptr = new TH1F("part_time", Form("Particles start TOF %s [%.1f, %.1f]; ps", hist_comment.Data(), part_time_sel_val[0], part_time_sel_val[1]),
	    2000, -10000, 10000);
    commhist_array->Add(th1_hist_ptr);

    th1_hist_ptr = new TH1F("part_time_rej", Form("Particles rejected TOF %s [%.1f, %.1f]; ps", hist_comment.Data(), part_time_sel_val[0], part_time_sel_val[1]),
	    2000, -10000, 10000);
    commhist_array->Add(th1_hist_ptr);

    th2_hist_ptr = new TH2F("part_time_energ", Form("Energy vs TOF %s;MeV;ns", hist_comment.Data()),
			    600,0,600, 1100, -1000, 10000);
    commhist_array->Add(th2_hist_ptr);


    for(Int_t icell = 1; icell <= readme_reader_ptr->Get_module_info()[0].total_cells; icell++)
    {
	th1_hist_ptr = new TH1F(Form("calo_energy_first_%i",icell), "temp",
				max_energy_bin, 5*min_energy, 5*max_energy);
	commhist_array->Add(th1_hist_ptr);
    }



    for(Int_t module_iter = 0; module_iter < total_modules; module_iter++)
    {
	Int_t total_cells = readme_reader_ptr->Get_module_info()[module_iter].total_cells;
	TString module_name = readme_reader_ptr->Get_module_info()[module_iter].comment;
	TObjArray *module_data_array = new TObjArray();
	module_data_array->SetName(module_name.Data());
	module_data_array->SetOwner();
	result_array->Add(module_data_array);

	for(Int_t icell = 1; icell <= total_cells; icell++)
	{
	    th1_hist_ptr = new TH1F(Form("%s_module_energy_first_%i",module_name.Data(), icell),
				    Form("Total module energy %s [first %i cell] %s; MeV", module_name.Data(), icell, hist_comment.Data()),
				    max_energy_bin, min_energy, max_energy);
	    module_data_array->Add(th1_hist_ptr);
	}

	th1_hist_ptr = new TH1F(Form("%s_module_trace",module_name.Data()),
				Form("Module trace %s %s; cell; MeV;", module_name.Data(), hist_comment.Data()),
				total_cells, 0, total_cells);
	module_data_array->Add(th1_hist_ptr);


	th2_hist_ptr = new TH2F(Form("%s_module_halfversus", module_name.Data()),
				Form("Summ charge firs vs last cells %s %s; summ first; summ last;", module_name.Data(), hist_comment.Data()),
				250, -10, 200, 250, -10, 200);
	module_data_array->Add(th2_hist_ptr);


	th2_hist_ptr = new TH2F(Form("%s_module_halfversus_sel",module_name.Data()),
				Form("Summ charge firs vs last cells [selection] %s %s; summ first, MeV; summ last, MeV;", module_name.Data(), hist_comment.Data()),
				250, -10, 200, 250, -10, 200);
	module_data_array->Add(th2_hist_ptr);


	for(Int_t cell_iter = 0; cell_iter < total_cells; cell_iter++)
	{
	    th1_hist_ptr = new TH1F(Form("%s_cell_%i_energy",module_name.Data(), cell_iter),
				    Form("mod %i cell %i; MeV", module_iter+1, cell_iter+1),
				    100, -25, 175);
	    module_data_array->Add(th1_hist_ptr);

	    th1_hist_ptr = new TH1F(Form("%s_cell_%i_charge_muons",module_name.Data(), cell_iter), "temp",
				    max_charge_bin, min_charge, max_charge);
	    module_data_array->Add(th1_hist_ptr);

	}


    }

    // ==============================================================



    // Source & Result file =================
    //    TFile *source_file = new TFile((source_path + run_file_name+".root").Data(), "READONLY");
    //    if(!source_file->IsOpen())
    //    {
    //	printf("File \"%s\" not found\n", (source_path + run_file_name+".root").Data());
    //	return;
    //    }
    //    else
    //    {
    //	printf("File \"%s\" was opened\n", (source_path + run_file_name+".root").Data());
    //    }

    //    TTree *data_tree = dynamic_cast<TTree*>(source_file->FindObjectAny("adc64_data"));
    //    if(!data_tree)
    //    {
    //	printf("data tree not found in file\n");
    //	return;
    //    }
    //    gDirectory->cd("Rint:/");
    // =============================
    TChain *data_tree = new TChain;
    data_tree->AddFile(source_path + run_file_name + ".root/adc64_data");

//    	data_tree->AddFile(source_path + "adc64_17_09_08_13_45_protonScan_mod5_6GeV.root/adc64_data");
//    	data_tree->AddFile(source_path + "adc64_17_09_10_16_38_protonScan_mod1_6GeV.root/adc64_data");
//    	data_tree->AddFile(source_path + "adc64_17_09_10_18_01_protonScan_mod2_6GeV.root/adc64_data");
//    	data_tree->AddFile(source_path + "adc64_17_09_10_19_40_protonScan_mod3_6GeV.root/adc64_data");
//    	data_tree->AddFile(source_path + "adc64_17_09_10_21_07_protonScan_mod6_6GeV.root/adc64_data");
//    	data_tree->AddFile(source_path + "adc64_17_09_10_22_33_protonScan_mod7_6GeV.root/adc64_data");
//    	data_tree->AddFile(source_path + "adc64_17_09_10_23_59_protonScan_mod8_6GeV.root/adc64_data");
//    	data_tree->AddFile(source_path + "adc64_17_09_11_01_28_protonScan_mod9_6GeV.root/adc64_data");
//    	data_tree->AddFile(source_path + "adc64_17_09_11_17_45_protonScan_mod4_6GeV.root/adc64_data");

    //	data_tree->AddFile(source_path + "adc64_17_09_11_15_18_muonScan_mod3_6GeV.root/adc64_data");
    //	data_tree->AddFile(source_path + "adc64_17_09_08_02_59_muonScan_mod4_6GeV.root/adc64_data");
    //	data_tree->AddFile(source_path + "adc64_17_09_11_20_09_muonScan_mod1_6GeV.root/adc64_data");
    //	data_tree->AddFile(source_path + "adc64_17_09_11_22_45_muonScan_mod2_6GeV.root/adc64_data");
    //	data_tree->AddFile(source_path + "adc64_17_09_12_01_20_muonScan_mod7_6GeV.root/adc64_data");
    //	data_tree->AddFile(source_path + "adc64_17_09_08_11_31_muonScan_mod8_6GeV.root/adc64_data");
    //	data_tree->AddFile(source_path + "adc64_17_09_08_08_10_muonScan_mod9_6GeV.root/adc64_data");
    //	data_tree->AddFile(source_path + "adc64_17_09_08_05_10_muonScan_mod6_6GeV.root/adc64_data");
    //	data_tree->AddFile(source_path + "adc64_17_09_11_12_10_muonScan_mod5_6GeV_test.root/adc64_data");




    // =============================

    event_data_struct *event_struct = new event_data_struct[total_channels];
    for(Int_t ch = 0; ch < total_channels; ch++)
    {
	//event_struct[ch].SetBranch(data_tree, ch);
	data_tree->SetBranchAddress((event_struct+ch)->GetChName(ch).Data(), (event_struct+ch));
	//printf("branch %s\n", (event_struct+ch)->GetChName(ch).Data());
    }

    MAIN_signal PMT1signal, PMT2signal;
    data_tree->SetBranchAddress("main_PMT01", &PMT1signal);
    data_tree->SetBranchAddress("main_PMT02", &PMT2signal);



    Int_t enery_trace_collected = 0;
    Float_t *calo_energy_summ = new Float_t[readme_reader_ptr->Get_module_info()[0].total_cells];
    Bool_t is_mod5_fired;

    for(Int_t entry = 0; entry <= data_tree->GetEntries(); entry++)
    {
	data_tree->GetEntry(entry);

	Float_t PMT_TOF = (PMT2signal.Time_pol1 - PMT1signal.Time_pol1)*1000.;
	Bool_t Is_TOF_in_reg = (PMT_TOF >= part_time_sel_val[0])&&(PMT_TOF <= part_time_sel_val[1]);

	if(Is_TOF_in_reg)
	{
	    th1_hist_ptr = ((TH1*)(gDirectory->FindObjectAny("part_time")));
	    th1_hist_ptr->Fill(PMT_TOF);
	}
	else
	{
	    th1_hist_ptr = ((TH1*)(gDirectory->FindObjectAny("part_time_rej")));
	    th1_hist_ptr->Fill(PMT_TOF);
	}

	for(Int_t icell = 0; icell < 10; icell++)calo_energy_summ[icell] = 0.;

	//if(!Is_TOF_in_reg) continue;



	Float_t calo_energy_summ_allpart = 0.;
	for(Int_t module_iter = 0; module_iter < total_modules; module_iter++)
	{
	    TString module_name = readme_reader_ptr->Get_module_info()[module_iter].comment;
	    Int_t total_cells = readme_reader_ptr->Get_module_info()[module_iter].total_cells;

	    Float_t module_charge_first_summ = 0;
	    Float_t module_charge_last_summ = 0;
	    Float_t module_charge_sqsumm = 0;
	    Int_t n_module_noize_rej = 9;
	    for(Int_t cell_iter = 0; cell_iter < total_cells; cell_iter++)
	    {
		Int_t tree_ch = readme_reader_ptr->GetContCh_from_CellN(module_iter, cell_iter);
		Int_t charge_cell = event_struct[tree_ch].integral_in_gate;
		Float_t energy_cell = (Float_t)charge_cell / muons_calib_coef[tree_ch] * mev_in_mip;
		UChar_t time_cell = event_struct[tree_ch].time_max_in_gate;
		Bool_t is_signal_in_gate = (time_cell > signal_time_gate[0])&&(time_cell < signal_time_gate[1]);
		Bool_t is_energy_in_range = (energy_cell > 0.5*mev_in_mip);

		if(!is_signal_in_gate) continue;
		//printf("[cell%i: %i sel %i] ",cell_iter, charge_cell, is_charge_in_range);
		//		if(!is_charge_in_range) continue;

		if(cell_iter < n_module_noize_rej) module_charge_sqsumm += (Float_t)energy_cell*(Float_t)energy_cell;

		if(cell_iter < 5)
		{
		    module_charge_first_summ += (Float_t)energy_cell;
		}
		else
		{
		    module_charge_last_summ += (Float_t)energy_cell;
		}
	    }// cell
	    module_charge_sqsumm = TMath::Sqrt(module_charge_sqsumm);
	    //printf("first %f last %f \n", module_charge_first_summ, module_charge_last_summ);


	    Bool_t is_all_cells_nonoize = module_charge_sqsumm > TMath::Sqrt(n_module_noize_rej)*mev_in_mip*0.85;
	    if((module_iter == 4)) is_mod5_fired = is_all_cells_nonoize;
	    Bool_t muon_selection = TMath::Abs(module_charge_first_summ - module_charge_last_summ) < 15.;
	    muon_selection = muon_selection &&(module_charge_last_summ > 14.);
	    muon_selection = muon_selection &&(module_charge_last_summ < 45.);
	    if(Is_TOF_in_reg)
		if(is_all_cells_nonoize)
		{
		    th2_hist_ptr = ((TH2*)(gDirectory->FindObjectAny(Form("%s_module_halfversus",module_name.Data()) )));
		    th2_hist_ptr->Fill(module_charge_first_summ, module_charge_last_summ);
		    if(muon_selection)
		    {
			th2_hist_ptr = ((TH2*)(gDirectory->FindObjectAny(Form("%s_module_halfversus_sel",module_name.Data()) )));
			th2_hist_ptr->Fill(module_charge_first_summ, module_charge_last_summ);
		    }
		}





	    Float_t module_energy_summ = 0;
	    for(Int_t cell_iter = 0; cell_iter < total_cells; cell_iter++)
	    {
		Int_t tree_ch = readme_reader_ptr->GetContCh_from_CellN(module_iter, cell_iter);
		TString channel_name = event_data_struct::GetChName(tree_ch);

		Int_t charge_cell = event_struct[tree_ch].integral_in_gate;
		Float_t energy_cell = (Float_t)charge_cell / muons_calib_coef[tree_ch] * mev_in_mip;
		//Float_t energy_cell = (Float_t)charge_cell * 1.;
		UChar_t time_cell = event_struct[tree_ch].time_max_in_gate;
		Bool_t is_signal_in_gate = (time_cell > signal_time_gate[0])&&(time_cell < signal_time_gate[1]);
		Bool_t is_energy_in_range = (energy_cell > .5*mev_in_mip);

		if(Is_TOF_in_reg)
		    if(is_signal_in_gate)
			if(is_all_cells_nonoize)
			    if(muon_selection)
			    {
				th1_hist_ptr = ((TH1*)(gDirectory->FindObjectAny( Form("%s_cell_%i_charge_muons",module_name.Data(), cell_iter) )));
				th1_hist_ptr->Fill(charge_cell);
			    }

		if(Is_TOF_in_reg)
		    if(is_signal_in_gate)
			if(is_all_cells_nonoize)
			    if(!muon_selection)
			    {
				module_energy_summ += energy_cell;

				th1_hist_ptr = ((TH1*)(gDirectory->FindObjectAny(Form("%s_cell_%i_energy",module_name.Data(), cell_iter))));
				th1_hist_ptr->Fill(energy_cell);

				th1_hist_ptr = ((TH1*)(gDirectory->FindObjectAny( Form("%s_module_trace",module_name.Data()) )));
				Float_t bin_content = th1_hist_ptr->GetBinContent(cell_iter+1);
				bin_content += energy_cell;
				th1_hist_ptr->SetBinContent(cell_iter+1, bin_content);
			    }

		if(Is_TOF_in_reg)
		    if(is_all_cells_nonoize)
			if(!muon_selection)
			{
			    th1_hist_ptr = ((TH1*)(gDirectory->FindObjectAny( Form("%s_module_energy_first_%i",module_name.Data(), cell_iter+1) )));
			    th1_hist_ptr->Fill(module_energy_summ);
			}

		if(Is_TOF_in_reg)
		    if(!muon_selection)
			if(is_energy_in_range)
//			    if(is_mod5_fired)
				if(is_signal_in_gate)

				{
				    calo_energy_summ[cell_iter] += energy_cell;
				}

		if(is_energy_in_range)
		    if(is_mod5_fired)
		    {
			calo_energy_summ_allpart += energy_cell;
		    }


	    }// cell

	    enery_trace_collected++;




	}// module

	Float_t calo_energy_summ_ = 0.;
	if(Is_TOF_in_reg)
	    for(Int_t icell = 0; icell < 10; icell++)
	    {
		calo_energy_summ_ += calo_energy_summ[icell];
		th1_hist_ptr = ((TH1*)(gDirectory->FindObjectAny(  Form("calo_energy_first_%i",icell+1)  )));
		th1_hist_ptr->Fill(calo_energy_summ_);
	    }

	th2_hist_ptr = ((TH2*)(gDirectory->FindObjectAny(  "part_time_energ"  )));
	th2_hist_ptr->Fill(calo_energy_summ_allpart, PMT_TOF);





    } //entry
    printf("test\n");
    delete[] calo_energy_summ;



    //Fit histogramms ============================================================
    if(1)
    {
	Double_t mean, sigma;
	Float_t *muons_calib_coef_mean = new Float_t[total_channels];
	Float_t *muons_calib_coef_sigma = new Float_t[total_channels];

	th1_hist_ptr = ((TH1*)(gDirectory->FindObjectAny( Form("calo_energy_first_%i",10) )));
	FitHistogrammSmartInMaxPeak(th1_hist_ptr, mean, sigma, 1.);
	th1_hist_ptr->SetTitle( Form("Calo energy [mean %.0f, sigma %.0f, res %.1f%%]; MeV", mean, sigma, (sigma/mean*100.)) );


	for(Int_t module_iter = 0; module_iter < total_modules; module_iter++)
	{
	    Int_t total_cells = readme_reader_ptr->Get_module_info()[module_iter].total_cells;
	    TString module_name = readme_reader_ptr->Get_module_info()[module_iter].comment;


	    th1_hist_ptr = ((TH1*)(gDirectory->FindObjectAny( Form("%s_module_trace",module_name.Data()) )));
	    for(Int_t cell_iter = 0; cell_iter < total_cells; cell_iter++)
	    {
		Float_t bin_content = th1_hist_ptr->GetBinContent(cell_iter+1);
		bin_content = bin_content / (Float_t)enery_trace_collected;
		th1_hist_ptr->SetBinContent(cell_iter+1, bin_content);
	    }

	    for(Int_t cell_iter = 0; cell_iter < total_cells; cell_iter++)
	    {
		th1_hist_ptr = ((TH1*)(gDirectory->FindObjectAny( Form("%s_cell_%i_charge_muons",module_name.Data(), cell_iter) )));
		FitHistogrammSmartInMaxPeak(th1_hist_ptr, mean, sigma, 1.5);
		th1_hist_ptr->SetTitle( Form("Muon mod %i Cell%i [mean %.0f, sigma %.0f]",module_iter+1, cell_iter+1, mean, sigma) );
		printf("muonfit:%s:%i:%.1f:%.1f\n", module_name.Data(), cell_iter, mean, sigma);
		muons_calib_coef_mean[readme_reader_ptr->GetContCh_from_CellN(module_iter, cell_iter)] = mean;
		muons_calib_coef_sigma[readme_reader_ptr->GetContCh_from_CellN(module_iter, cell_iter)] = sigma;

	    }
	}

	printf("coeffs table:\nch   module   cell   mean    sigma   sg^2/mn\n");
	for(Int_t ch = 0; ch < total_channels; ch++)
	{
	    Int_t board, cell;
	    readme_reader_ptr->GetBoardCh_from_ContCh(ch, board, cell);
	    printf("%i %i %i %f %f %f\n",
		  ch, board, cell, muons_calib_coef_mean[ch], muons_calib_coef_sigma[ch], muons_calib_coef_sigma[ch]*muons_calib_coef_sigma[ch] / muons_calib_coef_mean[ch]);
	}

	
	printf("Float_t muons_calib_coef[%i] = {", total_channels);
	for(Int_t ch = 0; ch < total_channels-1; ch++)
	    printf("%f, ", muons_calib_coef_mean[ch]);
	printf("%f};\n",muons_calib_coef_mean[total_channels-1]);
    }




    //============================================================================
    if(0){


	TObjArray *canv_array = new TObjArray;
	canv_array->SetName("result_canvas");
	canv_array->SetOwner();
	result_array->Add(canv_array);


	TCanvas *canvas_calo = new TCanvas(readme_reader_ptr->Get_run_name().Data(), readme_reader_ptr->Get_run_name().Data());
	canvas_calo->DivideSquare(2);
	canv_array->Add(canvas_calo);
	canvas_calo->cd(1);
	th1_hist_ptr = ((TH1*)(gDirectory->FindObjectAny("part_time")));
	th1_hist_ptr->SetLineWidth(3);
	th1_hist_ptr->Draw();
	th1_hist_ptr = ((TH1*)(gDirectory->FindObjectAny("part_time_rej")));
	th1_hist_ptr->SetLineColor(2);
	th1_hist_ptr->Draw("same");
	//    canvas_calo->cd(2);
	//    th1_hist_ptr = ((TH1*)(gDirectory->FindObjectAny("calo_energy")));
	//    th1_hist_ptr->Draw();

	TCanvas *canvas_calo1 = new TCanvas(
		    Form("%s_modenergy", readme_reader_ptr->Get_run_name().Data()),
		    readme_reader_ptr->Get_run_name().Data());
	canvas_calo1->DivideSquare(total_modules);
	canv_array->Add(canvas_calo1);




	for(Int_t module_iter = 0; module_iter < total_modules; module_iter++)
	{
	    Int_t total_cells = readme_reader_ptr->Get_module_info()[module_iter].total_cells;
	    TString module_name = readme_reader_ptr->Get_module_info()[module_iter].comment;

	    TCanvas *canv_module = new TCanvas(Form("%s", module_name.Data()), Form("%s", module_name.Data()));
	    canv_module->DivideSquare(total_cells+2);
	    canv_array->Add(canv_module);

	    canv_module->cd(1);
	    th1_hist_ptr = ((TH1*)(gDirectory->FindObjectAny( Form("%s_module_trace",module_name.Data()) )));
	    th1_hist_ptr->Draw();

	    canvas_calo1->cd(module_iter+1);
	    th1_hist_ptr = ((TH1*)(gDirectory->FindObjectAny( Form("%s_module_energy_first_%i",module_name.Data(), 10) )));
	    th1_hist_ptr->Draw();

	    for(Int_t cell_iter = 0; cell_iter < total_cells; cell_iter++)
	    {
		canv_module->cd(cell_iter +2);
		th1_hist_ptr = ((TH1*)(gDirectory->FindObjectAny( Form("%s_module_energy_first_%i",module_name.Data(), cell_iter+1) )));
		th1_hist_ptr->Draw();
	    }




	    canv_module = new TCanvas(Form("%s_muons", module_name.Data()), Form("%s", module_name.Data()));
	    canv_module->DivideSquare(total_cells);
	    canv_array->Add(canv_module);
	    for(Int_t cell_iter = 0; cell_iter < total_cells; cell_iter++)
	    {
		canv_module->cd(cell_iter +1);
		th1_hist_ptr = ((TH1*)(gDirectory->FindObjectAny( Form("%s_cell_%i_charge_muons",module_name.Data(), cell_iter) )));
		th1_hist_ptr->Draw();
	    }

	    canv_module = new TCanvas(Form("%s_%s", module_name.Data(),part_sel_comment.Data()), Form("%s", module_name.Data()));
	    canv_module->DivideSquare(total_cells);
	    canv_array->Add(canv_module);
	    for(Int_t cell_iter = 0; cell_iter < total_cells; cell_iter++)
	    {
		canv_module->cd(cell_iter +1);
		th1_hist_ptr = ((TH1*)(gDirectory->FindObjectAny( Form("%s_cell_%i_energy",module_name.Data(), cell_iter) )));
		th1_hist_ptr->Draw();
	    }

	}





	for(Int_t i = 0; i < canv_array->GetLast(); i++)
	{
	    ((TCanvas*)canv_array->At(i))->SaveAs(Form("%s.pdf(",   (result_path + result_file_name + "").Data() ));
	}
	((TCanvas*)canv_array->At(canv_array->GetLast()))->SaveAs(Form("%s.pdf)",  (result_path + result_file_name + "").Data() ));
    }

    printf("test2\n");
    // source_file->Close();

    TFile *result_file = new TFile((result_path + result_file_name + ".root").Data(), "RECREATE");
    SaveArrayStructInFile(result_array, result_file);
    printf("file %s was writed\n", (result_path + result_file_name + ".root").Data());
    //result_file->Close();
    gROOT->ProcessLine( Form(".cp calo_test_analisys.c %s",(result_path + result_file_name + "_code.c").Data()) );



    //    result_array->Delete();
    delete result_array;


    printf("test3\n");
    new TBrowser;


}





