#define isLOCAL 1


#include <iostream>
#include <fstream>
//#include <conio.h>
#include <ctime>

#include <TFile.h>
#include <TGraph.h>
#include <TF1.h>
#include <TTree.h>
#include <TObjArray.h>
#include <TString.h>
#include <TDirectory.h>
#include <TCanvas.h>
#include <TH1F.h>
#include <TSystemFile.h>
#include <TSystemDirectory.h>

#include "adc64_text_data.h"


Int_t FilesListFromDir(const TString filespath, TObjArray *const collection, Int_t max_num, TString content);


void convert_macro()
{

#if isLOCAL
    TString log_file_name = "/mnt/hgfs/DETECTORS_DATA/SOURCE/CALO/slowControl.csv";
    //TString source_path = "/mnt/hgfs/DETECTORS_DATA/SOURCE/CALO/";
    TString source_path = "/mnt/hgfs/DETECTORS_DATA/SOURCE/CALO/calib_7/";
    //TString result_path = "/mnt/hgfs/DETECTORS_DATA/RESULTS/CALO/root_source/";
    TString result_path = "/mnt/hgfs/DETECTORS_DATA/RESULTS/CALO/root_source/calib_7_mor2";
#else
    TString log_file_name = "/media/sf_ADC64/test/slowControl.csv";
    TString source_path = "/media/sf_ADC64/test/";
    TString result_path = "/media/sf_ADC64/data_processing/ADC64/PROCESSING/root_converted/";
#endif


    TString result_file_prefix = "";



    Int_t EvToConv = 10000; //0 - all
    Int_t gate_beg = 40;
    Int_t gate_end = 130;


    TObjArray files_names;
    files_names.SetOwner();


    // == Source files management ==
    /**** // Tate files list from directory
    FilesListFromDir(source_path, &files_names, 0, ".txt");

    //remove extension
    for(Int_t f=0; f<=files_names.GetLast();f++)
    {
	TObjString *iFile_name = (TObjString*)(files_names.At(f));
	Int_t p_pos = ((TObjString*)(files_names.At(f)))->GetString().Index(".txt", 4);
	TString new_name = iFile_name->GetString()(0,p_pos);
	iFile_name->SetString( new_name );
    }
    /**** //Print files list for manual array
    for(Int_t f=0; f<=files_names.GetLast();f++)
	printf("files_names.Add(new TObjString(\"%s\"));\n", ((TObjString*)(files_names.At(f)))->GetString().Data());
    /****/ //manual arry
    files_names.Add(new TObjString("adc64_17_03_01_16_20_overnight"));
    /****/
    // =============================



    printf("\n\n\n######################################################\n");
    printf("##################  DATA CONVERTER  ##################\n");
    printf("######################################################\n\n\n");

    printf("--------------------------\n");
    printf("Source path: %s\n", source_path.Data());
    printf("Result path: %s\n", result_path.Data());
    printf("File list to convert:\n");
    for(Int_t f=0; f<=files_names.GetLast();f++)
	printf("%s\n",  ((TObjString*)(files_names.At(f)))->GetString().Data());
    printf("--------------------------\n\n");



    for(Int_t iFile=0; iFile<=files_names.GetLast();iFile++)
    {
	TString iFile_name = ((TObjString*)(files_names.At(iFile)))->GetString();
	printf("\n\n### %i of %i ###\n", iFile+1, files_names.GetLast()+1);
	ConvertFile(source_path+iFile_name+".txt", result_path+iFile_name+result_file_prefix+".root", log_file_name,
		    gate_beg, gate_end, EvToConv);
    }


}

Int_t FilesListFromDir(const TString Directory, TObjArray *const collection, Int_t max_num, TString content)
{
    TString filespath = Directory;

    if(!filespath.EndsWith("/")) filespath += "/";

    if(!collection)
    {
	printf("ERROR: Collection ptr is NULL\n");
	return 0;
    }

    TSystemFile *currFile;
    TString currFileName;
    TSystemDirectory SDirectory(filespath, filespath);
    TIter nextfile(SDirectory.GetListOfFiles());

    Int_t nfiles = 0;
    while ((currFile=(TSystemFile*)nextfile()))
    {
	currFileName = currFile->GetName();
	if (!currFile->IsDirectory() && currFileName.Contains(content))
	{
	    collection->Add( new TObjString( currFileName.Data() ) );
	    nfiles++;
	    if(max_num > 0)
		if(nfiles == max_num) break;
	}
    }

    return nfiles;
}



