#include <iostream>
#include <fstream>
//#include <conio.h>
#include <ctime>

#include <TFile.h>
#include <TGraph.h>
#include <TF1.h>
#include <TTree.h>

#include <TString.h>

#include <TCanvas.h>
#include <TH1F.h>


#include "adc64_text_data.h"




void data_processing()
{
    //input data
    TString source_path = "/mnt/hgfs/DETECTORS_DATA/CALO/";
    TString result_path = "root_res/";


    //Int_t events_to_process = 2190400;
    Int_t events_to_process = 200000;
    //Int_t events_to_process = 20000;
    Int_t sample_total = 200;
    Int_t gate_beg = 40;
    Int_t gate_end = 130;


    /***/
    TString source_file_name = "adc64_17_04_06_19_15_overnight"; //2190402 readed 26284823 lines
    /***/
    TString result_file_name = result_path + source_file_name + ".root";


    printf("\n\n\n######################################################\n");
    printf("#################  SIGNAL VIEWER  ##################\n");
    printf("######################################################\n\n\n");


    adc64_text_data data_class(source_path + source_file_name + ".txt");
    Int_t channel_total = data_class.Get_Channel_total();
    event_data_struct *event_struct = new event_data_struct[channel_total];


    /****/
    // Waveform Inspector ********************************************************
    Int_t Channel_to_show = 0;

    TCanvas *canv = new TCanvas("canv", "canv");
    TGraph *waveform_graph = new TGraph();
    waveform_graph->SetTitle(Form("Channel [%i]", Channel_to_show));

    Int_t start_time_stamp = -1;
    for(Int_t nEvent = 0; nEvent <= events_to_process; nEvent++)
    {
	if((data_class.read_event()) != 0) break;
	data_class.Calculate_waveform(event_struct[Channel_to_show], Channel_to_show, gate_beg, gate_end);

	if(start_time_stamp < 0) start_time_stamp = event_struct[Channel_to_show].time_stamp;
	event_struct[Channel_to_show].time_stamp -= start_time_stamp;


	if(event_struct[Channel_to_show].time_stamp > 5400)
	{
	    for(Int_t s=0;s<sample_total;s++)
		waveform_graph->SetPoint(s, s, data_class.Get_samples_data()[Channel_to_show][s]);

	    printf("\n\n-------------------------\n");
	    event_struct[Channel_to_show].Print(); printf("\n");

	    waveform_graph->Draw("ALP*");
	    canv->Update();
	    canv->Modified();

	    printf("INPUT 'q' TO QUIT -------------------------\n");

	    char key = getchar();
	    if(key == 'q')break;
	}


	Int_t **sample_data = data_class.Get_samples_data();
    }
    return;
    // ***************************************************************************
    /****/

    //create result root file, create tree
    TFile result_file(result_file_name.Data(), "RECREATE");
    result_file.cd();


    TTree *data_tree = new TTree("adc64_data", "adc64_data");
    for(Int_t ch = 0; ch < channel_total; ch++)
    {
	//data_tree->Branch(Form("channel_%i", ch), (event_struct+ch), "time_stamp/I:mean_in_gate/F:mean_out_gate:RMS_in_gate:RMS_out_gate");
	(event_struct+ch)->CreateBranch(data_tree, ch);
    }



    time_t start_time = time(NULL);
    Int_t Error_code = 0;

    for(Int_t nEvent = 0; nEvent <= events_to_process; nEvent++)
    {
	if((nEvent%100) == 0)
	{
	    time_t current_time = time(NULL);
	    Int_t proc_sec = difftime(current_time,start_time);
	    Float_t percents = (float)nEvent/events_to_process;
	    Int_t time_est = (percents == 0)?0:proc_sec/percents*(1. - percents);
	    Float_t proc_rate = (float)proc_sec/nEvent*100000./60.;

	    printf("Processed events: %i (%5.1f%%); [pas %3.0dm %2.0is] [est %3.0dm %2.0is] [%.1f min/100M ev]\r",
		   nEvent, (percents*100.),
		   (proc_sec/60), proc_sec%60,
		   (time_est/60), time_est%60,
		   proc_rate);
	    cout<<flush;
	}


	if((Error_code = data_class.read_event()) != 0) break;

	for(Int_t ch = 0; ch < channel_total; ch++)
	{
	    data_class.Calculate_waveform(event_struct[ch], ch, gate_beg, gate_end);

	    if(start_time_stamp < 0) start_time_stamp = event_struct[ch].time_stamp;
	    event_struct[ch].time_stamp -= start_time_stamp;
	}


	data_tree->Fill();
	//event_struct.Print(); printf("\n");
    }

    printf("\nData processing finished with code %i\n", Error_code);

    printf("\nWriting data in file \"%s\"\n", result_file_name.Data());
    data_tree->Write();
    result_file.Close();

    delete [] event_struct;

}


