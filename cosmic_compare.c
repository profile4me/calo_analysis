

#include <TFile.h>
#include <TGraph.h>
#include <TF1.h>
#include <TTree.h>
#include <TObjArray.h>
#include <TString.h>
#include <TDirectory.h>
#include <TCanvas.h>
#include <TH1F.h>
#include <TSystemFile.h>
#include <TSystemDirectory.h>
#include <TBrowser.h>
#include <TAxis.h>
#include "langaus.hh"


void smartfit( TH1 *histogramm, Double_t &mean,Double_t &sigma, Double_t x_min, Double_t x_max)
{

    TF1 *func = new TF1("fitfunc", langaufun, 0, 10000, 4);
    func->SetParameters(500,3000,500000,100.);
    func->SetParNames("Width","MP","Area","GSigma");

    histogramm->Fit(func, "Q","",x_min,x_max);

    Double_t FWHM;
    langaupro(func->GetParameters(), mean, FWHM);
    sigma = 0.;

    printf("%s: widtg: %f; MP: %f; Area: %f; GSigma %f; %d\n",
           histogramm->GetName(),
           func->GetParameter(0),
           func->GetParameter(1),
           func->GetParameter(2),
           func->GetParameter(3),
           histogramm->GetEntries());

}

void cosmic_compare()
{


    TString source_path = "/mnt/disk_data/cosmic_data_2018_quater1/";
    //TString cut_eq = "(channel_0.time_half > 52)&&(channel_0.time_half < 56)";
    TString cut_eq = "(1)";

    const int num_files = 9;
    TString file_names[num_files] = {
        "adc64_18_02_11_15_00_CBM_6modules_TH_200_ZS100_TC_3_2M",
        "adc64_18_02_12_22_30_CBM_module1Only_TH_200_ZS100_MAF_2_BLC_50_TC_100k",
        "adc64_18_02_13_00_40_CBM_module1Only_TH_200_ZS100_MAF_4_BLC_50_TC_100k",
        "adc64_18_02_13_02_00_CBM_module1Only_TH_200_ZS100_MAF_8_BLC_50_TC_100k",
        "adc64_18_02_13_10_00_CBM_module1Only_TH_200_ZS100_MAF_16_BLC_50_TC_100k",
        "adc64_18_02_13_11_00_CBM_module1Only_TH_200_ZS100_MAF_16_BLC_50_TC_100k_RND_1",
        "adc64_18_02_13_13_30_CBM_module1Only_TH_200_ZS100_MAF_2_BLC_100_TC_100k",
        "adc64_18_02_13_13_30_CBM_module1Only_TH_200_ZS100_MAF_4_BLC_100_TC_100k",
        "adc64_18_02_13_15_30_CBM_module1Only_TH_200_ZS100_MAF_8_BLC_100_TC_100k"
    };

    double_t mean[num_files], sigma[num_files];
    double_t mean_ts[num_files], sigma_ts[num_files];

    TH1F *hist_arr[num_files];
    TH1F *hist_timesel_arr[num_files];
    TH1F *hist_time_arr[num_files];


    for(int hist_iter = 0; hist_iter < num_files; hist_iter++)
    {
	TString ifilename = source_path+file_names[hist_iter]+"/"+ file_names[hist_iter] +".root";
	TString ihistname = file_names[hist_iter](21, file_names[hist_iter].Length());

	TFile *file = new TFile(ifilename.Data(), "READONLY");
	TTree *data_tree = dynamic_cast<TTree*>(file->FindObjectAny("adc64_data"));


	hist_arr[hist_iter] = new TH1F(ihistname.Data(), ihistname.Data(), 100,1,12001);
	hist_arr[hist_iter]->SetLineColor(hist_iter+1);
	data_tree->Draw(Form("channel_0.integral_in_gate>>%s", ihistname.Data()), "(1)");
	//data_tree->Draw(Form("(channel_0.MAX_in_gate-channel_0.zero_level)>>%s", ihistname.Data()), "(1)");
	smartfit(hist_arr[hist_iter], mean[hist_iter], sigma[hist_iter], 100., 8500.);


	hist_timesel_arr[hist_iter] = new TH1F((ihistname+"_tmsel").Data(), ihistname.Data(), 100,1,12001);
	hist_timesel_arr[hist_iter]->SetLineColor(hist_iter+1);
	data_tree->Draw(Form("channel_0.integral_in_gate>>%s_tmsel", ihistname.Data()), "(channel_0.time_cross > 48)&&(channel_0.time_cross < 56)");
	smartfit(hist_timesel_arr[hist_iter], mean_ts[hist_iter], sigma_ts[hist_iter], 1000., 10000.);


	hist_time_arr[hist_iter]= new TH1F((ihistname+"_time").Data(), ihistname.Data(), 400,45,60);
	hist_time_arr[hist_iter]->SetLineColor(hist_iter+1);
	data_tree->Draw(Form("channel_0.time_cross>>%s", (ihistname+"_time").Data()));
    }

    TCanvas *canv = new TCanvas("canv", "canv");
    canv->Divide(2,2);



    TH1F* reshist1 = new TH1F("hists_means1", "Histogramm langau fit max", num_files,0,num_files);
    TH1F* reshist2 = new TH1F("hists_means_tsel", "Histogramm langau fit max [time selection]", num_files,0,num_files);
    for(int hist_iter = 0; hist_iter <num_files; hist_iter++)
    {

	reshist1->SetBinContent(hist_iter+1, mean[hist_iter]);
	reshist1->SetBinError(hist_iter+1, sigma[hist_iter]);

	reshist2->SetBinContent(hist_iter+1, mean_ts[hist_iter]);
	reshist2->SetBinError(hist_iter+1, sigma_ts[hist_iter]);


	//reshist->GetXaxis()->SetBinLabel(hist_iter+1, "TString file_names[hist_iter]%s");
    }
    canv->cd(4);
    reshist1->SetLineColor(1);
    reshist2->SetLineColor(2);
    reshist1->Draw();
    reshist2->Draw("same");




    for(int hist_iter = 0; hist_iter < num_files; hist_iter++)
    {
	canv->cd(1); 	hist_arr[hist_iter]->Draw( (hist_iter == 0)? "":"same"   );
//	canv->cd(2); 	hist_timesel_arr[hist_iter]->Draw( (hist_iter == 0)? "":"same"   );
//	canv->cd(1); 	hist_arr[hist_iter]->DrawNormalized( (hist_iter == 0)? "":"same"   );
	canv->cd(2); 	hist_timesel_arr[hist_iter]->DrawNormalized( (hist_iter == 0)? "":"same"   );
	canv->cd(3);    hist_time_arr[hist_iter]->DrawNormalized((hist_iter == 0)? "":"same");
    }
    canv->cd(1)->BuildLegend();




}

