#/usr/bin/bash

echo -e "\n\n=================================="
echo    "======== RUN INFO CREATOR ========"
echo -e "=================================="

if [ $# -eq 0 ]
then

echo -e "\nrun macro: ./makeRunInfo2.sh runfolder -p"
echo "-p  print result run_info.txt"
echo "-e  edit result run_info.txt"
echo

exit 1

fi



# select run info
run_name=`echo $1 | cut -c18-`

echo -e "\nWorking on RUN:"
echo "$1 ($run_name)" 

cd $1

#romoving empty data files
#echo -e "\nRemoving empty data data files:"
#find  . -type f -empty -name '*.data'
#find  . -type f -empty -name '*.data' -exec rm -Rf {} \;

#file1=`ls 0611cef9*.data`
#file2=`ls 07a8de9a*.data`

echo -e "\nData files list:"
ls -all -h 0*.data

file1=`du -aS 0611cef9*.data | sort -h | tail -1 | cut -f2`
file2=`du -aS 07a8de9a*.data | sort -h | tail -1 | cut -f2`

echo -e "\nInserting largest data files names:"
echo "File1: \"$file1\""
echo "File2: \"$file2\""

#writing run info
cat ../run_info.txt.in2 | sed -e "s/FILE1/$file1/g" | sed -e "s/FILE2/$file2/g" | sed -e "s/TEXTINFO1/$1/g" | sed -e "s/TEXTINFO2/$run_name/g"  > run_info.txt

echo
echo -e "=================================="
echo -e "=================================="


case "$2" in
-p) cat 'run_info.txt' ;;
-e) vi 'run_info.txt' ;;
*);;
esac

#if [ $2 -eq 1 ]
#then 
#cat 'run_info.txt'
#fi

cd '..'

