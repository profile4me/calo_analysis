#include <TObjArray.h>
#include <TFile.h>
#include <TChain.h>
#include <TObjString.h>
#include<TH2F.h>
#include<TF1.h>

#include<TROOT.h>
#include<TSystem.h>
#include<TSystemDirectory.h>
#include<TSystemFile.h>
#include<TList.h>
#include<TCollection.h>
#include<TMath.h>
#include<TSpectrum.h>
#include<TDirectory.h>
#include<TStopwatch.h>
#include<TGraph.h>
#include<TBrowser.h>
#include<TCanvas.h>
#include<TPad.h>
#include<TVirtualPad.h>
#include<TProfile.h>
#include<TStyle.h>
#include "TMarker.h"
#include "TLegend.h"
#include <TLatex.h>

#include "Readme_reader.h"
#include "event_data_struct.h"
#include "bindataformat.h"
#include "../libraries/utilites/utilites.h"

void
calo_test_analisys_T9_results_finalfit ()
{

  TCanvas *canvas = NULL;
  TH1 *th1_hist_ptr = NULL;
  TH2 *th2_hist_ptr = NULL;
  TF1 *tfunc_ptr = NULL;
  Double_t mean, sigma, mean_err, sigma_err;

  gStyle->SetOptFit (1111);

  /////////////////////////////////////////////////
  Readme_reader *readme_reader_ptr = new
  Readme_reader ("");
  //readme_reader_ptr->SetInfoFile("/mnt/hgfs/DETECTORS_DATA/RESULTS/CALO/root_source/T9_test_nov_17/Calibration.txt");
  //readme_reader_ptr->SetInfoFile (      "/home/runna61/adc64_data_2018_quater1/beam_root/Calibration.txt");
  //readme_reader_ptr->SetInfoFile (      "/mnt/disk_na61pc008/runna61/adc64_data_2018_quater1/beam_root/Calibration.txt");
  readme_reader_ptr->SetInfoFile (      "/mnt/c/Users/dmitr/cernbox/RESULTS/CALO/root_results/beam_results_2018_apr/Calibration.txt");

  //readme_reader_ptr->Print();

  const Int_t beam_to_module_N = 2;
  Int_t beam_to_module_tcells =
      readme_reader_ptr->Get_module_info ()[beam_to_module_N - 1].total_cells;
  TString beam_to_module_name =
      readme_reader_ptr->Get_module_info ()[beam_to_module_N - 1].comment;
  Int_t total_modules = readme_reader_ptr->Get_module_num ();
  /////////////////////////////////////////////////

  /////////////////////////////////////////////////
  //TString source_path = "/mnt/hgfs/DETECTORS_DATA/RESULTS/CALO/root_results/t9_testbeam_nov2017/";
  TString source_path = "/home/runna61/adc64_data_2018_quater1/beam_results/";
  source_path =      "/mnt/disk_na61pc008/runna61/adc64_data_2018_quater1/beam_results/";
  source_path =      "/mnt/c/Users/dmitr/cernbox/RESULTS/CALO/root_results/beam_results_2018_apr/";

  Float_t hist_1_lo = 1., hist_1_hi = 4.5;
  Float_t hist_2_lo = 4.6, hist_2_hi = 8.5;
  Float_t hist_3_lo = 8.6, hist_3_hi = 150.;

//    Float_t hist_1_lo = 1., hist_1_hi = 3.5;
//    Float_t hist_2_lo = 3.5, hist_2_hi = 9.5;

  /****/
   TString source_hist_name = Form("Calo_resolution_f%i",10);
   TString hist_comment = "Full calo";
   /****
  TString source_hist_name = Form ("Calo_mod_resolution_f%i", 10);
  TString hist_comment = "one module";
  /****/

  // erorrs 5%
  /****
   TString file_1 = source_path + "21_Ampl_pedsel/Calo_results_T10_protons_fit2_.root";
   TString file_2 = source_path + "21_Ampl_pedsel/Calo_results_protons_fit2_.root";
   hist_comment += ", Amplitude";
   /****
   TString file_1 = source_path + "16_pu_mod5/Calo_results_T10_protons_fit2_.root";
   TString file_2 = source_path + "16_pu_mod5/Calo_results_protons_fit2_.root";
   hist_comment += ", Charge";
   /****
   TString file_1 = source_path + "22_Charge_pedsel/Calo_results_T10_protons_fit2_.root";
   TString file_2 = source_path + "22_Charge_pedsel/Calo_results_protons_fit2_.root";
   hist_comment += ", Charge pedcut";
   /****
   TString file_1 = source_path + "16_scint_calib20/Calo_results_T10_18_protons_fit2_1.root";
   TString file_2 = source_path + "09_T9_no_mod3/Calo_results_protons_fit2_1.root";
   hist_comment += ", no mod 3";
   /****
   TString file_1 = source_path + "22_newpu_nmod3/Calo_results_T10_18_protons_fit3_15.root";
   TString file_2 = source_path + "20_T9_nmod3_newpu/_Calo_results_protons_fit3_15.root";
   hist_comment += ", no mod 3, new pule-up";
   /****/
  TString file_1 = source_path
      + "22_newpu_nmod3/Calo_results_T10_18_protons_fit3_15.root";
  TString file_2 = source_path
      + "20_T9_nmod3_newpu/_Calo_results_protons_fit3_15.root";
  TString file_3 = source_path
      + "30_NA61_nmod3_tomod5/Calo_results_NA61_fit3_15_.root";
  hist_comment += " 5, no mod 3";
  /****/

  TH1F *result_hist = new
  TH1F (Form ("Resolution"),
	Form ("Resolution [%s]; E, GeV", hist_comment.Data ()), 1500, 0,
	150);

  TFile *source_file1 = new
  TFile (file_1.Data (), "READONLY");
  TH1F *hist_1 = ((TH1F*)(gDirectory->FindObjectAny( source_hist_name.Data() )));
  if (hist_1 == NULL) return;

  for (Int_t ibin = 1; ibin <= hist_1->GetXaxis ()->GetLast (); ibin++)
    {
      Float_t bin_center = hist_1->GetBinCenter (ibin);
      if (bin_center < hist_1_lo) continue;
      if (bin_center > hist_1_hi) continue;

      Float_t bin_value = hist_1->GetBinContent (ibin);
      Float_t bin_error = hist_1->GetBinError (ibin);
      if (bin_value == 0.0) continue;
      //printf("bin %i: %f\n", ibin, bin_value);

      Int_t result_bin = result_hist->FindBin (bin_center);
      result_hist->SetBinContent (result_bin, bin_value);
      result_hist->SetBinError (result_bin, bin_error);
    }

  TFile *source_file2 = new
  TFile (file_2.Data (), "READONLY");
  TH1F *hist_2 = ((TH1F*)(gDirectory->FindObjectAny( Form("Calo_resolution_f%i",10) )));
  if (hist_2 == NULL) return;

  for (Int_t ibin = 1; ibin <= hist_2->GetXaxis ()->GetLast (); ibin++)
    {
      Float_t bin_center = hist_2->GetBinCenter (ibin);
      if (bin_center < hist_2_lo) continue;
      if (bin_center > hist_2_hi) continue;

      Float_t bin_value = hist_2->GetBinContent (ibin);
      Float_t bin_error = hist_2->GetBinError (ibin);
      if (bin_value == 0.0) continue;
      printf ("%i: %f : %f\n", ibin, bin_value, bin_error);

      Int_t result_bin = result_hist->FindBin (bin_center);
      result_hist->SetBinContent (result_bin, bin_value);
      result_hist->SetBinError (result_bin, bin_error);
      result_hist->SetBinError (result_bin, bin_value * 0.05);

    }

  TFile *source_file3 = new
  TFile (file_3.Data (), "READONLY");
  TH1F *hist_3 = ((TH1F*)(gDirectory->FindObjectAny( Form("Calo_resolution_f%i",10) )));
  if (hist_3 == NULL) return;

  for (Int_t ibin = 1; ibin <= hist_3->GetXaxis ()->GetLast (); ibin++)
    {
      Float_t bin_center = hist_3->GetBinCenter (ibin);
      if (bin_center < hist_3_lo) continue;
      if (bin_center > hist_3_hi) continue;

      Float_t bin_value = hist_3->GetBinContent (ibin);
      Float_t bin_error = hist_3->GetBinError (ibin);
      if (bin_value == 0.0) continue;
      printf ("%i: %f : %f\n", ibin, bin_value, bin_error);

      Int_t result_bin = result_hist->FindBin (bin_center);
      result_hist->SetBinContent (result_bin, bin_value);
      result_hist->SetBinError (result_bin, bin_error);
      result_hist->SetBinError (result_bin, bin_value * 0.05);

    }

  for (Int_t point = 1; point <= result_hist->GetNbinsX (); point++)
    {
      Float_t bin_value = result_hist->GetBinContent (point);
      Float_t bin_center = result_hist->GetBinCenter (point);
      if (bin_value == 0.) continue;

      printf ("%i : %f : %f\n", point, bin_center, bin_value);
    }

  ////////////////////////////////////////////////////////////////////////////////////////
  TCanvas *canv = new
  TCanvas ("canv_res", "canv_res");
  //canv->cd(1)->SetLogx(1);
  TLatex label;

  TF1 *calo_res_funk = new TF1("calo_res", "TMath::Sqrt( ([0]/TMath::Sqrt(x))*([0]/TMath::Sqrt(x)) + [1]*[1] + ([2]/x)*([2]/x) )", hist_1_lo, hist_3_hi);
  //TF1 *calo_res_funk = new  TF1 ("calo_res",       "TMath::Sqrt( ([0]/TMath::Sqrt(x))*([0]/TMath::Sqrt(x)) + [1]*[1])",       hist_1_lo, hist_2_hi);
  //TF1 *calo_res_funk = new TF1("calo_res", "TMath::Sqrt(  ([0]/TMath::Sqrt(x))*([0]/TMath::Sqrt(x))  )", hist_1_lo, hist_3_hi);
  result_hist->Fit (calo_res_funk, "R", "");

  result_hist->SetLineWidth (2);
  result_hist->SetMarkerStyle (22);

  result_hist->Draw ();

  //0.55 0.03 0.02
/***/
  label.DrawLatex(.35, .16, "#frac{#sigmaE}{E} = #sqrt{(#frac{a}{#sqrt{E}})^2 + (b)^2 + (#frac{c}{E})^2}");
    label.DrawLatex(.35, .08, Form("a = %.2f; b = %.2f; c = %.2f",
                                 calo_res_funk->GetParameter(0),
                                 calo_res_funk->GetParameter(1),
                                 calo_res_funk->GetParameter(2)) );

  /***
  label.DrawLatex (.35, .16,
		   "#frac{#sigmaE}{E} = #sqrt{(#frac{a}{#sqrt{E}})^2 + (b)^2}");
  label.DrawLatex (
      .35,
      .08,
      Form ("a = %.2f; b = %.2f", calo_res_funk->GetParameter (0),
	    calo_res_funk->GetParameter (1)));

/***/
  //	label.DrawLatex(.55, .4, "#frac{#sigma^{}_{E}}{E} = #frac{a}{#sqrt{E}}");
  //	label.DrawLatex(.1, .6, Form("a = %.3f", calo_res_funk->GetParameter(0)));
  ////////////////////////////////////////////////////////////////////////////////////////

  source_file1->Close ();
  source_file2->Close ();

}
