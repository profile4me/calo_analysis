

#include <fstream>
#include <iostream>
#include <TNtuple.h>
#include <TFile.h>
#include <TChain.h>
#include <Rtypes.h>
#include <vector>
#include <TBrowser.h>
#include <TObject.h>
#include <TLatex.h>
#include<TStyle.h>


#include "Readme_reader.h"
#include "event_data_struct.h"
#include "bindataformat.h"
#include "../libraries/utilites/utilites.h"


void  calo_test_analisys_T9_results()
{
    gStyle->SetErrorX(0);
    gStyle->SetOptFit(1111);

    ////////////////////////////////////////////////////////////////////////////////////////
    TString calc_type_comment = "";
    //TString sub_path = "01_test/"; calc_type_comment = "T10_18";
    //TString sub_path = "02_firstresults/"; calc_type_comment = "T10_18";
    //TString sub_path = "03_Scalib/"; calc_type_comment = "T10_18_Scalib";
    //TString sub_path = "04_multihit/"; calc_type_comment = "T10_18_multhit";
    //TString sub_path = "05_narrow/"; calc_type_comment = "T10_18_cllmtr";
//    TString sub_path = "06_T9_multhit/"; calc_type_comment = "T9_multihit";
//    TString sub_path = "07_scint2/"; calc_type_comment = "T10_18";
//    TString sub_path = "08_scont_no_mod3/"; calc_type_comment = "T10_18 no mod3";
//    TString sub_path = "09_T9_no_mod3/"; calc_type_comment = "T9 no mod 3";
//        TString sub_path = "10_scint_calib/"; calc_type_comment = "T10_18";
//        TString sub_path = "11_nom3_multht/"; calc_type_comment = "T10_18";
//        TString sub_path = "12_nom3/"; calc_type_comment = "T10_18";
 //       TString sub_path = "13_narrow_nom3/"; calc_type_comment = "T10_18";
        //    TString sub_path = "14_fbeam_nom3/"; calc_type_comment = "T10_18";
        //    TString sub_path = "15_fbeam_nom3_multihit/"; calc_type_comment = "T10_18";
           // TString sub_path = "16_scint_calib20/"; calc_type_comment = "T10_18";
            //TString sub_path = "17_new_pu_scint_calib20/"; calc_type_comment = "T10_18";
            //    TString sub_path = "18_new_pu_scint_calib20_el/"; calc_type_comment = "T10_18";
            //    TString sub_path = "19_T9_nom3_newpu_esel/"; calc_type_comment = "T9";
             //   TString sub_path = "20_T9_nmod3_newpu/"; calc_type_comment = "T9";
//                TString sub_path = "22_newpu_nmod3/"; calc_type_comment = "T10_18";
//                TString sub_path = "23_NA61_first/"; calc_type_comment = "NA61";
//        TString sub_path = "24_NA61_calib/"; calc_type_comment = "NA61";
        //TString sub_path = "27_NA61_electrons/"; calc_type_comment = "NA61_electrons";
     //   TString sub_path = "28_NA61_mod3/"; calc_type_comment = "NA61_mod3_fast_fee";
    //    TString sub_path = "29_NA61_nmod3/"; calc_type_comment = "29_NA61_nmod3_recalc";
    //    TString sub_path = "30_NA61_nmod3_tomod5/"; calc_type_comment = "30_NA61_nmod3_tomod5";


    //TString sub_path = "31_NA61_mod3_fastfeefit/"; calc_type_comment = "chargefit";
//    TString sub_path = "33_NA61_mod3_fastfeefit_chnofit/"; calc_type_comment = "chargenofit";



//    TString sub_path = "01_firstrun/"; calc_type_comment = "adc_v3";
//    TString sub_path = "02_nofit/"; calc_type_comment = "adc_v3_nofit";
    TString sub_path = "03_longcables/"; calc_type_comment = "adc_v3_longcable_fit";
//    TString sub_path = "32_NA61_mod3_fastfeefit_ampl/"; calc_type_comment = "amplfit";
//    TString sub_path = "32_NA61_mod3_fastfeefit_ampl/"; calc_type_comment = "amplfit";
//    TString sub_path = "32_NA61_mod3_fastfeefit_ampl/"; calc_type_comment = "amplfit";
//    TString sub_path = "32_NA61_mod3_fastfeefit_ampl/"; calc_type_comment = "amplfit";
    //TString sub_path = "20_Amplitude/"; calc_type_comment = "charge";

    //TString source_path = "/mnt/hgfs/DETECTORS_DATA/RESULTS/CALO/root_results/t9_testbeam_nov2017/";
    TString source_path = "/home/runna61/adc64_data_2018_quater1/beam_results/";
    source_path = "/mnt/disk_na61pc008/runna61/adc64_data_2018_quater1/beam_results/";

    source_path = "/home/runna61/adc64_data_2018_quater4/root_results/";


    source_path+=sub_path;
    
    TString result_file_name;
    TString result_file_postfix = "_fit3_15_";
    TString scan_name;
    
    TObjArray files_names;
    Float_t *scan_value = new Float_t[100]; Int_t i = 0;
    TString scan_comment;
    
    Int_t beam_to_module_N = 3;
    Float_t part_mass;
    //    if(part_name == "pions") part_mass = .135;
    //    else if(part_name == "protons") part_mass = .938;
    
    /********
    result_file_name += "Calo_results_negpions_minHV08";
    scan_comment = "negpions_minHV08_%.1f_GeV_c";
    scan_name = "neg Pions T9 [HV -0.8]";
    part_mass = .135;
    
    files_names.Add(new TObjString("26.11.2017_17_00_mod5_9GeV_neg_pions_HV_minus0.8V_analysis_1.root"));
    scan_value[i++] = 9.;

    files_names.Add(new TObjString("26.11.2017_17_21_mod5_10GeV_neg_pions_HV_minus0.8V_analysis_1.root"));
    scan_value[i++] = 10.;

    files_names.Add(new TObjString("26.11.2017_17_45_mod5_8GeV_neg_pions_HV_minus0.8V_analysis_1.root"));
    scan_value[i++] = 8.;

    files_names.Add(new TObjString("26.11.2017_18_15_mod5_7GeV_neg_pions_HV_minus0.8V_analysis_1.root"));
    scan_value[i++] = 7.;

    files_names.Add(new TObjString("26.11.2017_18_40_mod5_6GeV_neg_pions_HV_minus0.8V_analysis_1.root"));
    scan_value[i++] = 6.;

    files_names.Add(new TObjString("26.11.2017_19_00_mod5_5GeV_neg_pions_HV_minus0.8V_analysis_1.root"));
    scan_value[i++] = 5.;

    files_names.Add(new TObjString("26.11.2017_19_30_mod5_4GeV_neg_pions_HV_minus0.8V_analysis_1.root"));
    scan_value[i++] = 4.;

    files_names.Add(new TObjString("26.11.2017_19_50_mod5_3GeV_neg_pions_HV_minus0.8V_analysis_1.root"));
    scan_value[i++] = 3.;

    files_names.Add(new TObjString("26.11.2017_20_10_mod5_2GeV_neg_pions_HV_minus0.8V_analysis_1.root"));
    scan_value[i++] = 2.;

    files_names.Add(new TObjString("26.11.2017_20_36_mod5_1.5GeV_neg_pions_HV_minus0.8V_analysis_1.root"));
    scan_value[i++] = 1.5;

    files_names.Add(new TObjString("26.11.2017_21_05_mod5_1GeV_neg_pions_HV_minus0.8V_analysis_1.root"));
    scan_value[i++] = 1.;


    
    /********
    result_file_name += "Calo_results_protons_minHV08";
    scan_comment = "protons_minHV08_%.1f_GeV_c";
    scan_name = "Protons T9 [HV -0.8]";
    part_mass = .938;
    
    files_names.Add(new TObjString("26.11.2017_12_23_mod5_6GeV_protons_HV_minus0.8V_analysis_1.root"));
    scan_value[i++] = 6.;
    
    files_names.Add(new TObjString("26.11.2017_12_45_mod5_10GeV_protons_HV_minus0.8V_analysis_1.root"));
    scan_value[i++] = 10.;
    
    files_names.Add(new TObjString("26.11.2017_13_35_mod5_9GeV_protons_HV_minus0.8V_analysis_1.root"));
    scan_value[i++] = 9.;
    
    files_names.Add(new TObjString("26.11.2017_14_00_mod5_8GeV_protons_HV_minus0.8V_analysis_1.root"));
    scan_value[i++] = 8.;
    
    files_names.Add(new TObjString("26.11.2017_14_30_mod5_7GeV_protons_HV_minus0.8V_analysis_1.root"));
    scan_value[i++] = 7.;
    
    files_names.Add(new TObjString("26.11.2017_15_00_mod5_5GeV_protons_HV_minus0.8V_analysis_1.root"));
    scan_value[i++] = 5.;
    
    files_names.Add(new TObjString("26.11.2017_15_20_mod5_4GeV_protons_HV_minus0.8V_analysis_1.root"));
    scan_value[i++] = 4.;
    
    files_names.Add(new TObjString("26.11.2017_15_35_mod5_3GeV_protons_HV_minus0.8V_analysis_1.root"));
    scan_value[i++] = 3.;
    
    files_names.Add(new TObjString("26.11.2017_15_54_mod5_2GeV_protons_HV_minus0.8V_analysis_1.root"));
    scan_value[i++] = 2.;
    


    
    /********
    result_file_name += "Calo_results_electrons";
    scan_comment = "electrons_%.1f_GeV_c";
    scan_name = "Electrons T9";
    part_mass = .0005;
    
    files_names.Add(new TObjString("23.11.2017_19_21_mod5_5GeV_focus_7m_elsel_analysis_1.root"));
    scan_value[i++] = 5.;
    
    files_names.Add(new TObjString("23.11.2017_15_55_mod5_6GeV_focus_7m_elsel_analysis_1.root"));
    scan_value[i++] = 6.;
    
    files_names.Add(new TObjString("23.11.2017_16_52_mod5_7GeV_focus_7m_elsel_analysis_1.root"));
    scan_value[i++] = 7.;
    
    files_names.Add(new TObjString("23.11.2017_17_30_mod5_8GeV_focus_7m_elsel_analysis_1.root"));
    scan_value[i++] = 8.;
    
    files_names.Add(new TObjString("23.11.2017_18_02_mod5_9GeV_focus_7m_elsel_analysis_1.root"));
    scan_value[i++] = 9.;
    
    files_names.Add(new TObjString("23.11.2017_18_39_mod5_10GeV_focus_7m_elsel_analysis_1.root"));
    scan_value[i++] = 10.;
    
    files_names.Add(new TObjString("23.11.2017_20_00_mod5_4.5GeV_focus_7m_elsel_analysis_1.root"));
    scan_value[i++] = 4.5;
    
    files_names.Add(new TObjString("23.11.2017_20_33_mod5_4Gev_focus_7m_elsel_analysis_1.root"));
    scan_value[i++] = 4.;
    
    files_names.Add(new TObjString("23.11.2017_21_13_mod5_3.5GeV_focus_7m_elsel_analysis_1.root"));
    scan_value[i++] = 3.5;
    
    files_names.Add(new TObjString("23.11.2017_21_42_mod5_3GeV_focus_7m_elsel_analysis_1.root"));
    scan_value[i++] = 3.;
    
    files_names.Add(new TObjString("23.11.2017_22_13_mod5_2.5GeV_focus_7m_elsel_analysis_1.root"));
    scan_value[i++] = 2.5;
    
    
    /********
    result_file_name += "Calo_results_all";
    scan_comment = "prot_pions_%.1f_GeV_c";
    part_mass = .938;
    scan_name = "Protons and pions T9";
    
    
    files_names.Add(new TObjString("23.11.2017_19_21_mod5_5GeV_focus_7m_analysis_1.root"));
    scan_value[i++] = 5.;
    
    files_names.Add(new TObjString("23.11.2017_15_55_mod5_6GeV_focus_7m_analysis_1.root"));
    scan_value[i++] = 6.;
    
    files_names.Add(new TObjString("23.11.2017_16_52_mod5_7GeV_focus_7m_analysis_1.root"));
    scan_value[i++] = 7.;
    
    files_names.Add(new TObjString("23.11.2017_17_30_mod5_8GeV_focus_7m_analysis_1.root"));
    scan_value[i++] = 8.;
    
    files_names.Add(new TObjString("23.11.2017_18_02_mod5_9GeV_focus_7m_analysis_1.root"));
    scan_value[i++] = 9.;
    
    files_names.Add(new TObjString("23.11.2017_18_39_mod5_10GeV_focus_7m_analysis_1.root"));
    scan_value[i++] = 10.;
    
    files_names.Add(new TObjString("23.11.2017_20_00_mod5_4.5GeV_focus_7m_analysis_1.root"));
    scan_value[i++] = 4.5;
    
    files_names.Add(new TObjString("23.11.2017_20_33_mod5_4Gev_focus_7m_analysis_1.root"));
    scan_value[i++] = 4.;
    
    files_names.Add(new TObjString("23.11.2017_21_13_mod5_3.5GeV_focus_7m_analysis_1.root"));
    scan_value[i++] = 3.5;
    
    files_names.Add(new TObjString("23.11.2017_21_42_mod5_3GeV_focus_7m_analysis_1.root"));
    scan_value[i++] = 3.;
    
    files_names.Add(new TObjString("23.11.2017_22_13_mod5_2.5GeV_focus_7m_analysis_1.root"));
    scan_value[i++] = 2.5;
    
    
    /********
    result_file_name += "Calo_results_protons";
    scan_comment = "prot_%.1f_GeV_c";
    part_mass = .938;
    scan_name = "Protons T9 nomod3 newpu";
    
    files_names.Add(new TObjString("24.11.2017_15_28_mod5_10GeV_f7m_cherenkov_Veto_analysis_2.root"));
    scan_value[i++] = 10.;
    
    files_names.Add(new TObjString("24.11.2017_15_51_mod5_9GeV_f7m_cherenkov_Veto_analysis_2.root"));
    scan_value[i++] = 9.;
    
    files_names.Add(new TObjString("24.11.2017_16_11_mod5_8GeV_f7m_cherenkov_Veto_analysis_2.root"));
    scan_value[i++] = 8.;
    
    files_names.Add(new TObjString("24.11.2017_16_30_mod5_7GeV_f7m_cherenkov_Veto_analysis_2.root"));
    scan_value[i++] = 7.;
    
    files_names.Add(new TObjString("24.11.2017_16_51_mod5_6GeV_f7m_cherenkov_Veto_analysis_2.root"));
    scan_value[i++] = 6.;
    
    files_names.Add(new TObjString("24.11.2017_17_15_mod5_5GeV_f7m_cherenkov_Veto_analysis_2.root"));
    scan_value[i++] = 5.;
    
    files_names.Add(new TObjString("24.11.2017_17_37_mod5_4.5GeV_f7m_cherenkov_Veto_analysis_2.root"));
    scan_value[i++] = 4.5;
    
    files_names.Add(new TObjString("24.11.2017_18_01_mod5_4GeV_f7m_cherenkov_Veto_analysis_2.root"));
    scan_value[i++] = 4.;
    
    files_names.Add(new TObjString("24.11.2017_18_25_mod5_3.5GeV_f7m_cherenkov_Veto_analysis_2.root"));
    scan_value[i++] = 3.5;
    
    files_names.Add(new TObjString("24.11.2017_18_50_mod5_3GeV_f7m_cherenkov_Veto_analysis_2.root"));
    scan_value[i++] = 3.;
    
//    files_names.Add(new TObjString("24.11.2017_19_13_mod5_2.5GeV_f7m_cherenkov_Veto_analysis_2.root"));
//    scan_value[i++] = 2.5;

//    files_names.Add(new TObjString("24.11.2017_19_39_mod5_2GeV_f7m_cherenkov_Veto_analysis_2.root"));
//    scan_value[i++] = 2.;


    /********
    result_file_name += "Calo_results_pions";
    scan_comment = "pions_%.1f_GeV_c";
    part_mass = .135;
    scan_name = "Pions T9";
    
    
//        files_names.Add(new TObjString("24.11.2017_20_29_mod5_pions_2GeV_cherenkov_coincidence_analysis_1.root"));
//        scan_value[i++] = 2.;

    files_names.Add(new TObjString("24.11.2017_20_52_mod5_pions_10GeV_cherenkov_coincidence_analysis_1.root"));
    scan_value[i++] = 10.;
    
    files_names.Add(new TObjString("24.11.2017_21_11_mod5_pions_9GeV_cherenkov_coincidence_analysis_1.root"));
    scan_value[i++] = 9.;
    
    files_names.Add(new TObjString("24.11.2017_21_35_mod5_pions_8GeV_cherenkov_coincidence_analysis_1.root"));
    scan_value[i++] = 8.;
    
    files_names.Add(new TObjString("24.11.2017_22_15_mod5_pions_7GeV_cherenkov_coincidence_analysis_1.root"));
    scan_value[i++] = 7.;
    
    files_names.Add(new TObjString("24.11.2017_22_35_mod5_pions_6GeV_cherenkov_coincidence_analysis_1.root"));
    scan_value[i++] = 6.;
    
    files_names.Add(new TObjString("24.11.2017_22_51_mod5_pions_2GeV_cherenkov_coincedence_analysis_1.root"));
    scan_value[i++] = 2.;
    
    files_names.Add(new TObjString("25.11.2017_08_41_mod5_pions_5GeV_cherenkov_coincidence_analysis_1.root"));
    scan_value[i++] = 5.;
    
    files_names.Add(new TObjString("25.11.2017_08_59_mod5_pions_4.5GeV_cherenkov_coincidence_analysis_1.root"));
    scan_value[i++] = 4.5;
    
    files_names.Add(new TObjString("25.11.2017_09_16_mod5_pions_4GeV_cherenkov_coincidence_analysis_1.root"));
    scan_value[i++] = 4.;
    
    files_names.Add(new TObjString("25.11.2017_09_33_mod5_pions_3.5GeV_cherenkov_coincidence_analysis_1.root"));
    scan_value[i++] = 3.5;
    
    files_names.Add(new TObjString("25.11.2017_09_52_mod5_pions_3GeV_cherenkov_coincidence_analysis_1.root"));
    scan_value[i++] = 3.;
    
    files_names.Add(new TObjString("25.11.2017_10_41_mod5_pions_2.5GeV_cherenkov_coincedence_analysis_1.root"));
    scan_value[i++] = 2.5;
    
    
    /********
    result_file_name += "Calo_results_neg_pions";
    scan_comment = "neg_pions_%.1f_GeV_c";
    part_mass = .135;
    scan_name = "neg pions T9";
    
    
    files_names.Add(new TObjString("25.11.2017_12_16_mod5_neg_pions_10GeV_analysis_1.root"));
    scan_value[i++] = 10.;
    
    files_names.Add(new TObjString("25.11.2017_12_54_mod5_neg_pions_9GeV_analysis_1.root"));
    scan_value[i++] = 9.;
    
    files_names.Add(new TObjString("25.11.2017_13_30_mod5_neg_pions_8GeV_analysis_1.root"));
    scan_value[i++] = 8.;
    
    files_names.Add(new TObjString("25.11.2017_14_15_mod5_neg_pions_7GeV_analysis_1.root"));
    scan_value[i++] = 7.;
    
    files_names.Add(new TObjString("25.11.2017_14_40_mod5_neg_pions_6GeV_analysis_1.root"));
    scan_value[i++] = 6.;
    
    files_names.Add(new TObjString("25.11.2017_15_28_mod5_neg_pions_5GeV_analysis_1.root"));
    scan_value[i++] = 5.;
    
    files_names.Add(new TObjString("25.11.2017_15_49_mod5_neg_pions_4GeV_analysis_1.root"));
    scan_value[i++] = 4.;
    
    files_names.Add(new TObjString("25.11.2017_16_20_mod5_neg_pions_3.5GeV_analysis_1.root"));
    scan_value[i++] = 3.5;
    
    files_names.Add(new TObjString("25.11.2017_16_40_mod5_neg_pions_3GeV_analysis_1.root"));
    scan_value[i++] = 3.;
    
    files_names.Add(new TObjString("25.11.2017_17_05_mod5_neg_pions_2.5GeV_analysis_1.root"));
    scan_value[i++] = 2.5;
    
    files_names.Add(new TObjString("25.11.2017_17_28_mod5_neg_pions_2GeV_analysis_1.root"));
    scan_value[i++] = 2.;
    
    files_names.Add(new TObjString("25.11.2017_17_55_mod5_neg_pions_1.5GeV_analysis_1.root"));
    scan_value[i++] = 1.5;
    
    files_names.Add(new TObjString("25.11.2017_18_25_mod5_neg_pions_1GeV_analysis_1.root"));
    scan_value[i++] = 1.;

    
    /********
    result_file_name += "Calo_results_T10_pions";
    scan_comment = "T10_pions_%.1f_GeV_c";
    part_mass = .135;
    scan_name = "Pions T10";
    
    files_names.Add(new TObjString("adc64_17_09_08_13_45_protonScan_mod5_6GeVpions_analysis_1.root"));
    scan_value[i++] = 6.;
    
    files_names.Add(new TObjString("adc64_17_09_08_16_22_protonScan_mod5_5.5GeVpions_analysis_1.root"));
    scan_value[i++] = 5.5;
    
    files_names.Add(new TObjString("adc64_17_09_08_18_11_protonScan_mod5_5GeVpions_analysis_1.root"));
    scan_value[i++] = 5.;
    
    files_names.Add(new TObjString("adc64_17_09_08_19_30_protonScan_mod5_4.5GeVpions_analysis_1.root"));
    scan_value[i++] = 4.5;
    
    files_names.Add(new TObjString("adc64_17_09_08_20_50_protonScan_mod5_4GeVpions_analysis_1.root"));
    scan_value[i++] = 4.;
    
    files_names.Add(new TObjString("adc64_17_09_08_22_55_protonScan_mod5_3.5GeVpions_analysis_1.root"));
    scan_value[i++] = 3.5;
    
    files_names.Add(new TObjString("adc64_17_09_09_02_20_protonScan_mod5_3.0GeVpions_analysis_1.root"));
    scan_value[i++] = 3.;
    
    files_names.Add(new TObjString("adc64_17_09_09_17_00_protonScan_mod5_2.5GeVpions_analysis_1.root"));
    scan_value[i++] = 2.5;
    
    files_names.Add(new TObjString("adc64_17_09_09_11_15_protonScan_mod5_2.0GeVpions_analysis_1.root"));
    scan_value[i++] = 2.;
    
    files_names.Add(new TObjString("adc64_17_09_12_03_54_protonScan_mod5_1.5GeVpions_analysis_1.root"));
    scan_value[i++] = 1.5;
    /********/
    
    
    /********
    result_file_name += "Calo_results_T10_protons";
    scan_comment = "T10_Protons_%.1f_GeV_c";
    part_mass = .938;
    scan_name = "Protons T10 mltht";

    files_names.Add(new TObjString("adc64_17_09_08_13_45_protonScan_mod5_6GeVprotons_analysis_2.root"));
    scan_value[i++] = 6.;

    files_names.Add(new TObjString("adc64_17_09_08_16_22_protonScan_mod5_5.5GeVprotons_analysis_2.root"));
    scan_value[i++] = 5.5;

    files_names.Add(new TObjString("adc64_17_09_08_18_11_protonScan_mod5_5GeVprotons_analysis_2.root"));
    scan_value[i++] = 5.;

    files_names.Add(new TObjString("adc64_17_09_08_19_30_protonScan_mod5_4.5GeVprotons_analysis_2.root"));
    scan_value[i++] = 4.5;

    files_names.Add(new TObjString("adc64_17_09_08_20_50_protonScan_mod5_4GeVprotons_analysis_2.root"));
    scan_value[i++] = 4.;

    files_names.Add(new TObjString("adc64_17_09_08_22_55_protonScan_mod5_3.5GeVprotons_analysis_2.root"));
    scan_value[i++] = 3.5;

    files_names.Add(new TObjString("adc64_17_09_09_02_20_protonScan_mod5_3.0GeVprotons_analysis_2.root"));
    scan_value[i++] = 3.;

    files_names.Add(new TObjString("adc64_17_09_09_17_00_protonScan_mod5_2.5GeVprotons_analysis_2.root"));
    scan_value[i++] = 2.5;

    files_names.Add(new TObjString("adc64_17_09_09_11_15_protonScan_mod5_2.0GeVprotons_analysis_2.root"));
    scan_value[i++] = 2.;

    //files_names.Add(new TObjString("adc64_17_09_12_03_54_protonScan_mod5_1.5GeVprotons_analysis_1.root"));
    //scan_value[i++] = 1.5;
    /********

    result_file_name += "Calo_results_T10_18_protons";
    scan_comment = "T10_18_Protons_%.1f_GeV_c";
    part_mass = .938;
    scan_name = "Protons T10 2018 fbeam nmod3";

//    files_names.Add(new TObjString("beam_18_04_12_19_20_5GeV_mod5protons_analysis_2.root"));
//    scan_value[i++] = 5.;

//    files_names.Add(new TObjString("beam_18_04_12_20_20_4.5GeV_mod5protons_analysis_2.root"));
//    scan_value[i++] = 4.5;

//    files_names.Add(new TObjString("beam_18_04_12_19_47_4GeV_mod5protons_analysis_2.root"));
//    scan_value[i++] = 4.;

//    files_names.Add(new TObjString("beam_18_04_12_20_56_3.5GeV_mod5protons_analysis_2.root"));
//    scan_value[i++] = 3.5;

//    files_names.Add(new TObjString("beam_18_04_12_21_30_3GeV_mod5protons_analysis_2.root"));
//    scan_value[i++] = 3.;

//    files_names.Add(new TObjString("beam_18_04_12_22_10_2.5GeV_mod5protons_analysis_2.root"));
//    scan_value[i++] = 2.5;

//    files_names.Add(new TObjString("beam_18_04_12_23_06_2GeV_mod5protons_analysis_2.root"));
//    scan_value[i++] = 2.;



//    scan_name = "Protons T10 2018 narrow beam nom3";
//    files_names.Add(new TObjString("beam_18_04_13_18_00_5GeV_mod5_triggerTOF_100mV_narrowBeam2protons_analysis_2.root"));
//    scan_value[i++] = 5.;

//    files_names.Add(new TObjString("beam_18_04_14_04_46_4.5GeV_mod5_triggerTOF_100mV_narrowBeam2protons_analysis_2.root"));
//    scan_value[i++] = 4.5;

//    files_names.Add(new TObjString("beam_18_04_14_03_24_4GeV_mod5_triggerTOF_100mV_narrowBeam2protons_analysis_2.root"));
//    scan_value[i++] = 4.;

//    files_names.Add(new TObjString("beam_18_04_14_02_05_3.5GeV_mod5_triggerTOF_100mV_narrowBeam2protons_analysis_2.root"));
//    scan_value[i++] = 3.5;

//    files_names.Add(new TObjString("beam_18_04_14_00_35_3GeV_mod5_triggerTOF_100mV_narrowBeam2protons_analysis_2.root"));
//    scan_value[i++] = 3.;

//    files_names.Add(new TObjString("beam_18_04_13_22_30_2.5GeV_mod5_triggerTOF_100mV_narrowBeam2protons_analysis_2.root"));
//    scan_value[i++] = 2.5;

//    files_names.Add(new TObjString("beam_18_04_13_19_05_2GeV_mod5_triggerTOF_100mV_narrowBeam2protons_analysis_2.root"));
//    scan_value[i++] = 2.;



        scan_name = "Protons T10 2018 scn2_nomod3_calib20_newpu";
	files_names.Add(new TObjString("beam_18_04_14_19_50_5GeV_mod5_TOF1_TOF2_scint2_narrowBeam2protons_analysis_2.root"));
	scan_value[i++] = 5.;

	files_names.Add(new TObjString("beam_18_04_14_18_30_4GeV_mod5_TOF1_TOF2_scint2_narrowBeam2protons_analysis_2.root"));
	scan_value[i++] = 4.;

	files_names.Add(new TObjString("beam_18_04_14_17_00_3GeV_mod5_TOF1_TOF2_scint2_narrowBeam2protons_analysis_2.root"));
	scan_value[i++] = 3.;

	files_names.Add(new TObjString("beam_18_04_14_14_49_2GeV_mod5_TOF1_TOF2_scint2_narrowBeam2protons_analysis_2.root"));
	scan_value[i++] = 2.;
    /********/


/***
    result_file_name += "Calo_results_NA61";
    scan_comment = "NA61_%i_GeV_c";
    part_mass = .938;
    scan_name = "Protons NA61";
	beam_to_module_N = 5;


    files_names.Add(new TObjString("beam_NA61_18_04_29_10_14_10GeV_mod5_noZS_analysis_2.root"));
    scan_value[i++] = 10.;

    files_names.Add(new TObjString("beam_NA61_18_04_29_09_25_13GeV_mod5_noZS_desync_analysis_2.root"));
    scan_value[i++] = 13.;

    files_names.Add(new TObjString("beam_NA61_18_04_29_08_58_19GeV_mod5_noZS_desync_analysis_2.root"));
    scan_value[i++] = 19.;

    files_names.Add(new TObjString("beam_NA61_18_04_29_08_31_30GeV_mod5_noZS_analysis_2.root"));
    scan_value[i++] = 30.;

//    files_names.Add(new TObjString("beam_NA61_18_04_28_23_20_40GeV_mod5_noZS_analysis_2.root"));
//    scan_value[i++] = 40.;

    files_names.Add(new TObjString("beam_NA61_18_04_30_20_10_40GeV_mod5_noZS_analysis_2.root"));
    scan_value[i++] = 40.;

    files_names.Add(new TObjString("beam_NA61_18_04_28_14_50_80GeV_mod5_noZS_analysis_2.root"));
    scan_value[i++] = 80.;

    files_names.Add(new TObjString("beam_NA61_18_04_28_21_50_120GeV_mod5_noZS_desync_analysis_2.root"));
    scan_value[i++] = 120.;

    files_names.Add(new TObjString("beam_NA61_18_04_28_20_20_150GeV_mod5_noZS_desync_analysis_2.root"));
    scan_value[i++] = 150.;
    /********/


/***
    result_file_name += "Calo_results_NA61_el";
    scan_comment = "NA61_el_%i_GeV_c";
    part_mass = .0005;
    scan_name = "Protons NA61 electrons";



    files_names.Add(new TObjString("beam_NA61_18_04_29_10_14_10GeV_mod5_noZS_analysis_2.root"));
    scan_value[i++] = 10.;

    files_names.Add(new TObjString("beam_NA61_18_04_29_09_25_13GeV_mod5_noZS_desync_analysis_2.root"));
    scan_value[i++] = 13.;

    files_names.Add(new TObjString("beam_NA61_18_04_29_08_58_19GeV_mod5_noZS_desync_analysis_2.root"));
    scan_value[i++] = 19.;

    files_names.Add(new TObjString("beam_NA61_18_04_29_08_31_30GeV_mod5_noZS_analysis_2.root"));
    scan_value[i++] = 30.;

//    files_names.Add(new TObjString("beam_NA61_18_04_28_23_20_40GeV_mod5_noZS_analysis_2.root"));
//    scan_value[i++] = 40.;

    files_names.Add(new TObjString("beam_NA61_18_04_30_20_10_40GeV_mod5_noZS_analysis_2.root"));
    scan_value[i++] = 40.;

    files_names.Add(new TObjString("beam_NA61_18_04_28_14_50_80GeV_mod5_noZS_analysis_2.root"));
    scan_value[i++] = 80.;

/***/




/***
    files_names.Add(new TObjString("beam_NA61_18_04_29_09_52_10GeV_mod5_noZS_HV_minus_800mV_analysis_2.root"));
    scan_value[i++] = 10.;

        files_names.Add(new TObjString("beam_NA61_18_04_29_09_38_13GeV_mod5_noZS_HV_minus_800mV_analysis_2.root"));
	scan_value[i++] = 13.;

	files_names.Add(new TObjString("beam_NA61_18_04_29_10_30_19GeV_mod5_noZS_HV_minus_800mV_analysis_2.root"));
	scan_value[i++] = 19.;

	files_names.Add(new TObjString("beam_NA61_18_04_29_11_01_30GeV_mod5_noZS_HV_minus_800mV_analysis_2.root"));
	scan_value[i++] = 30.;

	files_names.Add(new TObjString("beam_NA61_18_04_30_20_15_40GeV_mod5_noZS_HV_minus_800mV_analysis_2.root"));
	scan_value[i++] = 40.;

	files_names.Add(new TObjString("beam_NA61_18_04_28_15_50_80GeV_mod5_noZS_HV_minus_800mV_analysis_2.root"));
	scan_value[i++] = 80.;

	files_names.Add(new TObjString("beam_NA61_18_04_28_21_30_120Gev_noZS_HV_minus_800mV_analysis_2.root"));
	scan_value[i++] = 120.;

	files_names.Add(new TObjString("beam_NA61_18_04_28_21_10_150GeV_noZS_HV_minus_800mV_analysis_2.root"));
	scan_value[i++] = 150.;

    //    files_names.Add(new TObjString("beam_18_04_12_23_06_2GeV_mod5protons_analysis_2.root"));
    //    scan_value[i++] = 2.;


/***

	result_file_name += "Calo_results_NA61_nmod3_rc";
	scan_comment = "NA61_%i_GeV_c";
	part_mass = .938;
	scan_name = "Protons NA61 nmod3";
	beam_to_module_N = 2;


    files_names.Add(new TObjString("beam_NA61_18_04_30_08_40_10GeV_mod2_noZS_analysis_2.root"));
    scan_value[i++] = 10.;
    files_names.Add(new TObjString("beam_NA61_18_04_30_16_10_13GeV_mod2_noZS_analysis_2.root"));
    scan_value[i++] = 13.;
    files_names.Add(new TObjString("beam_NA61_18_04_30_16_15_19GeV_mod2_noZS_analysis_2.root"));
    scan_value[i++] = 19.;
    files_names.Add(new TObjString("beam_NA61_18_04_30_16_35_30GeV_mod2_noZS_analysis_2.root"));
    scan_value[i++] = 30.;
    files_names.Add(new TObjString("beam_NA61_18_04_30_16_37_40GeV_mod2_noZS_analysis_2.root"));
    scan_value[i++] = 40.;
    files_names.Add(new TObjString("beam_NA61_18_04_30_16_48_80GeV_mod2_noZS_analysis_2.root"));
    scan_value[i++] = 80.;
    //    files_names.Add(new TObjString("beam_NA61_18_04_29_14_30_80GeV_mod2_noZS_analysis_2.root"));
    //    scan_value[i++] = 80.;
    files_names.Add(new TObjString("beam_NA61_18_04_30_16_59_120GeV_mod2_noZS_analysis_2.root"));
    scan_value[i++] = 120.;
    files_names.Add(new TObjString("beam_NA61_18_04_29_23_05_150GeV_mod2_noZS_analysis_2.root"));
    scan_value[i++] = 150.;

/*********/


    /***
        result_file_name += "Calo_results_NA61_mod3";
	scan_comment = "NA61_%i_GeV_c";
	part_mass = .938;
	scan_name = "Protons NA61 mod3";



	files_names.Add(new TObjString("beam_NA61_18_04_30_19_45_10GeV_mod3_noZS_analysis_2.root"));
	scan_value[i++] = 10.;

	files_names.Add(new TObjString("beam_NA61_18_04_30_19_00_13GeV_mod3_noZS_analysis_2.root"));
	scan_value[i++] = 13.;

	files_names.Add(new TObjString("beam_NA61_18_04_30_18_55_19GeV_mod3_noZS_analysis_2.root"));
	scan_value[i++] = 19.;

	files_names.Add(new TObjString("beam_NA61_18_04_30_18_37_30GeV_mod3_noZS_analysis_2.root"));
	scan_value[i++] = 30.;

	files_names.Add(new TObjString("beam_NA61_18_04_30_18_27_40GeV_mod3_noZS_analysis_2.root"));
	scan_value[i++] = 40.;

	files_names.Add(new TObjString("beam_NA61_18_04_29_15_50_80GeV_mod3_noZS_analysis_2.root"));
	scan_value[i++] = 80.;

    /***/

	    /***
     beam_to_module_N = 1;

	        result_file_name += "Calo_results_T10_fastfee_adcv3";
		scan_comment = "T10_%i_GeV_c";
		part_mass = .938;
		scan_name = "Protons T10 adc_v3";


		files_names.Add(new TObjString("beam_T10_18_10_24_22_35_beam_5GeV_noZSprotons_analysis_2.root"));
		scan_value[i++] = 5.;

		files_names.Add(new TObjString("beam_T10_18_10_24_23_38_beam_4GeV_noZSprotons_analysis_2.root"));
		scan_value[i++] = 4.;

		files_names.Add(new TObjString("beam_T10_18_10_25_00_42_beam_3GeV_noZSprotons_analysis_2.root"));
		scan_value[i++] = 3.;

		files_names.Add(new TObjString("beam_T10_18_10_25_10_55_beam_2GeV_noZSprotons_analysis_2.root"));
		scan_value[i++] = 2.;

 	    /***/

    /***/
beam_to_module_N = 1;

        result_file_name += "Calo_results_T10_fastfee_adcv3";
	scan_comment = "T10_%i_GeV_c";
	part_mass = .938;
	scan_name = "Protons T10 adc_v3 long cables";


	files_names.Add(new TObjString("beam_T10_18_10_25_15_09_beam_5GeV_noZS_longCableprotons_analysis_2.root"));
	scan_value[i++] = 5.;

	files_names.Add(new TObjString("beam_T10_18_10_25_16_40_beam_4GeV_noZS_longCableprotons_analysis_2.root"));
	scan_value[i++] = 4.;

	files_names.Add(new TObjString("beam_T10_18_10_25_17_51_beam_3GeV_noZS_longCableprotons_analysis_2.root"));
	scan_value[i++] = 3.;

	files_names.Add(new TObjString("beam_T10_18_10_25_19_27_beam_2GeV_noZS_longCableprotons_analysis_2.root"));
	scan_value[i++] = 2.;

	    /***/


    result_file_name += result_file_postfix;
    
    ////////////////////////////////////////////////////////////////////////////////////////
    
    
    
    
    ////////////////////////////////////////////////////////////////////////////////////////
    Readme_reader *readme_reader_ptr = new Readme_reader("");
    //readme_reader_ptr->SetInfoFile("/mnt/hgfs/DETECTORS_DATA/RESULTS/CALO/root_source/T9_test_nov_17/Calibration.txt");
    //readme_reader_ptr->SetInfoFile("/home/runna61/adc64_data_2018_quater1/beam_root/Calibration.txt");
    //readme_reader_ptr->SetInfoFile("/mnt/disk_na61pc008/runna61/adc64_data_2018_quater1/beam_root/Calibration.txt");
    //readme_reader_ptr->SetInfoFile("/mnt/disk_na61pc008/runna61/adc64_data_2018_quater1/beam_root_fit/Calibration.txt");
    readme_reader_ptr->SetInfoFile("/home/runna61/adc64_data_2018_quater4/root_files/Calibration.txt");
    //readme_reader_ptr->Print();
    
    Int_t beam_to_module_tcells = readme_reader_ptr->Get_module_info()[beam_to_module_N-1].total_cells;
    TString beam_to_module_name = readme_reader_ptr->Get_module_info()[beam_to_module_N-1].comment;
    Int_t total_modules = readme_reader_ptr->Get_module_num();
    Int_t total_files = files_names.GetLast()+1;
    
    
    TH1 *th1_hist_ptr = NULL;
    TH2 *th2_hist_ptr = NULL;
    TF1 *tfunc_ptr = NULL;
    Int_t bin;
    Double_t mean, sigma, mean_err, sigma_err;
    
    TObjArray *result_array = new TObjArray;
    result_array->SetName("calo_results");
    result_array->SetOwner();
    
    Float_t **val_mean_arr = new Float_t*[total_files];
    Float_t **valerr_mean_arr = new Float_t*[total_files];
    Float_t **val_res_arr = new Float_t*[total_files];
    Float_t **valerr_res_arr = new Float_t*[total_files];
    
    Float_t **val_mean_mod_arr = new Float_t*[total_files];
    Float_t **valerr_mean_mod_arr = new Float_t*[total_files];
    Float_t **val_res_mod_arr = new Float_t*[total_files];
    Float_t **valerr_res_mod_arr = new Float_t*[total_files];
    
    for(int i=0; i<total_files; i++)
    {
	val_mean_arr[i] = new Float_t[beam_to_module_tcells];
	valerr_mean_arr[i] = new Float_t[beam_to_module_tcells];
	val_res_arr[i] = new Float_t[beam_to_module_tcells];
	valerr_res_arr[i] = new Float_t[beam_to_module_tcells];
	
	val_mean_mod_arr[i] = new Float_t[beam_to_module_tcells];
	valerr_mean_mod_arr[i] = new Float_t[beam_to_module_tcells];
	val_res_mod_arr[i] = new Float_t[beam_to_module_tcells];
	valerr_res_mod_arr[i] = new Float_t[beam_to_module_tcells];
    }


    Float_t part_enrg_max = TMath::Sqrt(scan_value[total_files-1]*scan_value[total_files-1] + part_mass*part_mass) - part_mass;
    Float_t max_scale = part_enrg_max+10.;
    ////////////////////////////////////////////////////////////////////////////////////////
    
    
    
    
    ////////////////////////////////////////////////////////////////////////////////////////
    TObjArray file_comment;
    for(Int_t iFile=0; iFile<total_files;iFile++)
    {
	file_comment.Add(new TObjString(  Form(scan_comment.Data(), Int_t(10.*scan_value[iFile]))  ));
    }
    ////////////////////////////////////////////////////////////////////////////////////////
    
    
    
    ////////////////////////////////////////////////////////////////////////////////////////
    //TFile *result_file = new TFile((source_path + result_file_name + ".root").Data(), "RECREATE");
    ////////////////////////////////////////////////////////////////////////////////////////
    
    
    
    ////////////////////////////////////////////////////////////////////////////////////////
    for(Int_t icell = 0; icell < beam_to_module_tcells; icell++)
    {

	th1_hist_ptr = new TH1F(Form("Calo_energy_f%i", icell+1), Form("Calo energy first %i cells; E, GeV", icell+1), 600, 0, max_scale);
	if(icell == beam_to_module_tcells-1) th1_hist_ptr->SetTitle(Form("Full Calo Evisible {%s}; E,GeV;E, MeV",scan_name.Data()));
	th1_hist_ptr->SetMarkerStyle(22);
	th1_hist_ptr->SetLineWidth(2);
	result_array->Add(th1_hist_ptr);
	
	th1_hist_ptr = new TH1F(Form("Calo_resolution_f%i", icell+1), Form("Calo energy resolution first %i cells; E, GeV", icell+1), 600, 0, max_scale);
	if(icell == beam_to_module_tcells-1) th1_hist_ptr->SetTitle(Form("Full Calo resoluton {%s}; E,GeV;#sigmaE/E",scan_name.Data()));
	th1_hist_ptr->SetMarkerStyle(22);
	th1_hist_ptr->SetLineWidth(2);
	result_array->Add(th1_hist_ptr);
	
	th1_hist_ptr = new TH1F(Form("Calo_mod_energy_f%i", icell+1), Form("Calo mod%i energy first %i cells; E, GeV", beam_to_module_N, icell+1), 600, 0, max_scale);
	if(icell == beam_to_module_tcells-1) th1_hist_ptr->SetTitle(Form("Central module E {%s}; E,GeV;E, MeV",scan_name.Data()));
	th1_hist_ptr->SetMarkerStyle(22);
	th1_hist_ptr->SetLineWidth(2);
	result_array->Add(th1_hist_ptr);
	th1_hist_ptr = new TH1F(Form("Calo_mod_resolution_f%i", icell+1), Form("Calo mod%i energy resolution first %i cells; E, GeV", beam_to_module_N, icell+1), 600, 0, max_scale);
	if(icell == beam_to_module_tcells-1) th1_hist_ptr->SetTitle(Form("Central module resoluton {%s}; E,GeV;#sigmaE/E",scan_name.Data()));
	th1_hist_ptr->SetMarkerStyle(22);
	th1_hist_ptr->SetLineWidth(2);
	result_array->Add(th1_hist_ptr);
    }
    ////////////////////////////////////////////////////////////////////////////////////////
    
    
    
    ////////////////////////////////////////////////////////////////////////////////////////
    for(Int_t iFile=0; iFile<total_files;iFile++)
    {
	// variables ------------------------------------------
	Float_t part_enrg = TMath::Sqrt(scan_value[iFile]*scan_value[iFile] + part_mass*part_mass) - part_mass;
	Float_t calo_E_est = scan_value[iFile]*27. + 5.;
	calo_E_est = scan_value[iFile]*24. + 35.;
	Float_t bmod_E_est = scan_value[iFile]*20. + 5.;

	TString iFile_name = ((TObjString*)(files_names.At(iFile)))->GetString();
	iFile_name = source_path + iFile_name;
	TString iFile_comment = ((TObjString*)(file_comment.At(iFile)))->GetString();
	
	TObjArray *file_hist_arr = new TObjArray;
	file_hist_arr->SetName(Form("%s_hist", iFile_comment.Data()));
	file_hist_arr->SetOwner();
	result_array->Add(file_hist_arr);
	// ----------------------------------------------------
	
	
	
	// Open file ------------------------------------------
	TFile *source_file = new TFile(iFile_name.Data(), "READONLY");
	if(!source_file->IsOpen())
	{
	    printf("File \"%s\" not found\n", iFile_name.Data());
	    delete source_file;
	    continue;
	}
	else
	{
	    printf("File \"%s\" was opened\n", iFile_name.Data());
	}
	// ----------------------------------------------------
	
	// ----------------------------------------------------
	printf("\n***********\nScan %i; value %.1f; calo E %.1f; mod E %.1f\n", iFile, scan_value[iFile], calo_E_est, bmod_E_est);
	// ----------------------------------------------------


	
	// Energy & Resolution --------------------------------
	gDirectory->cd("Rint:/");
	TH1 *Eres_ncells_full = new TH1F(Form("%s_Eres_ncell_full", iFile_comment.Data()),
	                                 Form("#sigmaE/E dependance using i first calo cells {%s}; n cels; #sigmaE/E", iFile_comment.Data()), beam_to_module_tcells+1, .5, beam_to_module_tcells+1.5);
	file_hist_arr->Add(Eres_ncells_full);
	
	for(Int_t cell_iter = 0; cell_iter < beam_to_module_tcells; cell_iter++)
	{
	    TString hist_name = Form("calo_energy_first_%i",cell_iter+1);
	    
	    source_file->cd();
	    th1_hist_ptr = ((TH1*)(gDirectory->FindObjectAny( hist_name.Data() )));
	    
	    if(th1_hist_ptr)
	    {
		gDirectory->cd("Rint:/");
		th1_hist_ptr = (TH1*)(th1_hist_ptr->Clone( Form("%s_%s", iFile_comment.Data(), th1_hist_ptr->GetName()) ));
		th1_hist_ptr->Rebin(5);
		//FitHistogrammSmartInMaxPeak(th1_hist_ptr, mean, sigma, 3.0, 0., 400.);
		FitHistogrammSmartInMaxPeak(th1_hist_ptr, mean, sigma, 3.0, calo_E_est-1000., calo_E_est+1500.);
		Double_t new_X_min = mean-1.5*sigma;
		Double_t new_X_max = mean+1.5*sigma;
		FitHistogrammInRMSRange(th1_hist_ptr, mean, sigma, 1.5, new_X_min, new_X_max);
		//FitHistogrammInRMSRange(th1_hist_ptr, mean, sigma, 1.0, 0., 400.);

		th1_hist_ptr->SetTitle( Form("Calo energy [first %i cells] {%s} [%s] [%.0f +- %.0f, res %.1f%%]; MeV",cell_iter+1, iFile_comment.Data(), calc_type_comment.Data(), mean, sigma, (sigma/mean*100.)) );


		file_hist_arr->Add(th1_hist_ptr);
		
		tfunc_ptr = th1_hist_ptr->GetFunction(fit_funk_name);
		if(tfunc_ptr)
		{
		    mean = tfunc_ptr->GetParameter(1);
		    sigma = tfunc_ptr->GetParameter(2);
		    mean_err = tfunc_ptr->GetParError(1);
		    sigma_err = tfunc_ptr->GetParError(2);
		    
		    
		    val_mean_arr[iFile][cell_iter] = mean;
		    valerr_mean_arr[iFile][cell_iter] = mean_err;
		    val_res_arr[iFile][cell_iter] = sigma/mean;
		    valerr_res_arr[iFile][cell_iter] = TMath::Sqrt( (sigma_err/mean)*(sigma_err/mean) + (mean_err*sigma/mean/mean)*(mean_err*sigma/mean/mean) );
		    
		    th1_hist_ptr = ((TH1*)(gDirectory->FindObjectAny(  Form("Calo_energy_f%i", cell_iter+1) )));
		    //bin = th1_hist_ptr->FindBin(scan_value[iFile]);
		    bin = th1_hist_ptr->FindBin(part_enrg);
		    th1_hist_ptr->SetBinContent(bin, val_mean_arr[iFile][cell_iter]);
		    th1_hist_ptr->SetBinError(bin, valerr_mean_arr[iFile][cell_iter]);
		    
		    th1_hist_ptr = ((TH1*)(gDirectory->FindObjectAny(  Form("Calo_resolution_f%i", cell_iter+1) )));
		    //bin = th1_hist_ptr->FindBin(scan_value[iFile]);
		    bin = th1_hist_ptr->FindBin(part_enrg);
		    th1_hist_ptr->SetBinContent(bin, val_res_arr[iFile][cell_iter]);
		    th1_hist_ptr->SetBinError(bin, valerr_res_arr[iFile][cell_iter]);
		    
		    Eres_ncells_full->SetBinContent(cell_iter+1, val_res_arr[iFile][cell_iter]);
		    Eres_ncells_full->SetBinError(cell_iter+1, valerr_res_arr[iFile][cell_iter]);
		}
	    } else { printf("Hist %s not found\n", hist_name.Data()); }
	}
	// ----------------------------------------------------
	
	
	
	// Energy & Resolution for beam module ----------------
	gDirectory->cd("Rint:/");
	TH1 *Eres_ncells_mod = new TH1F(Form("%s_Eres_ncell_mod", iFile_comment.Data()),
	                                 Form("#sigmaE/E dependance using i first mod5 cells {%s}; n cells; #sigmaE/E", iFile_comment.Data()), beam_to_module_tcells+1, .5, beam_to_module_tcells+1.5);
	file_hist_arr->Add(Eres_ncells_mod);


	for(Int_t cell_iter = 0; cell_iter < beam_to_module_tcells; cell_iter++)
	{
	    TString hist_name = Form("%s_module_energy_first_%i",beam_to_module_name.Data(), cell_iter+1);
	    
	    source_file->cd();
	    th1_hist_ptr = ((TH1*)(gDirectory->FindObjectAny( hist_name.Data() )));
	    
	    if(th1_hist_ptr)
	    {
		gDirectory->cd("Rint:/");
		th1_hist_ptr = (TH1*)(th1_hist_ptr->Clone( Form("%s_%s", iFile_comment.Data(), th1_hist_ptr->GetName()) ));
		th1_hist_ptr->Rebin(5);

		//FitHistogrammSmartInMaxPeak(th1_hist_ptr, mean, sigma, 1.5);
		FitHistogrammSmartInMaxPeak(th1_hist_ptr, mean, sigma, 1.5, bmod_E_est-1000., bmod_E_est+1000.);
		Double_t new_X_min = mean-1.5*sigma;
		Double_t new_X_max = mean+1.5*sigma;
		FitHistogrammInRMSRange(th1_hist_ptr, mean, sigma, 1.5, new_X_min, new_X_max);

		//FitHistogrammSmartInMaxPeak(th1_hist_ptr, mean, sigma, 1.5);
		th1_hist_ptr->SetTitle( Form("Module %i energy [first %i cells] {%s} [%s]  [%.0f +- %.0f, res %.1f%%]; MeV",  beam_to_module_N,cell_iter+1, iFile_comment.Data(), calc_type_comment.Data(), mean, sigma, (sigma/mean*100.)) );

		file_hist_arr->Add(th1_hist_ptr);
		
		tfunc_ptr = th1_hist_ptr->GetFunction(fit_funk_name);
		if(tfunc_ptr)
		{
		    mean = tfunc_ptr->GetParameter(1);
		    sigma = tfunc_ptr->GetParameter(2);
		    mean_err = tfunc_ptr->GetParError(1);
		    sigma_err = tfunc_ptr->GetParError(2);
		    
		    
		    val_mean_mod_arr[iFile][cell_iter] = mean;
		    valerr_mean_mod_arr[iFile][cell_iter] = mean_err;
		    val_res_mod_arr[iFile][cell_iter] = sigma/mean;
		    valerr_res_mod_arr[iFile][cell_iter] = TMath::Sqrt( (sigma_err/mean)*(sigma_err/mean) + (mean_err*sigma/mean/mean)*(mean_err*sigma/mean/mean) );
		    
		    th1_hist_ptr = ((TH1*)(gDirectory->FindObjectAny(  Form("Calo_mod_energy_f%i", cell_iter+1) )));
		    //bin = th1_hist_ptr->FindBin(scan_value[iFile]);
		    bin = th1_hist_ptr->FindBin(part_enrg);
		    th1_hist_ptr->SetBinContent(bin, val_mean_mod_arr[iFile][cell_iter]);
		    th1_hist_ptr->SetBinError(bin, valerr_mean_mod_arr[iFile][cell_iter]);
		    
		    th1_hist_ptr = ((TH1*)(gDirectory->FindObjectAny(  Form("Calo_mod_resolution_f%i", cell_iter+1) )));
		    //bin = th1_hist_ptr->FindBin(scan_value[iFile]);
		    bin = th1_hist_ptr->FindBin(part_enrg);
		    th1_hist_ptr->SetBinContent(bin, val_res_mod_arr[iFile][cell_iter]);
		    th1_hist_ptr->SetBinError(bin, valerr_res_mod_arr[iFile][cell_iter]);

		    Eres_ncells_mod->SetBinContent(cell_iter+1, val_res_mod_arr[iFile][cell_iter]);
		    Eres_ncells_mod->SetBinError(cell_iter+1, valerr_res_mod_arr[iFile][cell_iter]);

		}
	    } else { printf("Hist %s not found\n", hist_name.Data()); }
	}
	// ----------------------------------------------------


	// Fit hists ------------------------------------------
	TString hist_name = Form("Calo_energy_f%i", beam_to_module_tcells);
	Float_t part_enrg_0 = TMath::Sqrt(scan_value[0]*scan_value[0] + part_mass*part_mass) - part_mass;
	Float_t part_enrg_1 = TMath::Sqrt(scan_value[total_files-1]*scan_value[total_files-1] + part_mass*part_mass) - part_mass;

	gDirectory->cd("Rint:/");
	th1_hist_ptr = ((TH1*)(gDirectory->FindObjectAny( hist_name.Data() )));

	if(th1_hist_ptr)
	{
	    //th1_hist_ptr->Fit("pol1");
	    //FitHistogramm(th1_hist_ptr, mean, sigma, 0, max_scale, "pol1");
	    FitHistogramm(th1_hist_ptr, mean, sigma, 5, 40, "pol1");
	    tfunc_ptr = th1_hist_ptr->GetFunction(fit_funk_name);
	    //tfunc_ptr->SetRange(part_enrg_1-0.2, part_enrg_0+0.2);
	    Double_t a=0, b=0;
	    if(tfunc_ptr)
	    {
		a = tfunc_ptr->GetParameter(0);
		b = tfunc_ptr->GetParameter(1);
	    }
	    th1_hist_ptr->SetTitle(  Form("Calo energy first %i cells {a=%.1f, b=%.1f} %s [%s]; E, GeV",
	                                  beam_to_module_tcells, a, b, scan_name.Data(), calc_type_comment.Data()) );
	}


	// ----------------------------------------------------


	
	
	source_file->Close();
	delete source_file;
    }
    ////////////////////////////////////////////////////////////////////////////////////////
    
    
    
    
    
    ////////////////////////////////////////////////////////////////////////////////////////
    TCanvas *canv = new TCanvas("canv_res", "canv_res");
    result_array->Add(canv);
    TLatex label;
    
    th1_hist_ptr = ((TH1*)(gDirectory->FindObjectAny(  Form("Calo_resolution_f%i", beam_to_module_tcells) )));
    TF1 *calo_res_funk = new TF1("calo_res", "TMath::Sqrt( ([0]/TMath::Sqrt(x))*([0]/TMath::Sqrt(x)) + [1]*[1] + ([2]/x)*([2]/x) )", 1., 150.);
    //TF1 *calo_res_funk = new TF1("calo_res", "TMath::Sqrt(  ([0]/TMath::Sqrt(x))*([0]/TMath::Sqrt(x))  )", 1., 6.);
    th1_hist_ptr->Fit(calo_res_funk, "R", "");


    th1_hist_ptr->Draw();
    label.DrawLatex(.55, .4, "#frac{#sigmaE}{E} = #sqrt{(#frac{a}{#sqrt{E}})^2 + (b)^2 + (#frac{c}{E})^2}");
    label.DrawLatex(.1, .6, Form("a = %.2f; b = %.2f; c = %.2f",
                                 calo_res_funk->GetParameter(0),
                                 calo_res_funk->GetParameter(1),
                                 calo_res_funk->GetParameter(2)) );
    //	label.DrawLatex(.55, .4, "#frac{#sigma^{}_{E}}{E} = #frac{a}{#sqrt{E}}");
    //	label.DrawLatex(.1, .6, Form("a = %.3f", calo_res_funk->GetParameter(0)));
    ////////////////////////////////////////////////////////////////////////////////////////
    
    
    
    
    ////////////////////////////////////////////////////////////////////////////////////////
    printf("\n------------------------------------------------------------------------------\n");
    printf("Resolution full calo first i cells: scan_val"); for(int i=0;i<beam_to_module_tcells;i++) printf(": %i_val : %i_err", i+1, i+1); printf("\n");
    for(Int_t iFile=0; iFile<total_files;iFile++)
    {
	TString iFile_comment = ((TObjString*)(file_comment.At(iFile)))->GetString();
	
	printf("%s : %.1f", iFile_comment.Data(), scan_value[iFile]); for(int i=0;i<beam_to_module_tcells;i++) printf(": %.3f : %.3f", val_res_arr[iFile][i], valerr_res_arr[iFile][i]); printf("\n");
    }
    
    printf("Energy full calo first i cells: scan_val"); for(int i=0;i<beam_to_module_tcells;i++) printf(": %i_val : %i_err", i+1, i+1); printf("\n");
    for(Int_t iFile=0; iFile<total_files;iFile++)
    {
	TString iFile_comment = ((TObjString*)(file_comment.At(iFile)))->GetString();
	
	printf("%s : %.1f", iFile_comment.Data(), scan_value[iFile]); for(int i=0;i<beam_to_module_tcells;i++) printf(": %.3f : %.3f", val_mean_arr[iFile][i], valerr_mean_arr[iFile][i]); printf("\n");
    }
    
    
    
    printf("Resolution mod %i first i cells: scan_val", beam_to_module_N); for(int i=0;i<beam_to_module_tcells;i++) printf(": %i_val : %i_err", i+1, i+1); printf("\n");
    for(Int_t iFile=0; iFile<total_files;iFile++)
    {
	TString iFile_comment = ((TObjString*)(file_comment.At(iFile)))->GetString();
	
	printf("%s : %.1f", iFile_comment.Data(), scan_value[iFile]); for(int i=0;i<beam_to_module_tcells;i++) printf(": %.3f : %.3f", val_res_mod_arr[iFile][i], valerr_res_mod_arr[iFile][i]); printf("\n");
    }
    
    printf("Energy mod %i first i cells: scan_val", beam_to_module_N); for(int i=0;i<beam_to_module_tcells;i++) printf(": %i_val : %i_err", i+1, i+1); printf("\n");
    for(Int_t iFile=0; iFile<total_files;iFile++)
    {
	TString iFile_comment = ((TObjString*)(file_comment.At(iFile)))->GetString();
	
	printf("%s : %.1f", iFile_comment.Data(), scan_value[iFile]); for(int i=0;i<beam_to_module_tcells;i++) printf(": %.3f : %.3f", val_mean_mod_arr[iFile][i], valerr_mean_mod_arr[iFile][i]); printf("\n");
    }
    printf("------------------------------------------------------------------------------\n\n");
    
    
    ////////////////////////////////////////////////////////////////////////////////////////
    
    
    
    
    
    ////////////////////////////////////////////////////////////////////////////////////////
    TFile *result_file = new TFile((source_path + result_file_name + ".root").Data(), "RECREATE");
    
    SaveArrayStructInFile(result_array, result_file);
    printf("file %s was writed\n", (source_path + result_file_name + ".root").Data());
    result_file->Close();
    delete result_file;
    
    for(int i=0; i<total_files; i++)
    {
	delete[] val_mean_arr[i];
	delete[] valerr_mean_arr[i];
	delete[] val_res_arr[i];
	delete[] valerr_res_arr[i];
	
	delete[] val_mean_mod_arr[i];
	delete[] valerr_mean_mod_arr[i];
	delete[] val_res_mod_arr[i];
	delete[] valerr_res_mod_arr[i];
    }
    delete[] val_mean_arr;
    delete[] valerr_mean_arr;
    delete[] val_res_arr;
    delete[] valerr_res_arr;
    
    delete[] val_mean_mod_arr;
    delete[] valerr_mean_mod_arr;
    delete[] val_res_mod_arr;
    delete[] valerr_res_mod_arr;
    
    result_file = new TFile((source_path + result_file_name + ".root").Data(), "READONLY");
    new TBrowser;
    ////////////////////////////////////////////////////////////////////////////////////////
    
}

