

#include <fstream>
#include <iostream>
#include <TNtuple.h>
#include <TFile.h>
#include <TChain.h>
#include <Rtypes.h>
#include <vector>
#include <TBrowser.h>

#include "Readme_reader.h"
#include "event_data_struct.h"
#include "bindataformat.h"
#include "../libraries/utilites/utilites.h"


void muon_calibration()
{
////////////////////////////////////////////////////////////////////////////////////////

    TString source_path = "/mnt/hgfs/DETECTORS_DATA/RESULTS/CALO/root_source/T10_test_sep_17/";
    TString result_path = "/mnt/hgfs/DETECTORS_DATA/RESULTS/CALO/root_results/T10_test_sep_17/";

//    TString result_file_name = result_path + "adc64_17_09_07_23_40_muonScan_mod5_6GeV_calib.root";
    TString result_file_name = result_path + "adc64_17_09_08_13_45_protonScan_mod5_6GeV_calib.root";
//    TString result_file_name = result_path + "adc64_17_09_06_22_44_muonScan_mod5_calib.root";

////////////////////////////////////////////////////////////////////////////////////////





    Readme_reader *readme_reader_ptr = new Readme_reader("/mnt/hgfs/DETECTORS_DATA/SOURCE/CALO/");
    readme_reader_ptr->Set_runfile_name("T10_RUNS");
    readme_reader_ptr->Print();

    Int_t total_modules = readme_reader_ptr->Get_module_num();
    Int_t total_channels = readme_reader_ptr->Get_total_ch();

    // Source & Result file =================
//    TFile *source_file = new TFile(source_file_name.Data(), "READONLY");
//    if(!source_file->IsOpen())
//    {
//	printf("File \"%s\" not found\n", source_file_name.Data());
//	return;
//    }
//    else
//    {
//	printf("File \"%s\" was opened\n", source_file_name.Data());
//    }

//    TTree *data_tree = dynamic_cast<TTree*>(source_file->FindObjectAny("adc64_data"));
//    if(!data_tree)
//    {
//	printf("data tree not found in file\n");
//	return;
//    }

    TChain *data_tree = new TChain;

//    data_tree->AddFile(source_path + "adc64_17_09_06_22_44_muonScan_mod5.root/adc64_data");
//    data_tree->AddFile(source_path + "adc64_17_09_06_23_25_muonScan_mod8.root/adc64_data");
//    data_tree->AddFile(source_path + "adc64_17_09_07_00_02_muonScan_mod2.root/adc64_data");
//    data_tree->AddFile(source_path + "adc64_17_09_07_00_29_muonScan_mod1.root/adc64_data");
//    data_tree->AddFile(source_path + "adc64_17_09_07_00_57_muonScan_mod4.root/adc64_data");
//    data_tree->AddFile(source_path + "adc64_17_09_07_01_26_muonScan_mod7.root/adc64_data");
//    data_tree->AddFile(source_path + "adc64_17_09_07_01_51_muonScan_mod9.root/adc64_data");
//    data_tree->AddFile(source_path + "adc64_17_09_07_02_11_muonScan_mod6.root/adc64_data");
//    data_tree->AddFile(source_path + "adc64_17_09_07_02_29_muonScan_mod3.root/adc64_data");

//    data_tree->AddFile(source_path + "adc64_17_09_07_23_40_muonScan_mod5_6GeV.root/adc64_data");
   data_tree->AddFile(source_path + "adc64_17_09_08_13_45_protonScan_mod5_6GeV.root/adc64_data");
//    data_tree->AddFile(source_path + "adc64_17_09_06_22_44_muonScan_mod5.root/adc64_data");




    TFile *result_file = new TFile(result_file_name.Data(), "RECREATE");
    gDirectory->cd("Rint:/");
    // =============================



    TObjArray *result_array = new TObjArray;
    result_array->SetName("muon_calib");
    result_array->SetOwner();

    TObjArray *canv_array = new TObjArray;
    canv_array->SetName("result_canvas");
    canv_array->SetOwner();
    result_array->Add(canv_array);
    TCanvas *temp_canv = new TCanvas("temp", "temp");

    /************************************************/
    const Int_t max_charge = 30000;
    const Int_t max_charge_bin = 100;
    Int_t pedestal_cut_trsh = 2000;

    Float_t part_time_sel_val[2] = {-.2, .3};
    /************************************************/


    Float_t *muons_calib_coef = new Float_t[total_channels];


    TH1 *th1_hist_ptr = NULL;

    // Partical times =============================================
    th1_hist_ptr = new TH1F("temp", "temp", 2000, -10, 10);


    th1_hist_ptr->SetName(Form("part_time"));
    th1_hist_ptr->SetTitle(Form("Particles start TOF [%.1f, %.1f]", part_time_sel_val[0], part_time_sel_val[1]));
    result_array->Add(th1_hist_ptr);
    // =============================================


    for(Int_t module_iter = 0; module_iter < total_modules; module_iter++)
    {
	TString module_name = readme_reader_ptr->Get_module_info()[module_iter].comment;
	TObjArray *module_data_array = new TObjArray();
	module_data_array->SetName(module_name.Data());
	module_data_array->SetOwner();
	result_array->Add(module_data_array);

	Int_t total_cells = readme_reader_ptr->Get_module_info()[module_iter].total_cells;

	TCanvas *canv_module = new TCanvas(Form("%s", module_name.Data()), Form("%s", module_name.Data()));
	canv_module->Divide(3,3);
	canv_array->Add(canv_module);




	for(Int_t cell_iter = 0; cell_iter < total_cells; cell_iter++)
	{
	    Int_t tree_ch = readme_reader_ptr->GetContCh_from_CellN(module_iter, cell_iter);
	    TString channel_name = event_data_struct::GetChName(tree_ch);

	    printf("Calculating %s: cell %i; tree_ch %i =============================\n", module_name.Data(), cell_iter, tree_ch);


	    // Selection equetions ==============================
	    TString pedes_trsh_allcells_equetion = "(1)";
	    for(Int_t sel_cell = 0; sel_cell < total_cells; sel_cell++)
	    {
		if(sel_cell == cell_iter) continue;
		Int_t sel_ch = readme_reader_ptr->GetContCh_from_CellN(module_iter, sel_cell);
		TString sel_ch_name = event_data_struct::GetChName(sel_ch);
		pedes_trsh_allcells_equetion += Form("&&((%s.integral_in_gate) > %i)",
							sel_ch_name.Data(), pedestal_cut_trsh);
	    }
	    //pedes_trsh_allcells_equetion = "(1)";
	    printf("pedes_trsh_allcells_equetion: %s\n", pedes_trsh_allcells_equetion.Data());

	    TString signal_time_sel_equetion = Form("(%s.time_max_in_gate > %i)&&(%s.time_max_in_gate < %i)",
			       channel_name.Data(), 70,  channel_name.Data(), 90);
	    // ==================================================



	    // Amplitude all cells ===========================
	    th1_hist_ptr = new TH1F("temp", "temp", max_charge_bin, 0, max_charge);

	    canv_module->cd(cell_iter+1);
	    data_tree->Draw(Form("(%s.integral_in_gate)>>temp", channel_name.Data()),
			    (pedes_trsh_allcells_equetion+"&&"+signal_time_sel_equetion);



	    Double_t mean, sigma;
	    FitHistogrammSmartInMaxPeak(th1_hist_ptr, mean, sigma, 1.5);

	    if(module_iter != 4) mean = 1.;
	    muons_calib_coef[readme_reader_ptr->GetContCh_from_CellN(module_iter, cell_iter)] = mean;


	    th1_hist_ptr->SetName(Form("%s_cell_%i_muonampl",module_name.Data(), cell_iter));
	    th1_hist_ptr->SetTitle(Form("Muon Amplitude %s cell %i [mean %.0f sigma %.0f]",
					module_name.Data(), cell_iter, mean, sigma));
	    module_data_array->Add(th1_hist_ptr);
	    // =============================================


	}
    }



    printf("Float_t muons_calib_coef[%i] = {", total_channels);
    for(Int_t ch = 0; ch < total_channels-1; ch++)
	printf("%f, ", muons_calib_coef[ch]);
    printf("%f};\n",muons_calib_coef[total_channels-1]);


    SaveArrayStructInFile(result_array, result_file);


    new TBrowser;


}
