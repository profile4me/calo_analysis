#include <iostream>
#include <fstream>
//#include <conio.h>
#include <ctime>

#include <TFile.h>
#include <TGraph.h>
#include <TF1.h>
#include <TTree.h>
#include <TObjArray.h>
#include <TString.h>
#include <TDirectory.h>
#include <TCanvas.h>
#include <TH1F.h>
#include <TSystemFile.h>
#include <TSystemDirectory.h>
#include <TBrowser.h>
#include <TLegend.h>
#include "TKey.h"
#include "langaus.hh"

#include "data_calculating.h"
#include "Readme_reader_.h"


#include "../libraries/utilites/utilites.h"
//#include "utilites.h"

std::ofstream data_table_n1 ("Data_table_n1.txt");
std::ofstream data_table_hor ("Data_table_hor.txt");

TH2F *hist_int = new TH2F("hist_int", "Integral ratio; Area/HistInt; Chisq", 1000, 0, 10000, 1000, 0, 10000);
TH2F *hist_max = new TH2F("hist_max", "Maximum ratio; MaxPeak/MP; Chisq", 1000, 0, 100, 1000, 0, 10000);

float makeCalib(TH1 *hsum) {

    float x_min = 2000;
    float x_max = 60000;

    Int_t x_min_bin = hsum->FindBin(x_min);
    Int_t x_max_bin = hsum->FindBin(x_max);

    Double_t hist_start_Xmin = hsum->GetXaxis()->GetXmin();
    Double_t hist_start_Xmax = hsum->GetXaxis()->GetXmax();

    hsum->SetAxisRange(x_min, x_max);
    Double_t histMean = hsum->GetMean();
    Double_t histRMS = hsum->GetRMS();
    Double_t histIntegral = hsum->Integral(x_min_bin, x_max_bin);

    Double_t histPeakMax = hsum->GetBinCenter( hsum->GetMaximumBin() );

    hsum->SetAxisRange(hist_start_Xmin, x_max+2000);


    if(histIntegral < 100.) return 0.;

    TF1 *fit = new TF1("langau",langaufun,x_min,x_max,4);
    fit->SetParNames("Width","MP","Area","GSigma");

    fit->SetParameters(1000.,histPeakMax/1.,histIntegral*660.,5000.);


    fit->SetParLimits(1,histPeakMax/1.5,histPeakMax/1.);
    fit->SetParLimits(2,histIntegral*600.,histIntegral*750.);


    hsum->Fit(fit,"","",x_min,x_max);

    Double_t maxx, FWHM;
    langaupro(fit->GetParameters(), maxx, FWHM);

    printf("hist %s : integral = %f; Area/integral = %f; max_peak = %f; maxx = %f; shisquare = %f\n",
	   hsum->GetName(), histIntegral, fit->GetParameter(2)/(float)histIntegral, histPeakMax,  maxx, fit->GetChisquare());
    hist_int->Fill(fit->GetParameter(2)/(float)histIntegral, fit->GetChisquare());
    hist_max->Fill(histPeakMax/fit->GetParameter(1), fit->GetChisquare());

    return (float)maxx;

}


void analysis_macro_results()
{
    //input data
    TString source_path = "/mnt/hgfs/DETECTORS_DATA/RESULTS/CALO/root_results/cosmic_autumn_2017/";
    //TString source_path = "/mnt/hgfs/DETECTORS_DATA/RESULTS/CALO/root_results/";
    //TString source_path = "/media/sf_ADC64/data_processing/ADC64/PROCESSING/root_results/";
    //TString readme_path = "/mnt/hgfs/DETECTORS_DATA/SOURCE/CALO/readme_files/";

    //TString source_file_name = "CALO_analysis_16_new_onemipfit";
    //TString source_file_name = "CALO_analysis_24_allfiles";
    TString source_file_name = "CALO_analysis_003_";
    //TString source_file_name = "CALO_analysis_24_test_R";

    //CBM - NICA : total ch; map; calib

    TString result_file_postfix = "_result_new_coeffs";
    //const Int_t total_Channels = 10;
    const Int_t total_Channels = 7;


    // Calculation CONSTANTS =======
    const Double_t pedescut_nsigma = 4.;
    const Double_t onemip_maxcut_nsigma = 1.5;
    // =============================


    current_out << Form("\n\n\n######################################################\n");
    current_out << Form("#################  DATA ANALYSER RESULTS  ##################\n");
    current_out << Form("######################################################\n\n\n");


    // Calibration data ============
    Double_t calib_nPe[20];
    //    calib_nPe[0] = 0.586526912;
    //    calib_nPe[1] = 0.602630072;
    //    calib_nPe[2] = 0.67048902;
    //    calib_nPe[3] = 0.58837673;
    //    calib_nPe[4] = 0.659346154;
    //    calib_nPe[5] = 0.668894492;
    //    calib_nPe[6] = 0.663090435;
    //    calib_nPe[7] = 0.63973757;
    //    calib_nPe[8] = 0.573594075;
    //    calib_nPe[9] = 0.618200957;

    //    calib_nPe[0] = 1./1.278788;
    //    calib_nPe[1] = 1./1.067014;
    //    calib_nPe[2] = 1./1.182539;
    //    calib_nPe[3] = 1./1.167021;
    //    calib_nPe[4] = 1./0.870970;
    //    calib_nPe[5] = 1./1.096090;
    //    calib_nPe[6] = 1./0.957689;
    //     calib_nPe[7] = 1./1.243638;
    //    calib_nPe[8] = 1./1.291237;
    //    calib_nPe[9] = 1./1.123209;

    //    calib_nPe[0] = 1./2.079853;
    //    calib_nPe[1] = 1./1.971259;
    //    calib_nPe[2] = 1./2.146283;
    //    calib_nPe[3] = 1./1.995116;
    //    calib_nPe[4] = 1./1.978190;
    //    calib_nPe[5] = 1./1.962114;
    //    calib_nPe[6] = 1./2.094842;
    //    calib_nPe[7] = 1./2.121955;
    //    calib_nPe[8] = 1./2.206998;
    //    calib_nPe[9] = 1./2.290622;
    //    calib_nPe[10] = 1./2.290622;

    calib_nPe[0] = 1./191.077000;
    calib_nPe[1] = 1./176.603818;
    calib_nPe[2] = 1./194.957030;
    calib_nPe[3] = 1./175.430885;
    calib_nPe[4] = 1./176.410190;
    calib_nPe[5] = 1./173.237960;
    calib_nPe[6] = 1./188.764617;
    calib_nPe[7] = 1./189.312836;
    calib_nPe[8] = 1./191.718213;
    calib_nPe[9] = 1./194.585714;
    calib_nPe[10] = 1./194.585714;


    //NICA
    calib_nPe[4] = 1./183.251956;
    calib_nPe[3] = 1./182.417851;
    calib_nPe[6] = 1./182.457794;
    calib_nPe[5] = 1./179.824751;
    calib_nPe[2] = 1./178.861136;
    calib_nPe[1] = 1./175.813079;
    calib_nPe[0] = 1./209.621815;
    calib_nPe[7] = 1./209.621815;

    //NICA
    calib_nPe[4] = 1./2.05E+02;
    calib_nPe[3] = 1./2.13E+02;
    calib_nPe[6] = 1./2.04E+02;
    calib_nPe[5] = 1./2.21E+02;
    calib_nPe[2] = 1./1.94E+02;
    calib_nPe[1] = 1./2.08E+02;
    calib_nPe[0] = 1./2.09E+02;
    
    
    
    
    
    

    //  0  1  2  3  4  5  6  7  8  9
    //Int_t get_ch_for_section[10] = {4, 3, 2, 1, 0, 7, 6, 9, 8, 5};
    //Int_t get_ch_for_section[7] = {0,4,1,5,2,6,3}; //NICA
    //Int_t get_ch_for_section[7] = {4,3,6,5,2,1,0}; //NICA
    Int_t get_ch_for_section[7] = {0,1,2,3,4,5,6}; //NICA

    // =============================


    //Readme_reader readme_rd(readme_path);

    data_table_n1 << "Npe Table [selection by one neighboring cell]" << endl;
    data_table_n1 << "file name:Module name:";
    data_table_hor << "Npe Table [horisontal selection]" << endl;
    data_table_hor << "file name:Module name:";
    for(Int_t channel_num = 0; channel_num < total_Channels; channel_num++)
    {
	Int_t section_num = 0;
	for(Int_t sec = 0; sec < total_Channels; sec++)
	    if(get_ch_for_section[sec] == channel_num)
		section_num = sec;
	data_table_n1 << section_num << ": :";
	data_table_hor << section_num << ": :";
    }
    data_table_n1 << endl;
    data_table_hor << endl;


    // #####################################################################
    // Source file; reading file content ====================
    TFile *source_file = new TFile(source_path+source_file_name+".root", "READONLY");
    if(!source_file->IsOpen())
    {
	printf("File \"%s\" not found\n", (source_path+source_file_name+".root").Data());
	return;
    }
    else
    {
	printf("File \"%s\" was opened\n", (source_path+source_file_name+".root").Data());
    }

    //main folder ------------------
    TString main_folder = source_file->GetListOfKeys()->At(0)->GetName();
    gDirectory->cd(main_folder.Data());


    //list of runs -----------------
    TObjArray RunsArray;
    RunsArray.SetOwner();

    TIter keyList(gDirectory->GetListOfKeys());
    TKey* key;
    while ( key = (TKey*)keyList() )
    {
//	if(((TString)key->GetName()).Contains( "adc64_17_02_15_18_10_overnight" )) continue;
//	if(((TString)key->GetName()).Contains( "adc64_17_02_16_17_00_overnight" )) continue;
//	if(((TString)key->GetName()).Contains( "adc64_17_03_09_14_20_overnight" )) continue;
//	if(((TString)key->GetName()).Contains( "adc64_17_03_14_20_45_overnight" )) continue;
//	if(((TString)key->GetName()).Contains( "adc64_17_03_20_15_40_overnight" )) continue;
//	if(((TString)key->GetName()).Contains( "adc64_17_03_27_17_05_overnight" )) continue;
//	if(((TString)key->GetName()).Contains( "adc64_17_04_26_16_58_overnight" )) continue;
//	if(((TString)key->GetName()).Contains( "adc64_17_05_26_14_12_overweekend" )) continue;
//	if(((TString)key->GetName()).Contains( "adc64_17_08_14_17_45_overnight" )) continue;
//	if(((TString)key->GetName()).Contains( "adc64_17_04_18_16_44_overnight" )) continue;
//	if(((TString)key->GetName()).Contains( "adc64_17_05_03_19_08_overnight" )) continue;
//	if(((TString)key->GetName()).Contains( "adc64_17_05_15_16_55_overnight" )) continue;
	//	if(((TString)key->GetName()).Contains( "" )) continue;
	//	if(((TString)key->GetName()).Contains( "" )) continue;
	RunsArray.Add(new TObjString(key->GetName()));
	//printf("%s \n", key->GetName());
    }

    printf("\nList of runs:\n");
    if(RunsArray.GetLast() < 0)
    {
	printf("No runs found\n");
	source_file->Close();
	delete source_file;
	return;
    }
    else
    {
//	for(Int_t iRun = 0; iRun <= RunsArray.GetLast(); iRun++)
//	{
//	    readme_rd.Set_run_name( ((TObjString*)RunsArray.At(iRun))->GetString().Data()  );
//	    printf("%s		 %s\n", ((TObjString*)RunsArray.At(iRun))->GetString().Data(), readme_rd.Read_module_name().Data());
//	}
    }

    //return;



    // number of channels ---------
    printf("\nChannel list:\n");
    printf("Searching runs with %i cannels\n", total_Channels);

    Int_t current_runs;

    for(Int_t iRun = 0; iRun <= RunsArray.GetLast(); iRun++)
    {
	current_runs = 0;
	source_file->cd("/");
	gDirectory->cd(main_folder.Data());
	gDirectory->cd(((TObjString*)RunsArray.At(iRun))->GetString().Data());
	TIter keyList(gDirectory->GetListOfKeys());
	TKey* key;
	while ( key = (TKey*)keyList() )
	{
	    if(((TString)key->GetName()).Contains("channel") && (key->IsFolder())) current_runs++;
	    //printf("%s \n", key->GetName());
	}
	printf("%s: %i\n", ((TObjString*)RunsArray.At(iRun))->GetString().Data(), current_runs);
	//	if(current_runs != total_Channels)
	//	{
	//	    RunsArray.RemoveAt(iRun);
	//	    printf("Run will be excluded\n");
	//	}
    }
    printf("\n\n=================================================\n");
    //=======================================================
    // #####################################################################






    // Result file =================
    TFile *result_file = new TFile((source_path + source_file_name + result_file_postfix + ".root").Data(), "RECREATE");
    gDirectory->cd("Rint:/");
    TObjArray *result_array = new TObjArray;
    result_array->SetName("Results");
    result_array->SetOwner();
    TCanvas *temp_canv = new TCanvas("temp", "temp");
    TObjArray canv_arr;
    // =============================

    TObjArray *fit_hists_arr = new TObjArray();
    fit_hists_arr->SetName("fit_hists_arr");
    fit_hists_arr->SetOwner();
    result_array->Add(fit_hists_arr);
    fit_hists_arr->Add(hist_int);
    fit_hists_arr->Add(hist_max);



    // #####################################################################
    TObjArray *result_hist_array = new TObjArray();
    result_hist_array->SetName("result_hist_array");
    for(Int_t iRun = 0; iRun <= RunsArray.GetLast(); iRun++)
    {
	TString run_name = ((TObjString*)RunsArray.At(iRun))->GetString();
	TObjArray *run_data_array = new TObjArray();
	run_data_array->SetName(run_name.Data());
	run_data_array->SetOwner();
	result_array->Add(run_data_array);

	//readme_rd.Set_run_name( run_name  );
	//TString module_name = readme_rd.Read_module_name();
	TString module_name = run_name.Data();
	data_table_n1 << run_name.Data() << ":";
	data_table_n1 << module_name.Data() << ":";
	data_table_hor << run_name.Data() << ":";
	data_table_hor << module_name.Data() << ":";


	TObjArray *one_mip_hist_arr = new TObjArray();
	one_mip_hist_arr->SetName("one_mip_hist_arr");
	result_file->cd();

	TH1 *onemip_trend = new TH1F(Form("onemip_trend_%s", run_name.Data()), Form("One mip file %s; sector; Npe", run_name.Data()),
				     total_Channels, 0, total_Channels);
	onemip_trend->SetTitle(Form("%s", module_name.Data()));
	onemip_trend->GetYaxis()->SetRangeUser(20, 100);
	onemip_trend->SetLineWidth(3);
	onemip_trend->SetLineColor(1);
	run_data_array->Add(onemip_trend);
	//result_hist_array->Add(onemip_trend);
	one_mip_hist_arr->Add(onemip_trend);


	TH1 *onemip_near2_trend = new TH1F(Form("onemip_near2_trend_%s", run_name.Data()), Form("One mip near two sides file %s", run_name.Data()),
					   total_Channels, 0, total_Channels);
	onemip_near2_trend->SetTitle("Two neighboring cells");
	onemip_near2_trend->GetYaxis()->SetRangeUser(20, 100);
	onemip_near2_trend->SetLineWidth(3);
	onemip_near2_trend->SetLineColor(2);
	run_data_array->Add(onemip_near2_trend);
	result_hist_array->Add(onemip_near2_trend);
	one_mip_hist_arr->Add(onemip_near2_trend);


	TH1 *onemip_near_trend = new TH1F(Form("onemip_near_trend_%s", run_name.Data()), Form("One mip near file %s", run_name.Data()),
					  total_Channels, 0, total_Channels);
	onemip_near_trend->SetTitle("One neighboring cell");
	onemip_near_trend->GetYaxis()->SetRangeUser(20, 100);
	onemip_near_trend->SetLineWidth(2);
	onemip_near_trend->SetLineColor(3);
	run_data_array->Add(onemip_near_trend);
	result_hist_array->Add(onemip_near_trend);
	one_mip_hist_arr->Add(onemip_near_trend);


	TH1 *onemip_hor_trend = new TH1F(Form("onemip_hor_trend_%s", run_name.Data()), Form("One mip horozontal file %s", run_name.Data()),
					 total_Channels, 0, total_Channels);
	onemip_hor_trend->SetTitle("Horisontal selection");
	onemip_hor_trend->GetYaxis()->SetRangeUser(20, 100);
	onemip_hor_trend->SetLineWidth(1);
	onemip_hor_trend->SetLineColor(4);

	run_data_array->Add(onemip_hor_trend);
	result_hist_array->Add(onemip_hor_trend);
	one_mip_hist_arr->Add(onemip_hor_trend);


	for(Int_t channel_num = 0; channel_num < total_Channels; channel_num++)
	{
	    Int_t section_num = 0;
	    for(Int_t sec = 0; sec < total_Channels; sec++)
		if(get_ch_for_section[sec] == channel_num)
		    section_num = sec;

	    TString channel_name = event_data_struct::GetChName(channel_num);
	    Double_t mean, sigma;


	    TObjArray *channel_data_array = new TObjArray();
	    channel_data_array->SetName(Form("ch%s_sec%i_%s", channel_name.Data(),section_num, run_name.Data()));
	    channel_data_array->SetOwner();
	    run_data_array->Add(channel_data_array);

	    // pedestal -----------------------------------
	    TH1 *th1_ptr = (TH1*)source_file->FindObjectAny( Form("%s_%s_pedestal", run_name.Data(), channel_name.Data()));
	    //	    if(!th1_ptr)
	    //	    {
	    //		printf("Pedestal histogramm not found for %s\n",
	    //		       Form("%s_%s_pedestal", run_name.Data(), channel_name.Data()));
	    //		continue;
	    //	    }
	    TF1 *fit_funk = NULL;//th1_ptr->GetFunction(fit_funk_name);
	    //	    if(!fit_funk)
	    //	    {
	    //		printf("Pedestal fit not found for %s; %s\n", run_name.Data(), channel_name.Data());
	    //		continue;
	    //	    }

	    //	    Double_t pedes_trh = fit_funk->GetParameter(1)+pedescut_nsigma*fit_funk->GetParameter(2);
	    Double_t pedes_trh = 7000.;
	    Double_t fit_peak_width = 10000.;
	    Double_t fit_max = 30000.;
	    Double_t amplitude_max = 60000.;
	    // --------------------------------------------


	    // one mip ------------------------------------
	    th1_ptr = (TH1*)source_file->FindObjectAny(Form("%s_%s_onemipamplitude", run_name.Data(), channel_name.Data()));
	    if(!th1_ptr)
	    {
		printf("One mip histogramm not found %s\n", Form("%s_%s_onemipamplitude", run_name.Data(), channel_name.Data()));
		continue;
	    }
	    //	    fit_funk = th1_ptr->GetFunction(fit_funk_name);
	    //	    if(!fit_funk)
	    //	    {
	    //		printf("One mip fit not found for %s; %s\n", run_name.Data(), channel_name.Data());
	    //		continue;
	    //	    }

	    th1_ptr->Rebin(20);

	    //FitHistogramm(th1_ptr, mean, sigma,  pedes_trh, 20000);
	    //fit_funk = th1_ptr->GetFunction(fit_funk_name);




	    th1_ptr->SetTitle(Form("one mip [sec %i, ch %i]", section_num, channel_num));
	    th1_ptr->SetLineColor(1);
	    th1_ptr->SetLineWidth(3);


	    one_mip_hist_arr->Add(th1_ptr);
	    channel_data_array->Add(th1_ptr);


	    //	    Double_t one_mip_mean = fit_funk->GetParameter(1);
	    //	    Double_t one_mip_sigma = fit_funk->GetParameter(2);
	    //Double_t one_mip_mean = makeCalib(th1_ptr);
	    //printf("%s : %f\n", th1_ptr->GetName(), one_mip_mean*calib_nPe[channel_num]);
	    //if((one_mip_mean < 0)||(one_mip_mean > 20000)) one_mip_mean = 0.;

	    //onemip_trend->SetBinContent(section_num+1, one_mip_mean*calib_nPe[channel_num]);
	    //onemip_trend->SetBinError(section_num+1, fit_funk->GetParError(1)*calib_nPe[channel_num]);
	    onemip_trend->GetXaxis()->SetBinLabel(section_num+1,
						  Form("sec %i, ch %i", section_num, channel_num));

	    // --------------------------------------------


	    // one mip near2 ------------------------------------
	    th1_ptr = (TH1*)source_file->FindObjectAny(Form("%s_%s_onemipnear2amplitude", run_name.Data(), channel_name.Data()));
	    if(!th1_ptr)
	    {
		printf("One mip near two sides histogramm not found %s\n", Form("%s_%s_onemipnear2amplitude", run_name.Data(), channel_name.Data()));
		continue;
	    }
	    th1_ptr->SetTitle(Form("one mip near2 [sec %i, ch %i]", section_num, channel_num));
	    th1_ptr->SetLineColor(2);
	    th1_ptr->SetLineWidth(3);

	    one_mip_hist_arr->Add(th1_ptr);
	    channel_data_array->Add(th1_ptr);

	    th1_ptr->Rebin(20);

	    //	    fit_funk = new TF1(fit_funk_name, "gaus", pedes_trh, amplitude_max);
	    //	    th1_ptr->Fit(fit_funk, "QR");
	    //	    delete fit_funk;

	    //FitHistogramm(th1_ptr, mean, sigma,  pedes_trh, one_mip_mean+onemip_maxcut_nsigma*one_mip_sigma);
	    FitHistogrammInMaxPeak(th1_ptr, mean, sigma, fit_peak_width, pedes_trh, fit_max);
	    fit_funk = th1_ptr->GetFunction(fit_funk_name);
	    th1_ptr->SetAxisRange(0, amplitude_max);

	    //mean = makeCalib(th1_ptr);
	    //printf("%s : %f\n", th1_ptr->GetName(), mean*calib_nPe[channel_num]);
	    //if((mean < 0)||(mean > 20000)) mean = 0.;

	    if(fit_funk)
	    {
		//onemip_near2_trend->SetBinContent(section_num+1, mean*calib_nPe[channel_num]);
		onemip_near2_trend->SetBinContent(section_num+1, fit_funk->GetParameter(1)*calib_nPe[channel_num]);
		//onemip_near2_trend->SetBinError(section_num+1, fit_funk->GetParError(1)*calib_nPe[channel_num]);
		onemip_near2_trend->SetBinError(section_num+1, 0.5*fit_funk->GetParameter(2)*calib_nPe[channel_num]);
		fit_funk->SetLineColor(1);
	    }


	    onemip_near2_trend->GetXaxis()->SetBinLabel(section_num+1,
							Form("sec %i, ch %i", section_num, channel_num));
	    // --------------------------------------------



	    // one mip near ------------------------------------
	    th1_ptr = (TH1*)source_file->FindObjectAny(Form("%s_%s_onemipnearamplitude", run_name.Data(), channel_name.Data()));
	    if(!th1_ptr)
	    {
		printf("One mip near sides histogramm not found %s\n", Form("%s_%s_onemipnearamplitude", run_name.Data(), channel_name.Data()));
		continue;
	    }
	    th1_ptr->SetTitle(Form("one mip near [sec %i, ch %i]", section_num, channel_num));
	    th1_ptr->SetLineColor(3);
	    th1_ptr->SetLineWidth(3);

	    one_mip_hist_arr->Add(th1_ptr);
	    channel_data_array->Add(th1_ptr);
	    \
	    th1_ptr->Rebin(20);
	    //	    fit_funk = new TF1(fit_funk_name, "gaus", pedes_trh, amplitude_max);
	    //	    th1_ptr->Fit(fit_funk, "QR");
	    //	    FitHistogramm(th1_ptr, mean, sigma,  pedes_trh, one_mip_mean+onemip_maxcut_nsigma*one_mip_sigma);
	    FitHistogrammInMaxPeak(th1_ptr, mean, sigma, fit_peak_width, pedes_trh, fit_max);
	    fit_funk = th1_ptr->GetFunction(fit_funk_name);
	    th1_ptr->SetAxisRange(0, amplitude_max);

	    //mean = makeCalib(th1_ptr);
	    //printf("%s : %f\n", th1_ptr->GetName(), mean*calib_nPe[channel_num]);
	    //if((mean < 0)||(mean > 20000)) mean = 0.;

	    if(fit_funk)
	    {
		//onemip_near_trend->SetBinContent(section_num+1,mean*calib_nPe[channel_num]);
		onemip_near_trend->SetBinContent(section_num+1, fit_funk->GetParameter(1)*calib_nPe[channel_num]);
		//onemip_near_trend->SetBinError(section_num+1, fit_funk->GetParError(1)*calib_nPe[channel_num]);
		onemip_near_trend->SetBinError(section_num+1, 0.5*fit_funk->GetParameter(2)*calib_nPe[channel_num]);

		data_table_n1 << Form("%.0f:%.0f:",
				      fit_funk->GetParameter(1)*calib_nPe[channel_num],
				      0.5*fit_funk->GetParameter(2)*calib_nPe[channel_num]);
	    }

	    onemip_near_trend->GetXaxis()->SetBinLabel(section_num+1,
						       Form("sec %i, ch %i", section_num, channel_num));
	    // --------------------------------------------


	    // one mip hor ------------------------------------
	    th1_ptr = (TH1*)source_file->FindObjectAny(Form("%s_%s_onemiphoramplitude", run_name.Data(), channel_name.Data()));
	    if(!th1_ptr)
	    {
		printf("One mip horizontal histogramm not found %s\n", Form("%s_%s_onemipamplitude", run_name.Data(), channel_name.Data()));
		continue;
	    }
	    th1_ptr->SetTitle(Form("one mip hor [sec %i, ch %i]", section_num, channel_num));
	    th1_ptr->SetLineColor(4);
	    th1_ptr->SetLineWidth(1);

	    one_mip_hist_arr->Add(th1_ptr);
	    channel_data_array->Add(th1_ptr);

	    th1_ptr->Rebin(100);
	    th1_ptr->GetXaxis()->SetRangeUser(pedes_trh, amplitude_max);

	    mean = th1_ptr->GetMean();
	    printf("%s : %f\n", th1_ptr->GetName(), mean*calib_nPe[channel_num]);
	    if((mean < 0)||(mean > 20000)) mean = 0.;

	    onemip_hor_trend->SetBinContent(section_num+1, mean*calib_nPe[channel_num]);
	    onemip_hor_trend->SetBinError(section_num+1, th1_ptr->GetMeanError()*calib_nPe[channel_num]);
	    onemip_hor_trend->GetXaxis()->SetBinLabel(section_num+1,
						      Form("sec %i, ch %i", section_num, channel_num));

	    data_table_hor << Form("%.0f:%.0f:",
				   mean*calib_nPe[channel_num],
				   th1_ptr->GetMeanError()*calib_nPe[channel_num]);

	    // --------------------------------------------
	}
	data_table_n1 << endl;
	data_table_hor << endl;
	//	TObjArray temp_canv_arr;
	//	temp_canv_arr.AddAll( HDraw(&one_mip_hist_arr, Form("%i", iRun), 3, 4, 4, 0,40,1,2) );
	//	run_data_array->AddAll(&temp_canv_arr);
	//	canv_arr.AddAll(&temp_canv_arr);


	// drawing canvas -----------------------------
	TCanvas *run_canv = new TCanvas(run_name.Data(), run_name.Data());
	run_data_array->Add(run_canv);
	canv_arr.Add(run_canv);

	run_canv->DivideSquare(total_Channels+2);

	run_canv->cd(1);
	//((TH1*)one_mip_hist_arr->At(0))->Draw("");
	//	((TH1*)one_mip_hist_arr->At(1))->Draw("same");
	//	((TH1*)one_mip_hist_arr->At(2))->Draw("same");
	//	((TH1*)one_mip_hist_arr->At(3))->Draw("same");
	TLegend *legend = new TLegend(0,0,1,1);
	legend->SetHeader(Form("%s", module_name.Data()));
	legend->AddEntry((TObject*)0, run_name.Data(), "");
	legend->AddEntry((TH1*)one_mip_hist_arr->At(1),((TH1*)one_mip_hist_arr->At(1))->GetTitle());
	legend->AddEntry((TH1*)one_mip_hist_arr->At(2),((TH1*)one_mip_hist_arr->At(2))->GetTitle());
	legend->AddEntry((TH1*)one_mip_hist_arr->At(3),((TH1*)one_mip_hist_arr->At(3))->GetTitle());
	legend->Draw();
	//run_canv->GetPad(1)->BuildLegend(0,0,1,1)->SetHeader(Form("%s", module_name.Data()),"C");

	run_canv->cd(2);
	//((TH1*)one_mip_hist_arr->At(0))->Draw("");
	((TH1*)one_mip_hist_arr->At(1))->Draw("same");
	((TH1*)one_mip_hist_arr->At(2))->Draw("same");
	((TH1*)one_mip_hist_arr->At(3))->Draw("same");

	//one_mip_hist_arr->Print();

	const Bool_t dr_norm = false;

	Int_t nPad = 3;
	for(Int_t channel_num = 0; channel_num < total_Channels; channel_num++)
	{
	    TString channel_name = event_data_struct::GetChName(channel_num);

	    run_canv->cd(nPad);
	    //printf("npad: %i\n", nPad);

	    TH1* th1_ptr = NULL;
	    TH1* th1_norm_ptr = NULL;
	    //	    th1_ptr = ((TH1*)one_mip_hist_arr->FindObject(  Form("%s_%s_onemipamplitude", run_name.Data(), channel_name.Data())  ));
	    //	    if(th1_ptr)
	    //	    {
	    //		th1_ptr->Draw("");
	    //		//printf("hist: %s\n", th1_ptr->GetName());
	    //	    }

	    th1_ptr = ((TH1*)one_mip_hist_arr->FindObject(  Form("%s_%s_onemipamplitude", run_name.Data(), channel_name.Data())  ));
	    if(th1_ptr)
	    {
		th1_ptr->GetYaxis()->SetRangeUser(1, 10000);
		if(dr_norm){
		    th1_norm_ptr = th1_ptr->DrawNormalized("same",1);
		    if(th1_norm_ptr) th1_norm_ptr->GetYaxis()->SetRangeUser(0, 0.15);
		}
		else
		{
		    th1_ptr->Draw("same");
		}
	    }

	    th1_ptr = ((TH1*)one_mip_hist_arr->FindObject(  Form("%s_%s_onemipnearamplitude", run_name.Data(), channel_name.Data())  ));
	    if(th1_ptr)
	    {
		th1_ptr->GetYaxis()->SetRangeUser(1, 10000);
		if(dr_norm){
		    th1_norm_ptr = th1_ptr->DrawNormalized("same",1);
		    if(th1_norm_ptr) th1_norm_ptr->GetYaxis()->SetRangeUser(0, 0.15);
		}
		else
		{
		    th1_ptr->Draw("same");
		}
	    }

	    th1_ptr = ((TH1*)one_mip_hist_arr->FindObject(  Form("%s_%s_onemipnear2amplitude", run_name.Data(), channel_name.Data())  ));
	    if(th1_ptr)
	    {
		th1_ptr->GetYaxis()->SetRangeUser(1, 10000);
		if(dr_norm){
		    th1_norm_ptr = th1_ptr->DrawNormalized("same",1);
		    if(th1_norm_ptr) th1_norm_ptr->GetYaxis()->SetRangeUser(0, 0.15);
		}
		else
		{
		    th1_ptr->Draw("same");
		}
	    }


	    th1_ptr = ((TH1*)one_mip_hist_arr->FindObject(  Form("%s_%s_onemiphoramplitude", run_name.Data(), channel_name.Data())  ));
	    if(th1_ptr)
	    {
		th1_ptr->GetYaxis()->SetRangeUser(1, 10000);
		if(dr_norm){
		    th1_norm_ptr = th1_ptr->DrawNormalized("same",0.1);
		    if(th1_norm_ptr) th1_norm_ptr->GetYaxis()->SetRangeUser(0, 0.15);
		}
		else
		{
		    th1_ptr->Draw("same");
		}

	    }

	    if(!dr_norm) run_canv->GetPad(nPad)->SetLogy();

	    nPad++;
	}

	// --------------------------------------------


	delete one_mip_hist_arr;
    }
    // #####################################################################

    //    TObjArray temp_canv_arr;
    //    temp_canv_arr.AddAll( HDraw(result_hist_array, Form("Canv_result"), 3, 3, 3, 0,400,0,2) );
    //    result_array->AddAll(&temp_canv_arr);
    //    canv_arr.AddAll(&temp_canv_arr);



    // Saving results ===============
    SaveArrayStructInFile(result_array, result_file);

    if(1){
	for(Int_t i = 0; i < canv_arr.GetLast(); i++)
	{
	    ((TCanvas*)canv_arr.At(i))->SaveAs(Form("%s.pdf(",(source_path + source_file_name + result_file_postfix).Data() ));
	}
	((TCanvas*)canv_arr.At(canv_arr.GetLast()))->SaveAs(Form("%s.pdf)",(source_path + source_file_name + result_file_postfix).Data() ));
    }
    //===============================

    //copy table data file
    gROOT->ProcessLine( Form(".cp Data_table_n1.txt %s",(source_path + source_file_name + result_file_postfix + "_data_table_n1.txt").Data()) );
    gROOT->ProcessLine( Form(".cp Data_table_hor.txt %s",(source_path + source_file_name + result_file_postfix + "_data_table_hor.txt").Data()) );


    delete result_hist_array;

    delete temp_canv;
    delete result_array;

    source_file->Close();
    delete source_file;

    result_file->Close();
    delete result_file;

    new TBrowser;
}










