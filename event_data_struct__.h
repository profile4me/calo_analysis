#ifndef EVENT_DATA_STRUCT
#define EVENT_DATA_STRUCT

#include<TTree.h>
#include<TString.h>
#include<TMath.h>
#include<TObject.h>


//data processed from waveform
struct event_data_struct
{

    UInt_t time_stamp;// 32bit
    UInt_t time_stamp_ns;
    UInt_t integral_in_gate;

    Float_t mean_in_gate;
    Float_t mean_out_gate;
    Float_t RMS_in_gate;
    Float_t RMS_out_gate;
    // 32*7 = 224

    UShort_t MAX_in_gate;//16
    UShort_t MIN_in_gate;
    UShort_t MAX_out_gate;
    UShort_t MIN_out_gate;
    //64

    UChar_t time_max_in_gate;//8
    UChar_t time_min_in_gate;
    //16

    //304 + 16void = 64*5
    //sizeof = 40 ok

    void reset()
    {
	time_stamp = 0;
	time_stamp_ns = 0;

	integral_in_gate = 0; //integral / gate_width

	MAX_in_gate = 0;
	MIN_in_gate = 0;
	MAX_out_gate = 0;
	MIN_out_gate = 0;

	time_max_in_gate = 0;
	time_min_in_gate = 0;

	mean_in_gate = 0.;
	mean_out_gate = 0.;
	RMS_in_gate = 0.;
	RMS_out_gate = 0.;
    }

    void Print()
    {
	printf("[time %d:%d; integral_in_gate %i; MAX_in_gate %i; MIN_in_gate %i; MAX_out_gate %i; MIN_out_gate %i; time_max_in_gate %i; time_min_in_gate %i; mean_in_gate %.2f; mean_out_gate %.2f; RMS_in_gate %.2f; RMS_out_gate %.2f; ]",
	       time_stamp, time_stamp_ns, integral_in_gate, MAX_in_gate, MIN_in_gate, MAX_out_gate, MIN_out_gate, time_max_in_gate, time_min_in_gate, mean_in_gate, mean_out_gate, RMS_in_gate, RMS_out_gate);
    }

    static TString GetChName(Int_t channel_num)
    {
	return TString::Format("channel_%i", channel_num);
    }

    TBranch* CreateBranch(TTree *tree, Int_t channel_num)
    {
	return tree->Branch(GetChName(channel_num).Data(), this,
			    "time_stamp/i:time_stamp_ns:integral_in_gate:mean_in_gate/F:mean_out_gate:RMS_in_gate:RMS_out_gate:MAX_in_gate/s:MIN_in_gate:MAX_out_gate:MIN_out_gate:time_max_in_gate/b:time_min_in_gate");
    }


    Int_t SetBranch(TTree *tree, Int_t channel_num)
    {
	return tree->SetBranchAddress(GetChName(channel_num).Data(), this);
    }

};


struct event_log_data_struct
{
    Int_t temp;
    Int_t LED_HV;
    Int_t PinDiode;

    void reset()
    {
	temp = 0;
	LED_HV = 0;
	PinDiode = 0;
    }

    void Print()
    {
	printf("[temp %.2fC; LED_HV %i; Pin Diode %i]\n", (((Float_t)temp)/100.), LED_HV, PinDiode);
    }


    TBranch* CreateBranch(TTree *tree, Int_t board_id = 0)
    {
	return tree->Branch(Form("log_data_%i", board_id), this,"temp/I:LED_HV:PinDiode");
    }


    Int_t SetBranch(TTree *tree, Int_t board_id = 0)
    {
	return tree->SetBranchAddress(Form("log_data_%i", board_id), this);
    }

    event_log_data_struct& operator= (event_log_data_struct const& rth)
    {
	if (this != &rth)
	{
	    this->temp = rth.temp;
	    this->LED_HV = rth.LED_HV;
	    this->PinDiode = rth.PinDiode;
	}

	return *this;
    }

};



class Log_file_reader
{
public:
    Log_file_reader(TString file_path);
    ~Log_file_reader();

    void Search_entry(event_log_data_struct &log_data_str, Long64_t timestamp_search, Int_t boardId);

private:
    event_log_data_struct *log_data_arr;
    Long64_t *log_timestamp_arr;
    Int_t *BoardId_arr;
    Int_t nLogsLines;
};

Log_file_reader::~Log_file_reader()
{
    if(log_data_arr) delete[] log_data_arr;
    if(log_timestamp_arr) delete[] log_timestamp_arr;
    if(BoardId_arr) delete[] BoardId_arr;
}

void Log_file_reader::Search_entry(event_log_data_struct &log_data_lnk, Long64_t timestamp_search, Int_t boardId_search)
{
    Long64_t timestamp_prev = -1;
    for(Int_t indx = 0; indx < nLogsLines; indx++)
    {
	if(BoardId_arr[indx] != boardId_search) continue;

	if(timestamp_prev == -1)
	{
	    timestamp_prev = log_timestamp_arr[indx];
	    continue;
	}

	if((log_timestamp_arr[indx] <= timestamp_search)&&
	   (log_timestamp_arr[indx] >= timestamp_prev))
	{
	    log_data_lnk = log_data_arr[indx];
	    return;
	}

	timestamp_prev = log_timestamp_arr[indx];
    }
}

Log_file_reader::Log_file_reader(TString file_path)
{

    log_data_arr = NULL;
    log_timestamp_arr = NULL;
    BoardId_arr = NULL;


    std::ifstream log_file;
    log_file.open(file_path.Data());
    if (!log_file.is_open())
    {
	printf("\nWARING: Log file \"%s\" was NOT opened\n", file_path.Data());
	return;
    }

    printf("\n\n--------------------------\n");
    printf("Loading log file \"%s\"\n", file_path.Data());


    char char_buffer[1000];
    TString *tstring_buffer = new TString(char_buffer);

    //numbers of line
    const Int_t n_first_lines = 100000;
    std::streampos line_n_size, file_size;
    log_file.seekg( 0, std::ios::beg );
    line_n_size = log_file.tellg();
    file_size = log_file.tellg();

    nLogsLines = -1;
    for(Int_t line = 1; line <= n_first_lines; line++)
    {
	log_file.getline(char_buffer, 1000);
	if(log_file.eof())
	{
	    nLogsLines = line;
	    break;
	}
    }

    if(nLogsLines<0)
    {
	line_n_size = log_file.tellg() - line_n_size;
	log_file.seekg( 0, std::ios::end );
	file_size = log_file.tellg() - file_size;
	nLogsLines = (int)ceil(( (float)file_size / (float)line_n_size )*n_first_lines);
    }

    printf("Estimate log lines: %i\n", nLogsLines);
    //log_file.seekg( 0, std::ios::beg );
    log_file.close();
    log_file.open(file_path.Data());
    if (!log_file.is_open())
    {
	printf("\nWARING: Log file \"%s\" was NOT opened\n", file_path.Data());
	nLogsLines = -1;
	return;
    }

    log_data_arr = new event_log_data_struct[nLogsLines];
    log_timestamp_arr = new Long64_t[nLogsLines];
    BoardId_arr = new Int_t[nLogsLines];


    Int_t line_curr = 0;
    for(Int_t line = 0; line < nLogsLines; line++)
    {
	log_file.getline(char_buffer, 1000);
	if(log_file.eof()) break;

	*tstring_buffer = char_buffer;

	TObjArray *str_words = tstring_buffer->Tokenize(",");

	if(str_words->GetLast() < 4)
	{
	    delete str_words;
	    continue;
	}

	Long64_t readed_timestamp = ((TObjString*)str_words->At(0))->GetString().Atoll();
	log_timestamp_arr[line_curr] = (long long int)(floor(readed_timestamp * 0.001));
	BoardId_arr[line_curr] =  (Int_t)(((TObjString*)str_words->At(1))->GetString().Atof());
	log_data_arr[line_curr].temp =  (Int_t)(((TObjString*)str_words->At(2))->GetString().Atof()*100.);
	log_data_arr[line_curr].LED_HV =  ((TObjString*)str_words->At(3))->GetString().Atoi();
	log_data_arr[line_curr].PinDiode =  ((TObjString*)str_words->At(4))->GetString().Atoi();
	//printf("%s : %i \n", ((TObjString*)str_words->At(4))->GetString().Data(), log_data_arr[line_curr].PinDiode);
	//log_data_arr[line_curr].Print();
	line_curr++;

	delete str_words;
    }

    nLogsLines = line_curr;

    time_t log_start = log_timestamp_arr[0];
    time_t log_end = log_timestamp_arr[nLogsLines-1];
    printf("Readed: %i lines\n", nLogsLines);
    printf("Log first line: %s", asctime( localtime( &log_start ) ) );
    printf("Log last line: %s", asctime( localtime( &log_end ) ) );
    printf("--------------------------\n\n");

    delete tstring_buffer;
    log_file.close();

}




#endif // EVENT_DATA_STRUCT

