

#include <fstream>
#include <iostream>
#include <TNtuple.h>
#include <TFile.h>
#include <TChain.h>
#include <Rtypes.h>
#include <vector>
#include <TBrowser.h>

#include "Readme_reader.h"
#include "event_data_struct.h"
#include "bindataformat.h"
#include "../libraries/utilites/utilites.h"


void muon_calib_print_results(TString source_file_name)
{
////////////////////////////////////////////////////////////////////////////////////////

    TString source_path = "/mnt/hgfs/DETECTORS_DATA/RESULTS/CALO/root_results/T10_test_sep_17/";

    TString result_file_name = source_path + source_file_name + "_print.root";

////////////////////////////////////////////////////////////////////////////////////////





    Readme_reader *readme_reader_ptr = new Readme_reader("/mnt/hgfs/DETECTORS_DATA/SOURCE/CALO/");
    readme_reader_ptr->Set_runfile_name("T10_RUNS");
    //readme_reader_ptr->Print();

    Int_t total_modules = readme_reader_ptr->Get_module_num();

    // Source & Result file =================
    TFile *source_file = new TFile((source_path + source_file_name + ".root").Data(), "READONLY");
    if(!source_file->IsOpen())
    {
	printf("File \"%s\" not found\n", source_file_name.Data());
	return;
    }
    else
    {
	printf("File \"%s\" was opened\n", source_file_name.Data());
    }


    TFile *result_file = new TFile(result_file_name.Data(), "RECREATE");
    gDirectory->cd("Rint:/");

    TObjArray *result_array = new TObjArray;
    result_array->SetName("muon_calib");
    result_array->SetOwner();

    TObjArray *canv_array = new TObjArray;
    canv_array->SetName("result_canvas");
    canv_array->SetOwner();
    result_array->Add(canv_array);
    TCanvas *temp_canv = new TCanvas("temp", "temp");

    for(Int_t module_iter = 0; module_iter < total_modules; module_iter++)
    {
	TString module_name = readme_reader_ptr->Get_module_info()[module_iter].comment;
//	TObjArray *module_data_array = new TObjArray();
//	module_data_array->SetName(module_name.Data());
//	module_data_array->SetOwner();
//	result_array->Add(module_data_array);

	Int_t total_cells = readme_reader_ptr->Get_module_info()[module_iter].total_cells;

	TCanvas *canv_module = new TCanvas(Form("%s", module_name.Data()), Form("%s", module_name.Data()));
	//canv_module->Divide(4,3);
	canv_module->DivideSquare(total_cells);
	canv_array->Add(canv_module);


	for(Int_t cell_iter = 0; cell_iter < total_cells; cell_iter++)
	{
	    Int_t tree_ch = readme_reader_ptr->GetContCh_from_CellN(module_iter, cell_iter);
	    TString channel_name = event_data_struct::GetChName(tree_ch);

	    TString hist_name = Form("%s_cell_%i_muonampl",module_name.Data(), cell_iter);
	    TH1 *th1_hist_ptr = (TH1*)(source_file->FindObjectAny(hist_name.Data()));
	    if(!th1_hist_ptr)
	    {
		printf("histogram %s not found\n", hist_name.Data());
		continue;
	    }

	    canv_module->cd(cell_iter+1);
	    th1_hist_ptr->Draw();

	}
    }




    SaveArrayStructInFile(result_array, result_file);

    if(1){
	for(Int_t i = 0; i < canv_array->GetLast(); i++)
	{
	    ((TCanvas*)canv_array->At(i))->SaveAs(Form("%s.pdf(",(source_path + source_file_name + "_printpdf").Data() ));
	}
	((TCanvas*)canv_array->At(canv_array->GetLast()))->SaveAs(Form("%s.pdf)",(source_path + source_file_name + "_printpdf").Data() ));
    }


    new TBrowser;


}
