//
//    Copyright 2015 AFI Electronics
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

#ifndef MPDRAWTYPES
#define MPDRAWTYPES

// === mstream-EventBuilder formats ===

enum {
    MPD_SYNC_NORM = 0x2A502A50,
    MPD_SYNC_EOB  = 0x4A624A62,
    MPD_SYNC_RUN_START = 0x72617453,
    MPD_SYNC_RUN_STOP  = 0x706F7453,
    MPD_SYNC_RUN_NUMBER = 0x236E7552,
    MPD_SYNC_RUN_INDEX = 0x78646E49
};

#pragma pack(push, 1)

struct __attribute__ ((packed)) MpdHdr
{
    MpdHdr()
	: sync(0), length(0) {}
    quint32 sync;
    quint32 length; // payload size in bytes
};

struct __attribute__ ((packed)) MpdRunNumberHdr
{
    MpdRunNumberHdr()
	: sync(), length(0) {}
    quint32 sync;
    quint32 length; // payload size in bytes
    quint32 runNumber;
};

struct __attribute__ ((packed)) MpdEventHdr
{
    MpdEventHdr()
	: sync(MPD_SYNC_NORM), length(0), evNum(0) {}
    const quint32 sync;
    quint32 length; // payload size in bytes
    quint32 evNum;
};

struct __attribute__ ((packed)) MpdDeviceHdr
{
//    MpdDeviceHdr()
//        : deviceSerial(0), length(0), deviceId(0) {}
    unsigned int deviceSerial:32;
    unsigned int length:24; // payload size in bytes
    unsigned char  deviceId:8;
};

struct __attribute__ ((packed)) MpdMStreamHdr
{
    MpdMStreamHdr()
	: subtype(0), words32b(0), usrDefBits(0) {}
    quint8  subtype:2;
    quint32 words32b:22; // payload size in 32-bit words
    quint8  usrDefBits; // Adc64-channal, HRB-exactTrigPos
};

struct __attribute__ ((packed)) AdcDataType0
{
    AdcDataType0()
	: taiSec(0), taiFlags(0), taiNSec(0), chLower(0), chUpper(0) {}
    quint32 taiSec;
    quint32 taiFlags:2;
    quint32 taiNSec:30;
    quint32 chLower; // readout channels 31:0 bit mask
    quint32 chUpper; // readout channels 63:32 bit mask
};

struct __attribute__ ((packed)) AdcDataType1
{
    AdcDataType1()
	: wfTsHi(0), wfTsLow(0) {}
    quint32 wfTsHi;
    quint8 taiFlags:2;
    quint32 wfTsLow:30;
};

#pragma pack(pop)

#endif // MPDRAWTYPES
