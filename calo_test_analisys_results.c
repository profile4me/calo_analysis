

#include <fstream>
#include <iostream>
#include <TNtuple.h>
#include <TFile.h>
#include <TChain.h>
#include <Rtypes.h>
#include <vector>
#include <TBrowser.h>
#include <TObject.h>
#include <TLatex.h>

#include "Readme_reader.h"
#include "event_data_struct.h"
#include "bindataformat.h"
#include "../libraries/utilites/utilites.h"


void  calo_test_analisys_results()
{
    ////////////////////////////////////////////////////////////////////////////////////////

    TString source_path = "/mnt/hgfs/DETECTORS_DATA/RESULTS/CALO/root_results/T10_test_sep_17/";


    ////////////////////////////////////////////////////////////////////////////////////////





    Readme_reader *readme_reader_ptr = new Readme_reader("");
    readme_reader_ptr->SetInfoFile("/mnt/hgfs/DETECTORS_DATA/RESULTS/CALO/root_source/T10_test_sep_17/adc64_17_09_08_16_22_protonScan_mod5_5.5GeV.txt");
    //readme_reader_ptr->Print();

    Int_t total_modules = readme_reader_ptr->Get_module_num();

    TObjArray files_names;


//    TString part_name = "pions";
//    files_names.Add(new TObjString("adc64_17_09_08_13_45_protonScan_mod5_6GeV_protoncalib_3_pions.root"));
//    files_names.Add(new TObjString("adc64_17_09_08_16_22_protonScan_mod5_5.5GeV_protoncalib_3_pions.root"));
//    files_names.Add(new TObjString("adc64_17_09_08_18_11_protonScan_mod5_5GeV_protoncalib_3_pions.root"));
//    files_names.Add(new TObjString("adc64_17_09_08_19_30_protonScan_mod5_4.5GeV_protoncalib_3_pions.root"));
//    files_names.Add(new TObjString("adc64_17_09_08_20_50_protonScan_mod5_4GeV_protoncalib_3_pions.root"));
//    files_names.Add(new TObjString("adc64_17_09_08_22_55_protonScan_mod5_3.5GeV_protoncalib_3_pions.root"));
//    files_names.Add(new TObjString("adc64_17_09_09_02_20_protonScan_mod5_3.0GeV_protoncalib_3_pions.root"));
//    files_names.Add(new TObjString("adc64_17_09_09_17_00_protonScan_mod5_2.5GeV_protoncalib_3_pions.root"));
//    files_names.Add(new TObjString("adc64_17_09_09_11_15_protonScan_mod5_2.0GeV_protoncalib_3_pions.root"));
//    files_names.Add(new TObjString("adc64_17_09_12_03_54_protonScan_mod5_1.5GeV_protoncalib_3_pions.root"));


//    TString part_name = "protons";
//    files_names.Add(new TObjString("adc64_17_09_08_13_45_protonScan_mod5_6GeV_protoncalib_3_protons.root"));
//    files_names.Add(new TObjString("adc64_17_09_08_16_22_protonScan_mod5_5.5GeV_protoncalib_3_protons.root"));
//    files_names.Add(new TObjString("adc64_17_09_08_18_11_protonScan_mod5_5GeV_protoncalib_3_protons.root"));
//    files_names.Add(new TObjString("adc64_17_09_08_19_30_protonScan_mod5_4.5GeV_protoncalib_3_protons.root"));
//    files_names.Add(new TObjString("adc64_17_09_08_20_50_protonScan_mod5_4GeV_protoncalib_3_protons.root"));
//    files_names.Add(new TObjString("adc64_17_09_08_22_55_protonScan_mod5_3.5GeV_protoncalib_3_protons.root"));
//    files_names.Add(new TObjString("adc64_17_09_09_02_20_protonScan_mod5_3.0GeV_protoncalib_3_protons.root"));
//    files_names.Add(new TObjString("adc64_17_09_09_17_00_protonScan_mod5_2.5GeV_protoncalib_3_protons.root"));
//    files_names.Add(new TObjString("adc64_17_09_09_11_15_protonScan_mod5_2.0GeV_protoncalib_3_protons.root"));
//    files_names.Add(new TObjString("adc64_17_09_12_03_54_protonScan_mod5_1.5GeV_protoncalib_3_protons.root"));

//    TString part_name = "protons";
//    files_names.Add(new TObjString("adc64_17_09_08_13_45_protonScan_mod5_6GeV_protoncalib_4_summ_onlyTOF_protons.root"));
//    files_names.Add(new TObjString("adc64_17_09_08_16_22_protonScan_mod5_5.5GeV_protoncalib_4_summ_onlyTOF_protons.root"));
//    files_names.Add(new TObjString("adc64_17_09_08_18_11_protonScan_mod5_5GeV_protoncalib_4_summ_onlyTOF_protons.root"));
//    files_names.Add(new TObjString("adc64_17_09_08_19_30_protonScan_mod5_4.5GeV_protoncalib_4_summ_onlyTOF_protons.root"));
//    files_names.Add(new TObjString("adc64_17_09_08_20_50_protonScan_mod5_4GeV_protoncalib_4_summ_onlyTOF_protons.root"));
//    files_names.Add(new TObjString("adc64_17_09_08_22_55_protonScan_mod5_3.5GeV_protoncalib_4_summ_onlyTOF_protons.root"));
//    files_names.Add(new TObjString("adc64_17_09_09_02_20_protonScan_mod5_3.0GeV_protoncalib_4_summ_onlyTOF_protons.root"));
//    files_names.Add(new TObjString("adc64_17_09_09_17_00_protonScan_mod5_2.5GeV_protoncalib_4_summ_onlyTOF_protons.root"));
//    files_names.Add(new TObjString("adc64_17_09_09_11_15_protonScan_mod5_2.0GeV_protoncalib_4_summ_onlyTOF_protons.root"));
//    files_names.Add(new TObjString("adc64_17_09_12_03_54_protonScan_mod5_1.5GeV_protoncalib_4_summ_onlyTOF_protons.root"));

//    TString part_name = "protons";
//    files_names.Add(new TObjString("adc64_17_09_08_13_45_protonScan_mod5_6GeV_protoncalib_8_tof_timegate_cellped_protons.root"));
//    files_names.Add(new TObjString("adc64_17_09_08_16_22_protonScan_mod5_5.5GeV_protoncalib_8_tof_timegate_cellped_protons.root"));
//    files_names.Add(new TObjString("adc64_17_09_08_18_11_protonScan_mod5_5GeV_protoncalib_8_tof_timegate_cellped_protons.root"));
//    files_names.Add(new TObjString("adc64_17_09_08_19_30_protonScan_mod5_4.5GeV_protoncalib_8_tof_timegate_cellped_protons.root"));
//    files_names.Add(new TObjString("adc64_17_09_08_20_50_protonScan_mod5_4GeV_protoncalib_8_tof_timegate_cellped_protons.root"));
//    files_names.Add(new TObjString("adc64_17_09_08_22_55_protonScan_mod5_3.5GeV_protoncalib_8_tof_timegate_cellped_protons.root"));
//    files_names.Add(new TObjString("adc64_17_09_09_02_20_protonScan_mod5_3.0GeV_protoncalib_8_tof_timegate_cellped_protons.root"));
//    files_names.Add(new TObjString("adc64_17_09_09_17_00_protonScan_mod5_2.5GeV_protoncalib_8_tof_timegate_cellped_protons.root"));
//    files_names.Add(new TObjString("adc64_17_09_09_11_15_protonScan_mod5_2.0GeV_protoncalib_8_tof_timegate_cellped_protons.root"));
//    files_names.Add(new TObjString("adc64_17_09_12_03_54_protonScan_mod5_1.5GeV_protoncalib_8_tof_timegate_cellped_protons.root"));

    TString part_name = "pions";
    files_names.Add(new TObjString("adc64_17_09_08_13_45_protonScan_mod5_6GeV_protoncalib_8_tof_timegate_cellped_pions.root"));
    files_names.Add(new TObjString("adc64_17_09_08_16_22_protonScan_mod5_5.5GeV_protoncalib_8_tof_timegate_cellped_pions.root"));
    files_names.Add(new TObjString("adc64_17_09_08_18_11_protonScan_mod5_5GeV_protoncalib_8_tof_timegate_cellped_pions.root"));
    files_names.Add(new TObjString("adc64_17_09_08_19_30_protonScan_mod5_4.5GeV_protoncalib_8_tof_timegate_cellped_pions.root"));
    files_names.Add(new TObjString("adc64_17_09_08_20_50_protonScan_mod5_4GeV_protoncalib_8_tof_timegate_cellped_pions.root"));
    files_names.Add(new TObjString("adc64_17_09_08_22_55_protonScan_mod5_3.5GeV_protoncalib_8_tof_timegate_cellped_pions.root"));
    files_names.Add(new TObjString("adc64_17_09_09_02_20_protonScan_mod5_3.0GeV_protoncalib_8_tof_timegate_cellped_pions.root"));
    files_names.Add(new TObjString("adc64_17_09_09_17_00_protonScan_mod5_2.5GeV_protoncalib_8_tof_timegate_cellped_pions.root"));
    files_names.Add(new TObjString("adc64_17_09_09_11_15_protonScan_mod5_2.0GeV_protoncalib_8_tof_timegate_cellped_pions.root"));
    files_names.Add(new TObjString("adc64_17_09_12_03_54_protonScan_mod5_1.5GeV_protoncalib_8_tof_timegate_cellped_pions.root"));



//    TString part_name = "protons";
//	files_names.Add(new TObjString("adc64_17_09_10_09_00_positionScan_mod54_0cm_6GeV_protoncalib_3_protons.root"));
//	files_names.Add(new TObjString("adc64_17_09_10_09_00_positionScan_mod54_3cm_6GeV_protoncalib_3_protons.root"));
//	files_names.Add(new TObjString("adc64_17_09_10_11_21_positionScan_mod54_6cm_6GeV_protoncalib_3_protons.root"));
//	files_names.Add(new TObjString("adc64_17_09_10_12_29_positionScan_mod54_9cm_6GeV_protoncalib_3_protons.root"));
//	files_names.Add(new TObjString("adc64_17_09_10_13_47_positionScan_mod54_12cm_6GeV_protoncalib_3_protons.root"));
//	files_names.Add(new TObjString("adc64_17_09_10_15_15_positionScan_mod54_14cm_6GeV_protoncalib_3_protons.root"));


    Int_t total_files = files_names.GetLast()+1;
    Float_t *part_momentum = new Float_t[100];
    int i = 0;
    part_momentum[i++] = 6.;
    part_momentum[i++] = 5.5;
    part_momentum[i++] = 5.;
    part_momentum[i++] = 4.5;
    part_momentum[i++] = 4.;
    part_momentum[i++] = 3.5;
    part_momentum[i++] = 3.;
    part_momentum[i++] = 2.5;
    part_momentum[i++] = 2.;
    part_momentum[i++] = 1.5;

    Float_t *x_position = new Float_t[100];
    i = 0;
    x_position[i++] = 0.;
    x_position[i++] = 3.;
    x_position[i++] = 6.;
    x_position[i++] = 9.;
    x_position[i++] = 12.;
    x_position[i++] = 14.;

    Float_t part_mass;
    if(part_name == "pions") part_mass = .135;
    else if(part_name == "protons") part_mass = .938;
    else part_mass = 0.;



    //    0,135	0,938
    //    5,866518558	5,134877736
    //    5,366656569	4,641412514
    //    4,866822168	4,149223604
    //    4,367024545	3,658721005
    //    3,867277477	3,170508732
    //    3,367602604	2,68551266
    //    2,868035964	2,205221914
    //    2,368642347	1,732176773
    //    1,869551072	1,271036894
    //    1,371062748	0,831136513



    Int_t ncells = 10;
    TString comment = Form("full calo, first %i cells",ncells);
    TString result_file_name = part_name + Form("_fullcalo_%icells_result_4_newsel", ncells);
    result_file_name = source_path + result_file_name;





    TObjArray *result_array = new TObjArray;
    result_array->SetName("calo_results");
    result_array->SetOwner();

    TH1* calo_enrg_resolution = new TH1F("Calo_resolution", Form("Energy resolution for %s {%s}; E, GeV", part_name.Data(), comment.Data()), 60, 0, 7);
    result_array->Add(calo_enrg_resolution);

    TH1* calo_enrg_mean = new TH1F("Calo_mean", Form("Energy for %s {%s}; E, GeV; Ecalo, MeV", part_name.Data(), comment.Data()), 60, 0, 7);
    result_array->Add(calo_enrg_mean);

    TH1* calo_enrg_resolution_ifile = new TH1F("Calo_resolution_ifile", Form("Energy resolution for %s {%s}; x, cm", part_name.Data(), comment.Data()), 1000, -1, 15);
    result_array->Add(calo_enrg_resolution_ifile);

    TH1* calo_enrg_mean_ifile = new TH1F("Calo_mean_ifile", Form("Energy for %s {%s}; x, cm; Ecalo, MeV", part_name.Data(), comment.Data()), 1000, -1, 15);
    result_array->Add(calo_enrg_mean_ifile);


    TH1 *th1_hist_ptr = NULL;
    TH2 *th2_hist_ptr = NULL;
    TF1 *fit_funk = NULL;


    for(Int_t iFile=0; iFile<total_files;iFile++)
    {
	Int_t total_cells = readme_reader_ptr->Get_module_info()[4].total_cells;
	TString module_name = readme_reader_ptr->Get_module_info()[4].comment;
	TString iFile_name = ((TObjString*)(files_names.At(iFile)))->GetString();
	iFile_name = source_path + iFile_name;
	TString iFile_comment = Form("%s_%.1f_GeV_c", part_name.Data(), part_momentum[iFile]);

	Float_t part_enrg = TMath::Sqrt(part_momentum[iFile]*part_momentum[iFile] + part_mass*part_mass) - part_mass;
	Float_t x_pos = x_position[iFile];


	TFile *source_file = new TFile(iFile_name.Data(), "READONLY");
	if(!source_file->IsOpen())
	{
	    printf("File \"%s\" not found\n", iFile_name.Data());
	    return;
	}
	else
	{
	    printf("File \"%s\" was opened\n", iFile_name.Data());
	}





	th1_hist_ptr = ((TH1*)(source_file->FindObjectAny(  Form("%s_module_halfversus", module_name.Data()) )));
	Int_t all_ent = th1_hist_ptr->GetEntries();
	th1_hist_ptr = ((TH1*)(source_file->FindObjectAny(  Form("%s_module_halfversus_sel", module_name.Data()) )));
	Int_t muons_ent = th1_hist_ptr->GetEntries();
	printf("E %.1f moun/proton %.1f%%\n", part_enrg, ((float)muons_ent/(float)all_ent)*100.);

	gDirectory->cd("Rint:/");
	TH1* calo_res_n_modules = new TH1F(Form("Calo_resolution_nmod_%s",iFile_comment.Data()),
					   Form("%s {%.1f GeV/c};first n cells; calo resolution",
						part_name.Data(), part_momentum[iFile]),
					   1000,-1, total_cells+2);
	result_array->Add(calo_res_n_modules);
	for(Int_t cell_iter = 0; cell_iter < total_cells; cell_iter++)
	{
	    th1_hist_ptr = ((TH1*)(source_file->FindObjectAny(  Form("calo_energy_first_%i",cell_iter+1) )));
	    //th1_hist_ptr = ((TH1*)(source_file->FindObjectAny(  Form("%s_module_energy_first_%i",module_name.Data(), 10) )));
	    th1_hist_ptr = (TH1*)(  th1_hist_ptr->Clone(Form("CaloEnrg_cell%i_%s",cell_iter+1, iFile_comment.Data()))  );
	    result_array->Add(th1_hist_ptr);

	    Double_t mean, sigma, mean_err, sigma_err, resolution, resolution_err;
	    FitHistogrammSmartInMaxPeak(th1_hist_ptr, mean, sigma, 1.5, 15., 300.);
	    fit_funk = th1_hist_ptr->GetFunction(fit_funk_name);
	    if(fit_funk)
	    {
		mean_err = fit_funk->GetParError(1);
		sigma_err = fit_funk->GetParError(2);
	    }

	    resolution = sigma/mean;
	    resolution_err = 1/mean*TMath::Sqrt(sigma_err*sigma_err + (sigma*mean_err/mean)*(sigma*mean_err/mean));
	    printf("mean = %f, sigma = %f, resolution = %f, res_err = %f\n", mean, sigma, resolution, resolution_err);
	    Int_t bin = calo_enrg_resolution->FindBin(part_enrg);

	    if(ncells == cell_iter+1)
	    {
		calo_enrg_mean->SetBinContent(bin, mean);
		calo_enrg_mean->SetBinError(bin, mean_err);

		calo_enrg_resolution->SetBinContent(bin, resolution);
		calo_enrg_resolution->SetBinError(bin, resolution_err);

		Int_t bin_ = calo_enrg_resolution_ifile->FindBin(x_pos);
		calo_enrg_resolution_ifile->SetBinContent(bin_, resolution);
		calo_enrg_resolution_ifile->SetBinError(bin_, resolution_err);
		calo_enrg_mean_ifile->SetBinContent(bin_, mean);
		calo_enrg_mean_ifile->SetBinError(bin_, mean_err);
	    }


	    Int_t bin__ = calo_res_n_modules->FindBin(cell_iter+1);
	    calo_res_n_modules->SetBinContent(bin__, resolution);
	    calo_res_n_modules->SetBinError(bin__, resolution_err);
	}





	source_file->Close();
	delete source_file;
    }

    if(1)
    {
	TF1 *calo_res_funk = new TF1("calo_res", "TMath::Sqrt( ([0]/TMath::Sqrt(x))*([0]/TMath::Sqrt(x)) + [1]*[1] + ([2]/x)*([2]/x) )", 1., 6.);
	//TF1 *calo_res_funk = new TF1("calo_res", "TMath::Sqrt(  ([0]/TMath::Sqrt(x))*([0]/TMath::Sqrt(x))  )", 1., 6.);
	calo_enrg_resolution->Fit(calo_res_funk, "R", "", 1.5, 6.);



	TCanvas *canv = new TCanvas("canv_res", "canv_res");
	result_array->Add(canv);
	TLatex label;


	calo_enrg_resolution->Draw();
	label.DrawLatex(.55, .4, "#frac{#sigmaE}{E} = #sqrt{(#frac{a}{#sqrt{E}})^2 + (b)^2 + (#frac{c}{E})^2}");
	    label.DrawLatex(.1, .6, Form("a = %.2f; b = %.2f; c = %.2f",
					 calo_res_funk->GetParameter(0),
					 calo_res_funk->GetParameter(1),
					 calo_res_funk->GetParameter(2)) );
//	label.DrawLatex(.55, .4, "#frac{#sigma^{}_{E}}{E} = #frac{a}{#sqrt{E}}");
//	label.DrawLatex(.1, .6, Form("a = %.3f", calo_res_funk->GetParameter(0)));
    }

    TFile *result_file = new TFile((result_file_name + ".root").Data(), "RECREATE");
    SaveArrayStructInFile(result_array, result_file);
    printf("file %s was writed\n", (result_file_name + ".root").Data());
    //result_file->Close();





    new TBrowser;


}
