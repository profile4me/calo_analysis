#ifndef README_READER_H
#define README_READER_H

#include <iostream>
#include <fstream>
#include <ctime>

#include<TString.h>
#include<TMath.h>
#include<TTree.h>
#include<TObject.h>


class Readme_reader
{
public:
    Readme_reader(TString readme_folder_path_);
    ~Readme_reader(){}


    void Set_run_name(TString Run_name_){run_name = Run_name_;} //file must be read and closed here


    TString Read_module_name(Int_t board_ch = 0);
    Int_t Read_module_number(Int_t board_ch);
    TString Read_module_FEE(Int_t board_ch = 0);
    void Read_module_XY(Int_t &X_, Int_t &Y_);
    Int_t Read_module_SC(Int_t board_ch);
    Int_t Read_module_Comment(Int_t board_ch);

    Int_t Read_module_section(Int_t board_ch);
    Int_t Read_board_ch(Int_t module_num, Int_t section_num);
    Int_t Read_left_section(Int_t board_ch);
    Int_t Read_right_section(Int_t board_ch);

    TString Get_run_name(){return run_name;}

private:

    static const char *readme_file_mask;

    TString readme_folder_path;
    TString run_name;

};

const char* Readme_reader::readme_file_mask = "%s.readme.txt";


// #####################################################################
Readme_reader::Readme_reader(TString readme_folder_path_)
    : readme_folder_path(readme_folder_path_), run_name("void")
{

}
// #####################################################################


// #####################################################################
TString Readme_reader::Read_module_name(Int_t board_ch)
{
    if(run_name == "void")
    {
	printf("Waring run name is not set\n");
	return "void";
    }

    TString run_readme_name = Form(readme_file_mask, run_name.Data());

    std::ifstream file;
    file.open( (readme_folder_path + run_readme_name).Data() );
    if (file.is_open())
    {
	//printf("File \"%s\" was opened for reading\n", run_readme_name.Data());
    }
    else
    {
	printf("\nERROR: Readme file \"%s\" was NOT opened for reading\n", (readme_folder_path + run_readme_name).Data() );
	return "void";
    }


    char char_buffer[1000];
    file.getline(char_buffer, 1000);


    file.close();

    TString name = char_buffer;

    Int_t s_pos = 10;
    Int_t p_pos = name.Length();

    //if(name.Contains("module"))s_pos =

    TString new_name = name(s_pos,p_pos-s_pos-1);


    return new_name;



}
// #####################################################################


#endif // README_READER_H



// readme file example
/*

# module record
[1]
#comment sign

module	    # name [string]
data_file   # data file name [string]
ADC64_board # ADC64 board identificator [string]
15_v16	    # FEE name [string]

2   4	    # X Y module position [int, int]
1	    # cell number in slow control [int]
0   1	2   3	4   5	6   7	8   9	10  # cells number - ch numbers [int, int, ... int]

comment	    # comment




[2]

--- // ---

[human]
module №: 12,14,15 CBM PSD module
FEE: 2,7,9
orientation: horizontal
FEE attached without optical grease
MAPD voltage: FEE2 all channels 71.1 V; FEE7 all channels 71.3 V; FEE9 all channels 71.4 V;
thermal compensation: absent
PCB temperature: FEE2 27.08 FEE7 27.12 FEE9 27.06 deg. C from SLowControl at start; FEE2 26.95 FEE7 27.00 FEE9 26.95 deg. C from SLowControl at stop
ADC64S2_v5
ADC64 readout count: 200
ADC64 after trigger readout count: 140
ADC64 trigger condition: threshold on rising edge - all channels 200; ch 13 300 (noisy)
noise levels: TODO
LED flash calibration: TODO
LED flash disabled;
*/
