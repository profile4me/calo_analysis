#ifndef ADC64_TEXT_DATA_H
#define ADC64_TEXT_DATA_H

#include <iostream>
#include <fstream>
#include <ctime>

#include<TString.h>
#include<TMath.h>
#include<TTree.h>
#include<TObject.h>
#include<TString.h>
//#include<TGraph.h>


#include "event_data_struct.h"
#include "data_calculating.h"

class adc64_text_data
{
public:
    adc64_text_data(TString file_name_, TString log_file_name_);
    ~adc64_text_data();

    Int_t read_event();
    void Calculate_waveform(event_data_struct &result_event, event_log_data_struct &result_log_event,
			    Int_t ch_num, Int_t gate_beg, Int_t gate_end);

    // TGraph *GetWaveForm(Int_t channel);

    Bool_t FileIsOpen(){return file.is_open();}
    Int_t **Get_samples_data(){return samples_data;}
    Int_t Get_Events_total(){return events_total;}
    Int_t Get_Channel_total(){return channel_total;}
    Int_t Get_Samples_total(){return sample_total;}

private:

    static const char *header_pattern;
    static const int MaxStrlen;
    static const int MaxLogLines;

    TString file_name;
    TString log_file_name;
    Int_t channel_total;
    Int_t sample_total;
    Int_t events_total;

    std::ifstream file;

    Int_t *ch_num_table;
    Int_t **samples_data;
    Long64_t timestamp;
    Int_t event_number;

    event_log_data_struct *log_data_arr;
    Long64_t *log_timestamp_arr;
    Int_t nLogsLines;

    //TGraph *signal_graph;

    Int_t InspectTXT();
    void ReadLogs();
};

const char* adc64_text_data::header_pattern = "Event";
const int adc64_text_data::MaxStrlen = 10000;


// #####################################################################
adc64_text_data::adc64_text_data(TString file_name_, TString log_file_name_)
    : file_name(file_name_), log_file_name(log_file_name_)
{
    if(InspectTXT() != 0) return;

    ch_num_table = new Int_t[channel_total];
    samples_data = new Int_t*[channel_total];
    for(Int_t ch =0; ch < channel_total; ch++)
	samples_data[ch] = new Int_t[sample_total];

    log_data_arr = NULL;
    log_timestamp_arr = NULL;

    ReadLogs();

    file.open(file_name.Data());
    if (file.is_open())
    {
	printf("File \"%s\" was opened for reading\n", file_name.Data());
    }
    else
    {
	printf("\nERROR: File \"%s\" was NOT opened for reading\n", file_name.Data());
    }

}

// #####################################################################
adc64_text_data::~adc64_text_data()
{
    for(Int_t ch =0; ch < channel_total; ch++)
	delete[] samples_data[ch];
    delete[] samples_data;
    delete[] ch_num_table;

    if(log_timestamp_arr != NULL) delete[] log_timestamp_arr;
    if(log_data_arr != NULL) delete[] log_data_arr;
}
// #####################################################################
Int_t adc64_text_data::InspectTXT()
{
    //    static const char *header_pattern = "Event";
    //    static const int MaxStrlen = 10000;

    printf("\n--------------------------\n");
    printf("Inspecting file \"%s\"\n", file_name.Data());

    std::ifstream file;
    file.open(file_name.Data());
    if (file.is_open())
    {
	char char_buffer[MaxStrlen];
	TString *tstring_buffer = new TString(char_buffer);

	//searching header
	while(true)
	{
	    file.getline(char_buffer, MaxStrlen);
	    if( file.eof() )
	    {
		printf("ERROR: file header NOT found\n");
		delete tstring_buffer;
		return 1;
	    }

	    *tstring_buffer = char_buffer;
	    TObjArray *str_words = tstring_buffer->Tokenize(" ");

	    if((str_words->GetLast() == 2) && ((TObjString*)str_words->At(0))->GetString().Contains(header_pattern))
	    {
		delete str_words;
		break;
	    }
	    delete str_words;
	}
	std::streampos packet_size_beg = file.tellg(), packet_size_end;
	channel_total = 0;
	printf("Channel table:\n");
	Int_t Channels_calc_total = 0;
	while(true)
	{
	    file.getline(char_buffer, MaxStrlen);
	    packet_size_end = file.tellg();
	    if( file.eof() )
	    {
		printf("ERROR: reaching EOF\n");
		delete tstring_buffer;
		return 1;
	    }

	    *tstring_buffer = char_buffer;
	    TObjArray *str_words = tstring_buffer->Tokenize(" ");
	    Int_t ch_num_curr = ((TObjString*)str_words->At(0))->GetString().Atoi();
	    if((str_words->GetLast() == 2) && ((TObjString*)str_words->At(0))->GetString().Contains(header_pattern))
	    {
		delete str_words;
		break;
	    }

	    printf("%i	", ch_num_curr);
	    Channels_calc_total++;
	    sample_total = str_words->GetLast();
	    delete str_words;
	}
	printf("\n");
	channel_total = Channels_calc_total;
	Int_t Packet_size = packet_size_end-packet_size_beg;

	file.seekg( 0, std::ios::beg );
	std::streampos file_size = file.tellg();
	file.seekg( 0, std::ios::end );
	file_size = file.tellg() - file_size;
	events_total = file_size/Packet_size-100;

	printf("Total Channels: %i\n", channel_total);
	printf("Total Samples: %i\n", sample_total);
	printf("Total events: %i\n\n",events_total);
	printf("File size: %i\n", int(file_size));
	printf("Packet size: %i\n", Packet_size);

	delete tstring_buffer;
	file.close();
    }
    else
    {
	printf("\nERROR: File \"%s\" was NOT opened\n", file_name.Data());
	return 1;
    }



    printf("--------------------------\n");
    return 0;
}

// #####################################################################
void adc64_text_data::ReadLogs()
{
    nLogsLines = -1;
    if(log_timestamp_arr != NULL) delete[] log_timestamp_arr;
    if(log_data_arr != NULL) delete[] log_data_arr;

    std::ifstream log_file;
    log_file.open(log_file_name.Data());
    if (!log_file.is_open())
    {
	printf("\nWARING: Log file \"%s\" was NOT opened\n", log_file_name.Data());
	return;
    }

    printf("\n\n--------------------------\n");
    printf("Loading log file \"%s\"\n", log_file_name.Data());


    char char_buffer[MaxStrlen];
    TString *tstring_buffer = new TString(char_buffer);

    //numbers of line
    const Int_t n_first_lines = 100000;
    std::streampos line_n_size, file_size;
    log_file.seekg( 0, std::ios::beg );
    line_n_size = log_file.tellg();
    file_size = log_file.tellg();

    for(Int_t line = 1; line <= n_first_lines; line++)
    {
	log_file.getline(char_buffer, MaxStrlen);
	if(log_file.eof())
	{
	    nLogsLines = line;
	    break;
	}
    }

    if(nLogsLines<0)
    {
	line_n_size = log_file.tellg() - line_n_size;
	log_file.seekg( 0, std::ios::end );
	file_size = log_file.tellg() - file_size;
	nLogsLines = (int)ceil(( (float)file_size / (float)line_n_size )*n_first_lines);
    }

    printf("Estimate log lines: %i\n", nLogsLines);
    //log_file.seekg( 0, std::ios::beg );
    log_file.close();
    log_file.open(log_file_name.Data());
    if (!log_file.is_open())
    {
	printf("\nWARING: Log file \"%s\" was NOT opened\n", log_file_name.Data());
	nLogsLines = -1;
	return;
    }

    log_data_arr = new event_log_data_struct[nLogsLines];
    log_timestamp_arr = new Long64_t[nLogsLines];


    Int_t line_curr = 0;
    for(Int_t line = 0; line < nLogsLines; line++)
    {
	log_file.getline(char_buffer, MaxStrlen);
	if(log_file.eof()) break;

	*tstring_buffer = char_buffer;

	TObjArray *str_words = tstring_buffer->Tokenize(",");

	if(str_words->GetLast() < 4)
	{
	    delete str_words;
	    continue;
	}

	Long64_t readed_timestamp = ((TObjString*)str_words->At(0))->GetString().Atoll();
	log_timestamp_arr[line_curr] = (long long int)(floor(readed_timestamp * 0.001));
	log_data_arr[line_curr].temp =  (Int_t)(((TObjString*)str_words->At(2))->GetString().Atof()*100.);
	log_data_arr[line_curr].LED_HV =  ((TObjString*)str_words->At(3))->GetString().Atoi();
	log_data_arr[line_curr].PinDiode =  ((TObjString*)str_words->At(4))->GetString().Atoi();
	//printf("%s : %i \n", ((TObjString*)str_words->At(4))->GetString().Data(), log_data_arr[line_curr].PinDiode);
	line_curr++;

	delete str_words;
    }

    nLogsLines = line_curr;

    time_t log_start = log_timestamp_arr[0];
    time_t log_end = log_timestamp_arr[nLogsLines-1];
    printf("Readed: %i lines\n", nLogsLines);
    printf("Log first line: %s", asctime( localtime( &log_start ) ) );
    printf("Log last line: %s", asctime( localtime( &log_end ) ) );
    printf("--------------------------\n\n");

    delete tstring_buffer;
    log_file.close();
}

// #####################################################################
Int_t adc64_text_data::read_event()
{
    //    static const char *header_pattern = "Event";
    //    static const int MaxStrlen = 10000;

    Int_t Error_code = 0;

    if(!file.is_open())
    {
	Error_code = -2;
	printf("\nERROR (%i): File with name: \"%s\" is NOT open\n",Error_code, file_name.Data());
	return Error_code;
    }



    //string buffer
    char char_buffer[MaxStrlen];
    TString *tstring_buffer = new TString(char_buffer);


    for(Int_t line_in_event = 0; line_in_event <= channel_total; line_in_event++)
    {
	//reading a line
	if( file.eof() )
	{
	    Error_code = -1;
	    printf("\nWARNING (%i): Reaching EOF\n",Error_code);
	    break;
	}

	file.getline(char_buffer, MaxStrlen);
	//if(DEBUG){ printf("\nDEBUG: readed line [%i] ---------- \n%s\"\n-----------------------------\n",line_in_event, buf);}

	//tokenize the string
	*tstring_buffer = char_buffer;
	TObjArray *str_words = tstring_buffer->Tokenize(" ");
	if(str_words == NULL){Error_code = 4;
	    printf("\nERROR (%i) in event %i: zero tokenize array\n",
		   Error_code, event_number); break;}

	Int_t n_last_word = str_words->GetLast();

	if(n_last_word < 0)
	    if(line_in_event != 0){Error_code = 5; //interupt in middle of packet
		printf("\nERROR (%i) in event %i: tokenize array is empty\n",
		       Error_code, event_number); delete str_words; break;}
	    else{printf("\nReaching end of data, total events: %i\n", event_number); delete str_words; break;}





	//read header
	if(line_in_event == 0)
	{
	    //check header
	    if((n_last_word != 2) || !((TObjString*)str_words->At(0))->GetString().Contains(header_pattern)){Error_code = 1;
		printf("\nERROR (%i): wrong header: \"%s\", must be \"%s + ev_number + timestamp\"\n",
		       Error_code, ((TObjString*)str_words->At(0))->GetString().Data(), header_pattern); delete str_words; break;}

	    event_number = ((TObjString*)str_words->At(1))->GetString().Atoi();
	    timestamp = ((TObjString*)str_words->At(2))->GetString().Atoll();

	}
	else //read data (line_in_event != 0)
	{
	    Int_t ch_num_curr = ((TObjString*)str_words->At(0))->GetString().Atoi();
	    ch_num_table[line_in_event-1] = ch_num_curr;

	    if(((TObjString*)str_words->At(0))->GetString().Contains(header_pattern)){Error_code = 2;
		printf("\nERROR (%i) in event %i: Header instead data, wrong total channel number, data line %i\n",
		       Error_code, event_number, line_in_event); delete str_words; break;}

	    if(n_last_word != sample_total){Error_code = 3;
		printf("\nERROR (%i) in event %i: wrong sample number: %i, must be %i\n",
		       Error_code, event_number, n_last_word, sample_total); delete str_words; break;}

	    //reading samples
	    for(Int_t n_sample = 0; n_sample < sample_total; n_sample++)
	    {
		samples_data[line_in_event-1][n_sample] = ((TObjString*)str_words->At(n_sample+1))->GetString().Atoi();
	    }
	} // header or data

	delete str_words;

    }// for line_in_event

    delete tstring_buffer;

    return Error_code;

}

// #####################################################################
void adc64_text_data::Calculate_waveform(event_data_struct &result_event, event_log_data_struct &result_log_event,
					 Int_t ch_num, Int_t gate_beg, Int_t gate_end)
{

    result_event.reset();
    result_log_event.reset();
    result_event.time_stamp = timestamp;


    //searching log timestamp ----
    Int_t search_index = -1;

    if(nLogsLines>0)
    {
	if(timestamp == log_timestamp_arr[nLogsLines-1])
	{
	    search_index = nLogsLines-1;
	}
	else if ( (timestamp >= log_timestamp_arr[0])&&(timestamp < log_timestamp_arr[nLogsLines-1]) )
	{
	    Int_t indx_l = 0;
	    Int_t indx_r = nLogsLines-1;
	    while( true )
	    {
		//printf("si %i; %lli; %lli\n", search_index, timestamp, log_timestamp_arr[search_index]);
		Int_t indx_m = indx_l + floor((indx_r-indx_l)*0.5);
		if( (timestamp >= log_timestamp_arr[indx_m])&&(timestamp < log_timestamp_arr[indx_m+1])){search_index = indx_m; break;}
		else if(timestamp < log_timestamp_arr[indx_m]) indx_r = indx_m;
		else indx_l = indx_m;
	    }
	}
    }

    if(search_index >= 0)
    {
	result_log_event = log_data_arr[search_index];
    }

    //if(   !(   (timestamp >= log_timestamp_arr[search_index])&&(timestamp < log_timestamp_arr[search_index+1])   )   )
    //printf("ERROR result %i\n", search_index);
    // ---------------------------


    Int_t samples_in_gate = 0;
    Int_t samples_out_gate = 0;

    //PASS 1
    result_event.mean_in_gate = 0.;
    result_event.mean_out_gate = 0.;
    result_event.MAX_in_gate = -100000;
    result_event.MIN_in_gate = 100000;
    result_event.MAX_out_gate = -100000;
    result_event.MIN_out_gate = 100000;
    for(Int_t sample_curr = 0; sample_curr < sample_total; sample_curr++)
    {
	Float_t val_sample = (float)samples_data[ch_num][sample_curr];
	if(sample_curr < gate_beg) //out of gate
	    //if((sample_curr < gate_beg) || (sample_curr > gate_end)) //out of gate
	{
	    result_event.mean_out_gate += val_sample;

	    if(val_sample < result_event.MIN_out_gate)result_event.MIN_out_gate = val_sample;
	    if(val_sample > result_event.MAX_out_gate)result_event.MAX_out_gate = val_sample;

	    samples_out_gate++;
	}

	if((sample_curr >= gate_beg) && (sample_curr <= gate_end)) //in of gate
	{
	    result_event.mean_in_gate += val_sample;

	    if(val_sample < result_event.MIN_in_gate)
	    {
		result_event.MIN_in_gate = val_sample;
		result_event.time_min_in_gate = sample_curr;
	    }
	    if(val_sample > result_event.MAX_in_gate)
	    {
		result_event.MAX_in_gate = val_sample;
		result_event.time_max_in_gate = sample_curr;
	    }

	    samples_in_gate++;
	}
    }
    result_event.mean_in_gate /= (float)samples_in_gate;
    result_event.mean_out_gate /= (float)samples_out_gate;
    //result_event.integral_in_gate /= (float)samples_in_gate;


    //PASS 2
    result_event.integral_in_gate = 0;
    result_event.RMS_in_gate = 0.;
    result_event.RMS_out_gate = 0.;
    for(Int_t sample_curr = 0; sample_curr < sample_total; sample_curr++)
    {
	Float_t val_sample = (float)samples_data[ch_num][sample_curr];
	if(sample_curr < gate_beg) //out of gate
	    //	if((sample_curr < gate_beg) || (sample_curr > gate_end)) //out of gate
	{
	    result_event.RMS_out_gate +=
		    (val_sample-result_event.mean_out_gate) * (val_sample-result_event.mean_out_gate);
	}

	if((sample_curr >= gate_beg) && (sample_curr <= gate_end)) //in of gate
	{
	    result_event.integral_in_gate += (val_sample-result_event.mean_out_gate);

	    result_event.RMS_in_gate +=
		    (val_sample-result_event.mean_in_gate) * (val_sample-result_event.mean_in_gate);
	}
    }

    result_event.RMS_in_gate = TMath::Sqrt( result_event.RMS_in_gate / (float)samples_in_gate );
    result_event.RMS_out_gate = TMath::Sqrt( result_event.RMS_out_gate / (float)samples_out_gate );

}

// #####################################################################
//TGraph *adc64_text_data::GetWaveForm(Int_t channel)
//{

//}


Int_t ConvertFile(TString source_file_name, TString result_file_name, TString log_file_name, Int_t gate_beg, Int_t gate_end, Int_t events_to_process = 0)
{
    printf("CONVERTING ###########################################\n \"%s\"\n to \n \"%s\"\n\n",
	   source_file_name.Data(), result_file_name.Data());

    adc64_text_data *data_class = new adc64_text_data(source_file_name, log_file_name);
    if (!data_class->FileIsOpen()) return 0;

    Int_t channel_total = data_class->Get_Channel_total();
    if(events_to_process == 0) events_to_process = data_class->Get_Events_total();

    event_data_struct *event_struct = new event_data_struct[channel_total];
    event_log_data_struct *log_event_struct = new event_log_data_struct;


    //create result root file, create tree
    TFile *result_file = new TFile(result_file_name.Data(), "RECREATE");
    result_file->cd();


    TTree *data_tree = new TTree("adc64_data", "adc64_data");
    for(Int_t ch = 0; ch < channel_total; ch++)
    {
	(event_struct+ch)->CreateBranch(data_tree, ch);
    }
    log_event_struct->CreateBranch(data_tree);


    time_t start_time = time(NULL);
    Int_t Error_code = 0;
    Int_t start_time_stamp = -1;

    for(Int_t nEvent = 0; nEvent <= events_to_process; nEvent++)
    {
	if((nEvent%100) == 0)
	{
	    time_t current_time = time(NULL);
	    Int_t proc_sec = difftime(current_time,start_time);
	    Float_t percents = (float)nEvent/events_to_process;
	    Int_t time_est = (percents == 0)?0:proc_sec/percents*(1. - percents);
	    Float_t proc_rate = (float)proc_sec/nEvent*1000000./60.;

	    printf("Processed events: %i (%5.1f%%); [pas %3.0dm %2.0is] [est %3.0dm %2.0is] [%.1f min/1M ev]\r",
		   nEvent, (percents*100.),
		   (proc_sec/60), proc_sec%60,
		   (time_est/60), time_est%60,
		   proc_rate);
	    cout<<flush;
	}


	if((Error_code = data_class->read_event()) != 0) break;

	for(Int_t ch = 0; ch < channel_total; ch++)
	{
	    data_class->Calculate_waveform(event_struct[ch], (*log_event_struct), ch, gate_beg, gate_end);

	    if(start_time_stamp < 0) start_time_stamp = event_struct[ch].time_stamp;
	    event_struct[ch].time_stamp -= start_time_stamp;
	}


	data_tree->Fill();
    }

    printf("\nData processing finished with code %i\n", Error_code);
    //if(Error_code > 0) return 2;

    printf("\nWriting data in file \"%s\"\n", result_file_name.Data());
    data_tree->Write();

    //Data calculation
    data_calculation *Data_calc = new data_calculation(data_tree);
    Data_calc->Calculate();

    //Saving Histogramms
    result_file->mkdir("Results_hists");
    TIter nx_iter((TCollection*)(Data_calc->Get_Results()));
    TObjArray *hist_arr;
    while ((hist_arr=(TObjArray*)nx_iter()))
    {
	result_file->mkdir(Form("Results_hists/%s",hist_arr->GetName()));
	result_file->cd(Form("Results_hists/%s",hist_arr->GetName()));
	hist_arr->Write();
    }

    //Saving Canvas
    result_file->mkdir("Results_canvas");
    result_file->cd("Results_canvas");
    TObjArray *canv_arr = Data_calc->Get_Canvas();
    canv_arr->Write();

    for(Int_t i = 0; i< canv_arr->GetLast(); i++)
    {
	((TCanvas*)canv_arr->At(i))->SaveAs(Form("%s.pdf(",result_file_name.Data() ));
    }
    ((TCanvas*)canv_arr->At(canv_arr->GetLast()))->SaveAs(Form("%s.pdf)",result_file_name.Data() ));

    delete data_tree;
    delete Data_calc;

    result_file->Close();
    delete result_file;

    delete data_class;
    delete [] event_struct;

    delete log_event_struct;

    return 0;

}


#endif // ADC64_TEXT_DATA_H

