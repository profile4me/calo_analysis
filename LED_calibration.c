#include <iostream>
#include <fstream>
//#include <conio.h>
#include <ctime>

#include <TFile.h>
#include <TGraph.h>
#include <TF1.h>
#include <TTree.h>
#include <TObjArray.h>
#include <TString.h>
#include <TDirectory.h>
#include <TCanvas.h>
#include <TH1F.h>
#include <TSystemFile.h>
#include <TSystemDirectory.h>
#include <TBrowser.h>
#include <TGraphErrors.h>

//#include "adc64_text_data.h"
#include "event_data_struct_v0.h"
#include "../libraries/utilites/utilites.h"




//Int_t FilesListFromDir(const TString filespath, TObjArray *const collection, Int_t max_num, TString content);


void LED_calibration()
{
    //input data
    //TString source_path = "/mnt/hgfs/DETECTORS_DATA/SOURCE/CALO/T9_cosmic/preprocessed/";

    //TString source_path = "/mnt/disk_data/LED_calib_1q_2018/root_calib_4_maf8_blc50/";
    //TString source_path = "/home/runna61/temp_data/calib_3/";
    //TString source_path = "/home/runna61/temp_data/calib_4_maf8_blc50/";
    //TString source_path = "/home/runna61/temp_data/calib_5_noDSP_freq1_trsh5000/";
    //TString source_path = "/home/runna61/temp_data/calib_6_noDSP_freq100_trsh5000/";
    //TString source_path = "/home/runna61/temp_data/calib_7_noDSP_PINdiode/";
    //TString source_path = "/mnt/win/temp_data/calib_8/";
    //TString source_path = "/mnt/win/temp_data/calib_9_maf8_blc50_fr100_trsh500/";
    //TString source_path = "/mnt/disk_data/LED_calib_1q_2018/calib_10_feeex_maf8_blc50_fr100_trsh500/";

//    TString source_path = "/home/runna61/adc64_data_2018_quater1/LED_calibration_20.04.2018/root/";
//    TString result_path = "/home/runna61/adc64_data_2018_quater1/LED_calibration_20.04.2018/";

//    TString source_path = "/home/runna61/adc64_data_2018_quater1/LED_calibration_5and9_changed_FEE_21.04.2018/root/";
//    TString result_path = "/home/runna61/adc64_data_2018_quater1/LED_calibration_5and9_changed_FEE_21.04.2018/";

//    TString source_path = "/home/runna61/adc64_data_2018_quater1/LED_calibration_5and9_changed_FEE_20Hz_21.04.2018/root/";
//    TString result_path = "/home/runna61/adc64_data_2018_quater1/LED_calibration_5and9_changed_FEE_20Hz_21.04.2018/";

//    TString source_path = "/home/runna61/adc64_data_2018_quater1/LED_calibration_10Hz_22_04_2018/root/";
//    TString result_path = "/home/runna61/adc64_data_2018_quater1/LED_calibration_10Hz_22_04_2018/";

//    TString source_path = "/home/runna61/adc64_data_2018_quater1/LED_calibration_50Hz_index_plus1_23.04.2018/root/";
//    TString result_path = "/home/runna61/adc64_data_2018_quater1/LED_calibration_50Hz_index_plus1_23.04.2018/";

//    TString source_path = "/home/runna61/adc64_data_2018_quater1/LED_calibration_50Hz_index_plus2_23_04_2018/root/";
//    TString result_path = "/home/runna61/adc64_data_2018_quater1/LED_calibration_50Hz_index_plus2_23_04_2018/";

//    TString source_path = "/home/runna61/adc64_data_2018_quater1/LED_calibration_50Hz_index_plus3_23_04_2018/root/";
//    TString result_path = "/home/runna61/adc64_data_2018_quater1/LED_calibration_50Hz_index_plus3_23_04_2018/";

//    TString source_path = "/mnt/disk_data/cosmic_data_2018_quater2/Optical_contact_tests/setting1/LED_calibration_100Hz_II/roots/";
//    TString result_path = "/mnt/disk_data/cosmic_data_2018_quater2/Optical_contact_tests/setting1/";

//    TString source_path = "/mnt/disk_data/cosmic_data_2018_quater2/Optical_contact_tests/setting3/LED_calibration_100Hz/roots/";
//    TString result_path = "/mnt/disk_data/cosmic_data_2018_quater2/Optical_contact_tests/setting3/";

    TString source_path = "/mnt/disk_data/cosmic_data_2018_quater2/adc64_18_06_19_LED_calibration_100Hz/root/";
    TString result_path = "/mnt/disk_data/cosmic_data_2018_quater2/adc64_18_06_19_LED_calibration_100Hz/";

    //TString result_path = "/mnt/disk_data/LED_calib_1q_2018/root_calib2/";
    //TString result_path = "/home/runna61/temp_data/calib_4_maf8_blc50/";
    //TString result_path = source_path;

    TString result_file_name = "Calibration_03_test";
    //TString result_file_name = "LED_calib_50hz_allfiles_00";


    TObjArray files_names;
    files_names.SetOwner();

    // ==================================
    float_t ampl_pedestal = 0.;
    float_t ampl_max = 1000000;
    // ==================================



    // == Source filse management ==
    /****/ // Tate files list from directory
    FilesListFromDir(source_path, &files_names, 5, ".root");

    //remove extension
    for(Int_t f=0; f<=files_names.GetLast();f++)
    {
	TObjString *iFile_name = (TObjString*)(files_names.At(f));
	Int_t p_pos = ((TObjString*)(files_names.At(f)))->GetString().Index(".root", 4);
	TString new_name = iFile_name->GetString()(0,p_pos);
	iFile_name->SetString( new_name );
    }
    /**** //Print files list for manual array
    for(Int_t f=0; f<=files_names.GetLast();f++)
	printf("files_names.Add(new TObjString(\"%s\"));\n", ((TObjString*)(files_names.At(f)))->GetString().Data());
    /**** //manual arry
    files_names.Add(new TObjString("adc64_17_04_12_16_22_overnight")); //m-r
    /****/
    // =============================

    Int_t Files_max = files_names.GetLast();

    printf("\n\n\n#######################################################\n");
    printf("##################  LED CALIBRATION  ##################\n");
    printf("#######################################################\n\n\n");

    printf("--------------------------\n");
    printf("Source path: %s\n", source_path.Data());
    printf("Result path: %s\n", result_path.Data());
    printf("Result file: %s\n", result_file_name.Data());
    printf("Calibration file list:\n");
    for(Int_t f=0; f<=Files_max;f++)
	printf("%s\n",  ((TObjString*)(files_names.At(f)))->GetString().Data());
    printf("--------------------------\n\n");




    std::ofstream log_out ((result_path+result_file_name+".txt").Data());
    log_out<<Form("source folder: %s\n", source_path.Data());
    log_out<<Form("Calibration info:\nfile name : LED ampl : channel : mean : sigma\n");

    TFile *Result_file = new TFile((result_path+result_file_name+".root").Data(), "RECREATE");

    TObjArray *ResulCollection = new TObjArray();
    ResulCollection->SetOwner();


    const Int_t Channels_max = 100;
    Double_t **LED_charge_mean = new Double_t*[Files_max+1];
    Double_t **LED_charge_mean_err = new Double_t*[Files_max+1];
    Double_t **LED_charge_sigma = new Double_t*[Files_max+1];
    Double_t **LED_charge_sigma_err = new Double_t*[Files_max+1];
    for(Int_t f=0; f <= Files_max; f++)
    {
	LED_charge_mean[f] = new Double_t[Channels_max];
	LED_charge_mean_err[f] = new Double_t[Channels_max];
	LED_charge_sigma[f] = new Double_t[Channels_max];
	LED_charge_sigma_err[f] = new Double_t[Channels_max];
    }



    // =============== loop over files
    Int_t Channel_found = 0;
    Int_t Files_found = 0;
    for(Int_t iFile=0; iFile<=Files_max;iFile++)
    {
	// opening files, and load ttree ------------------
	printf("\n");
	TString iFile_name = ((TObjString*)(files_names.At(iFile)))->GetString();
	TFile *currFile = new TFile(source_path+iFile_name+".root");
	if(currFile->IsOpen())
	{
	    printf("File \"%s\" opened\n", iFile_name.Data());
	}
	else
	{
	    printf("File \"%s\" was NOT opened\n", iFile_name.Data());
	    continue;
	    delete currFile;
	}

	TString File_end = iFile_name(iFile_name.Length()-4,4);
	Int_t LED_ampl = File_end.Atoi();


	TTree *data_tree = dynamic_cast<TTree*>(currFile->FindObjectAny("adc64_data"));
	if(data_tree == NULL)
	{
	    printf("Data tree was NOT found\n");
	    currFile->Close();
	    continue;
	}

	Result_file->cd("/");

	// =============== loop over channel
	Int_t total_channels = data_tree->GetNbranches()-1;
	TF1 *fit_gaus = new TF1("gaus_func","gaus");
	for(Int_t iChannel=0; iChannel<total_channels; iChannel++)
	{
	    if(iChannel >= Channels_max) continue;

	    printf("Calculating data for channel %i\n", iChannel);

	    // load or ceating array ----------------------
	    TString ch_name = event_data_struct::GetChName(iChannel);
	    TObjArray *hist_arr_ch = (TObjArray*)(ResulCollection->FindObject( ch_name.Data()));
	    if(hist_arr_ch == NULL)
	    {
		ResulCollection->Add(  hist_arr_ch = new TObjArray() );
		hist_arr_ch->SetName( ch_name.Data() );
		Channel_found++;
		printf("Created array for %s\n", ch_name.Data());
	    }

	    TH1 *TH1_ptr = NULL;

	    // calculation time -------------------------
	    TH1_ptr = new TH1F("name_temp", "title_temp", 2000, 0, 200);
	    data_tree->Draw(Form("(%s.time_half)>>name_temp", ch_name.Data()),
	                    Form("(%s.integral_in_gate) > %f", ch_name.Data(), ampl_pedestal));

	    double_t time_max = TH1_ptr->GetBinCenter( TH1_ptr->GetMaximumBin() );

	    TH1_ptr->SetName(Form("LED_Time_%s_%i",ch_name.Data(), LED_ampl));
	    TH1_ptr->SetTitle( Form("LED Time [%s] [%i]",
	                            ch_name.Data(), LED_ampl) );
	    //hist_arr_ch->Add(TH1_ptr);


	    // calculation charge -------------------------
	    TH1_ptr = new TH1F("name_temp", "title_temp", 10000, 0, ampl_max);
	    data_tree->Draw(Form("(%s.integral_in_gate)>>name_temp", ch_name.Data()));

	    /***
	    Double_t hist_max, hist_RMS, hist_mean;
	    TH1_ptr->GetXaxis()->SetRangeUser(ampl_pedestal,ampl_max);
	    hist_max = TH1_ptr->GetBinCenter( TH1_ptr->GetMaximumBin() );
	    //TH1_ptr->GetXaxis()->SetRangeUser(hist_max -100., hist_max +100.);
	    TH1_ptr->GetXaxis()->SetRangeUser(hist_max -15000., hist_max +15000.);
	    hist_RMS = TH1_ptr->GetRMS();
	    hist_mean = TH1_ptr->GetMean();
	    //TH1_ptr->GetXaxis()->SetRangeUser(hist_mean -hist_RMS*10., hist_mean +hist_RMS*10.);
	    TH1_ptr->GetXaxis()->SetRangeUser(((hist_mean -100000. < ampl_pedestal)?ampl_pedestal: hist_mean -100000.),
					      hist_mean +100000.);
	    fit_gaus->SetRange(hist_mean -hist_RMS*2., hist_mean +hist_RMS*2.);
	    TH1_ptr->Fit(fit_gaus, "QR");
	    /***/
	    double_t mean, sigma;
	    FitHistogrammSmartInMaxPeak(TH1_ptr, mean, sigma, 2, ampl_pedestal, ampl_max);
	    fit_gaus = TH1_ptr->GetFunction(fit_funk_name);
	    /***/



	    if(fit_gaus)
	    {
	    LED_charge_mean[Files_found][iChannel] = fit_gaus->GetParameter(1);
	    LED_charge_mean_err[Files_found][iChannel] = fit_gaus->GetParError(1);
	    LED_charge_sigma[Files_found][iChannel] = fit_gaus->GetParameter(2);
	    LED_charge_sigma_err[Files_found][iChannel] = fit_gaus->GetParError(2);
	    }
	    else
	    {
		LED_charge_mean[Files_found][iChannel] = 0.;
		LED_charge_mean_err[Files_found][iChannel] = 0.;
		LED_charge_sigma[Files_found][iChannel] = 0.;
		LED_charge_sigma_err[Files_found][iChannel] = 0.;
	    }

	    TH1_ptr->SetName(Form("LED_Charge_%s_%i",ch_name.Data(), LED_ampl));
	    TH1_ptr->SetTitle( Form("LED Charge [%s] [%i] [mean %.1f +- %.1f sigma %.1f +- %.1f]",
	                            ch_name.Data(), LED_ampl,
	                            LED_charge_mean[Files_found][iChannel], LED_charge_mean_err[Files_found][iChannel],
	                            LED_charge_sigma[Files_found][iChannel], LED_charge_sigma_err[Files_found][iChannel]) );
	    Double_t x_min = (LED_charge_mean[Files_found][iChannel] -100000. < ampl_pedestal)?ampl_pedestal: LED_charge_mean[Files_found][iChannel] -100000.;
	    double_t x_max = LED_charge_mean[Files_found][iChannel] +100000.;
	    TH1_ptr->GetXaxis()->SetRangeUser(x_min, x_max);

	    hist_arr_ch->Add(TH1_ptr);

	    log_out<<Form("%s : %i : %i: %f : %f\n",
	                  iFile_name.Data(), LED_ampl, iChannel, LED_charge_mean[Files_found][iChannel], LED_charge_sigma[Files_found][iChannel]);


	}
	delete fit_gaus;


	currFile->Close();
	delete currFile;

	Files_found++;
	if(Files_found > Files_max) break;

    }


    log_out<<Form("\n\n");

    // =============== building graphs
    Result_file->cd("/");
    TObjArray *Graph_coll = new TObjArray();
    Graph_coll->SetOwner();
    Graph_coll->SetName("Results_Graphs");
    ResulCollection->Add( Graph_coll );

    Double_t *LED_charge_npech = new Double_t[Channel_found];
    Double_t *LED_charge_npech_err = new Double_t[Channel_found];

    for(Int_t iChannel = 0; iChannel < Channel_found; iChannel++)
    {
	TString ch_name = event_data_struct::GetChName(iChannel);

	//TH1 *Graph_ptr = new TH1F("h_temp", "h_temp", 1000, 0, 10000);

	Double_t *graph_X = new Double_t[Files_found];
	Double_t *graph_Y = new Double_t[Files_found];
	Double_t *graph_X_e = new Double_t[Files_found];
	Double_t *graph_Y_e = new Double_t[Files_found];

	for(Int_t iFile = 0; iFile < Files_found; iFile++)
	{
	    //	    printf("%i %i\n", Files_found, iChannel);

	    //    //che/ch = mean/sigma^2
	    //	    Double_t Y_value = LED_charge_sigma[iFile][iChannel]*LED_charge_sigma[iFile][iChannel];
	    //	    Double_t Y_value_err = 2.*LED_charge_sigma_err[iFile][iChannel]*LED_charge_sigma[iFile][iChannel];

	    Double_t m_ = LED_charge_mean[iFile][iChannel];
	    Double_t m_e = LED_charge_mean_err[iFile][iChannel];
	    Double_t s_ = LED_charge_sigma[iFile][iChannel];
	    Double_t s_e = LED_charge_sigma_err[iFile][iChannel];

	    graph_X[iFile] = m_;
	    graph_Y[iFile] = s_ * s_;
	    graph_X_e[iFile] = m_e;
	    graph_Y_e[iFile] = 2*s_e*s_e;


	    //	    graph_X[iFile] = m_;
	    //	    graph_Y[iFile] = m_ / s_ / s_;
	    //	    graph_X_e[iFile] = m_e;
	    //	    graph_Y_e[iFile] = TMath::Sqrt((m_e/s_/s_)*(m_e/s_/s_) + (-2.*m_*s_e/s_/s_/s_)*(-2.*m_*s_e/s_/s_/s_));


	    //Int_t mean_bin = Graph_ptr->GetXaxis()->FindBin( LED_charge_mean[iFile][iChannel] );
	    //	    Graph_ptr->SetBinContent(mean_bin, Y_value);
	    //	    Graph_ptr->SetBinError(mean_bin, Y_value_err);
	}

	TGraphErrors *Graph_ptr = new TGraphErrors(Files_found, graph_X, graph_Y, graph_X_e, graph_Y_e);


	TF1 *fit_pol1 = new TF1("pol1_func","pol1");
	fit_pol1->SetRange(0, 10000000);

	Graph_ptr->Fit(fit_pol1, "QR");

	Double_t c0 = fit_pol1->GetParameter(0);
	Double_t c1 = fit_pol1->GetParameter(1);
	LED_charge_npech[iChannel] = c1;
	LED_charge_npech_err[iChannel] = fit_pol1->GetParError(1);

	//	Int_t X_min = Graph_ptr->FindFirstBinAbove(1,1);
	//	Int_t X_max = Graph_ptr->FindLastBinAbove(1,1);
	//	Graph_ptr->GetXaxis()->SetRange(X_min, X_max);


	Graph_ptr->SetName(Form("graph_%s", ch_name.Data()));
	Graph_ptr->SetTitle(Form("Charge Mean vs Sigma^2 [%s] [%.1f + x*%.4f];Mean;Sigma^2", ch_name.Data(), c0, LED_charge_npech[iChannel]));
	Graph_coll->Add(Graph_ptr);
	//printf("calib_c0[%i] = %f; calib_c1[%i] = %f;\n", iChannel, c0, iChannel, LED_charge_npech[iChannel]);
	//printf("calib_nPe[%i] = 1./%f;\n", iChannel, LED_charge_npech[iChannel]);
    }



    log_out << Form("\n\nCoeffs table:\nch : coeff : coeff_error\n");
    for(Int_t iCh=0;iCh<Channel_found;iCh++)
    {
	log_out << Form("%i : %f : %f\n", iCh, LED_charge_npech[iCh], LED_charge_npech_err[iCh]);
    }

    log_out << Form("\n\nCoeffs array record:\n");
    for(Int_t iCh=0;iCh<Channel_found;iCh++)
    {
	log_out << Form( "calib_nPe[%i] = 1./%f;\n", iCh, LED_charge_npech[iCh] );
    }

    log_out << Form("\n\nDouble_t calib_nPe[%i] = {", Channel_found);
    for(Int_t iCh=0;iCh<Channel_found;iCh++)
    {
	log_out << Form( "%f", (1./LED_charge_npech[iCh]) );
	if(iCh<Channel_found-1) log_out <<", ";
    }
    log_out << "};\n";



    // =============== saving results
    Result_file->mkdir("Results_hists");
    TIter nx_iter1((TCollection*)(ResulCollection));
    TObjArray *hist_arr;
    while ((hist_arr=(TObjArray*)nx_iter1()))
    {
	Result_file->mkdir(Form("Results_hists/%s",hist_arr->GetName()));
	Result_file->cd(Form("Results_hists/%s",hist_arr->GetName()));
	hist_arr->Write();
    }

    // =============== building canvas
    Result_file->cd("/");
    TObjArray *Canv_coll = new TObjArray();
    Canv_coll->SetOwner();
    Canv_coll->SetName("Results_Canvas");
    //ResulCollection->Add( Canv_coll );

    TIter nx_iter2((TCollection*)(ResulCollection));
    while ((hist_arr=(TObjArray*)nx_iter2()))
    {
	TString array_name = hist_arr->GetName();
	TCanvas *canv_curr = new TCanvas (Form("%s_canv", array_name.Data()), Form("%s", array_name.Data()));
	Int_t n_hists = hist_arr->GetLast();
	canv_curr->DivideSquare( n_hists+1 );
	Canv_coll->Add(canv_curr);
	for(Int_t ihist = 0; ihist <= n_hists; ihist++)
	{
	    canv_curr->cd(ihist+1);

	    TH1 *TH1_ptr = dynamic_cast<TH1*>(hist_arr->At(ihist));
	    if(TH1_ptr != NULL)
	    {
		TH1_ptr->Draw();
	    }

	    TGraph *Graph_ptr = dynamic_cast<TGraph*>(hist_arr->At(ihist));
	    if(Graph_ptr != NULL)
	    {
		Graph_ptr->SetLineWidth(1);
		Graph_ptr->Draw("AP");
	    }
	}

    }

    Result_file->mkdir("/Results_hists");
    Result_file->cd("/Results_hists");
    Canv_coll->Write();

    for(Int_t i = 0; i< Canv_coll->GetLast(); i++)
    {
	((TCanvas*)Canv_coll->At(i))->SaveAs(Form("%s.pdf(",(result_path+result_file_name).Data() ));
    }
    ((TCanvas*)Canv_coll->At(Canv_coll->GetLast()))->SaveAs(Form("%s.pdf)",(result_path+result_file_name).Data() ));








    // =============== cleaning up

    delete ResulCollection;
    delete Canv_coll;

    Result_file->Close();
    delete Result_file;

    for(Int_t f=0; f < Files_max; f++)
    {
	delete[] LED_charge_mean[f];
	delete[] LED_charge_sigma[f];
    }
    delete[] LED_charge_mean;
    delete[] LED_charge_sigma;
    delete[] LED_charge_npech;
    delete[] LED_charge_npech_err;


}
/*
Int_t FilesListFromDir(const TString Directory, TObjArray *const collection, Int_t max_num, TString content)
{
    TString filespath = Directory;

    if(!filespath.EndsWith("/")) filespath += "/";

    if(!collection)
    {
	printf("ERROR: Collection ptr is NULL\n");
	return 0;
    }

    TSystemFile *currFile;
    TString currFileName;
    TSystemDirectory SDirectory(filespath, filespath);
    TIter nextfile(SDirectory.GetListOfFiles());

    Int_t nfiles = 0;
    while ((currFile=(TSystemFile*)nextfile()))
    {
	currFileName = currFile->GetName();
	if (!currFile->IsDirectory() && currFileName.Contains(content))
	{
	    collection->Add( new TObjString( currFileName.Data() ) );
	    nfiles++;
	    if(max_num > 0)
		if(nfiles == max_num) break;
	}
    }

    new TBrowser;
    return nfiles;
}
*/


